/* Update Welcome Messages - Local */
	update maxmessages set value = 'LOCALVM | Welcome, {0}' where msgkey = 'welcomeusername' and msggroup = 'login';
	update maxmessages set value = 'Welcome to Maximo | LOCALVM' where msgkey = 'welcomemaximomessage' and msggroup = 'login';

/* Maximo DocLinks Common Properties */
    update MAXPROPVALUE SET PROPVALUE='E:\doclinks' WHERE Propname ='mxe.doclink.doctypes.topLevelPaths';
    update MAXPROPVALUE SET PROPVALUE='E:\doclinks' WHERE Propname ='mxe.doclink.doctypes.defpath';
    update doctypes set defaultfilepath = 'E:\doclinks';
    update doctypes set defaultfilepath = 'E:\doclinks\attachments' where doctype = 'Attachments';
	update doctypes set defaultfilepath = 'E:\doclinks\signatures' where doctype in ('Recoverable','JSEA','Open','Close','Observer','Entry','Exit');
	
    
/* Maximo DocLinks properties */
	update MAXPROPVALUE SET PROPVALUE='E:\doclinks=http://localhost' WHERE Propname ='mxe.doclink.path01';

/* Maximo Administration Email */
	update MAXPROPVALUE SET PROPVALUE='localadmin@taswaterfake.com.au' WHERE Propname ='mxe.adminEmail';
	update doctypes set defaultfilepath = 'E:\doclinks\attachments' where doctype = 'Attachments';

/* Name of the machine and port hosting MXServer */
    update MAXPROPVALUE SET PROPVALUE='localhost:9080' WHERE Propname ='mxe.hostname';
    update maxpropvalue set propvalue='http://localhost:9080/meaweb' where propname='mxe.int.webappurl';
	
/* SMTP Configuration */
    update MAXPROPVALUE SET PROPVALUE='localhost' WHERE Propname ='mail.smtp.host';
    update MAXPROPVALUE SET PROPVALUE='' WHERE Propname ='mxe.smtp.password';
    update MAXPROPVALUE SET PROPVALUE='' WHERE Propname ='mxe.smtp.user';

/* Global Directory Setting */
/* This might be pushed to env level if not common */
    update MAXPROPVALUE SET PROPVALUE='E:\globaldir' WHERE Propname ='mxe.int.globaldir';
    
	update maxifaceout set ifacemapname='E:\globaldir\xsl\nav\generaljournal.xsl' where ifacename='GBSGLTXNInterface';
	update maxifaceout set ifacemapname='E:\globaldir\xsl\nav\fixedasset.xsl' where ifacename='TWMXFAInterface';
	update maxifaceout set ifacemapname='E:\globaldir\xsl\nav\invoice.xsl' where ifacename='TWMXINVOICEInterface';
	update	maxifaceinvoke set ifacemapname='E:\globaldir\xsl\nav\twnavglvalreq.xsl', 
			replymapname='E:\globaldir\xsl\nav\twnavglvalresp.xsl' where ifacename='TWNAVGLVAL';
	update	maxifaceinvoke set ifacemapname='E:\globaldir\xsl\nav\twnavpurchaseglvalreq.xsl', 
			replymapname='E:\globaldir\xsl\nav\twnavpurchaseglvalresp.xsl' where ifacename='TWNAVPURCHASEGLVAL';
    
/* mxe.dm.root Property for Migration Packages */
    update maxpropvalue set propvalue = 'E:\globaldir\migration' where propname = 'mxe.dm.dmroot';
    
/* Maximo Logging Directory */
    update MAXPROPVALUE SET PROPVALUE='E:\globaldir\logging' WHERE Propname ='mxe.logging.rootfolder';

/* GBS Audit Enable Configuration */
	update MAXPROPVALUE SET PROPVALUE='1' WHERE Propname ='gbs.auditenable.skip';
	
/* Esig not required for admin mode on LocalVM */
	update sigoption set esigenabled = 0 where app = 'CONFIGUR' and optionname in ('ADMINMODE','CONFIGURE');
	update maxpropvalue set propvalue = 0 where propname = 'mxe.adminmode.logoutmin';

/* GIS TWGISWO Mobility Logic Values */
	UPDATE MAXENDPOINTDTL SET VALUE = 'jdbc:sqlserver://;serverName=localhost\\maxdev;databaseName=maximo;portNumber=1433;integratedSecurity=false;sendStringParametersAsUnicode=false;selectMethod=cursor;' WHERE ENDPOINTNAME = 'TWGISWO' AND PROPERTY = 'URL';
	UPDATE MAXENDPOINTDTL SET VALUE = '{call getNearbyAssetsAndLocations(?, ?, ?, ?, ?)}' WHERE ENDPOINTNAME = 'TWGISWO' AND PROPERTY = 'CALL';
	UPDATE MAXENDPOINTDTL SET VALUE = 'maximo' WHERE ENDPOINTNAME = 'TWGISWO' AND PROPERTY = 'USERNAME';
	UPDATE MAXENDPOINTDTL SET PASSWORD = CONVERT(varbinary(2000), '0x2383E883896C0775AE3E562B1BB17A9F', 1) WHERE ENDPOINTNAME = 'TWGISWO' AND PROPERTY = 'PASSWORD';
	UPDATE CRONTASKINSTANCE SET SCHEDULE = '1m,*,*,*,*,*,*,*,*,*', ACTIVE = 1 WHERE CRONTASKNAME = 'TWGISWONEAR' AND INSTANCENAME = 'TWWOGIS';