if [ -z "$1" ]
  then
    echo "No database name supplied"
    echo "Usage -- ./db2BestPracticesConfig.sh maxdb76"
    exit 1
fi

db2set DB2_WORKLOAD=MAXIMO
db2set DB2_FMP_COMM_HEAPSZ=65536


db2 update db cfg for $1 using CHNGPGS_THRESH 40
db2 update db cfg for $1 using DFT_QUERYOPT 5
db2 update db cfg for $1 using LOGBUFSZ 1024
db2 update db cfg for $1 using LOGFILSIZ 8192
db2 update db cfg for $1 using LOGPRIMARY 20
db2 update db cfg for $1 using LOGSECOND 100
db2 update db cfg for $1 using LOCKLIST AUTOMATIC
db2 update db cfg for $1 using LOCKTIMEOUT 300
db2 update db cfg for $1 using MAXFILOP 61440
db2 update db cfg for $1 using NUM_IOCLEANERS AUTOMATIC
db2 update db cfg for $1 using NUM_IOSERVERS AUTOMATIC
db2 update db cfg for $1 using SOFTMAX 1000
db2 update db cfg for $1 using STMTHEAP 20000
db2 update db cfg for $1 using CUR_COMMIT ON

db2 update db cfg for $1 using AUTO_REVAL DEFERRED
db2 update db cfg for $1 using DEC_TO_CHAR_FMT NEW
db2 update db cfg for $1 using STMT_CONC LITERALS

db2 update db cfg for $1 using DATABASE_MEMORY AUTOMATIC
db2 update db cfg for $1 using PCKCACHESZ AUTOMATIC
db2 update db cfg for $1 using DBHEAP AUTOMATIC
db2 update db cfg for $1 using STAT_HEAP_SZ AUTOMATIC
 


db2 update dbm cfg using AGENT_STACK_SZ 1024
db2 update dbm cfg using RQRIOBLK 65535
db2 update dbm cfg using HEALTH_MON OFF
db2 update dbm cfg using MON_HEAP_SZ AUTOMATIC
db2 update dbm cfg using KEEPFENCED YES
db2 update dbm cfg using FENCED_POOL 200

db2 update db cfg for $1 using REC_HIS_RETENTN 30



