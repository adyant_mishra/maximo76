# cluster example
# app name, earfile, cluster (optional), virutal host
apps = [
        {'name':'MAXIMO',       'file':'E:\MaximoDevelopment\maximo\deployment\default\maximo.ear',        'cluster':'MAXIMOUI',        'vhost':'default_host' }
]
 
servers = [ 
    {'nodename':'node01',    'servername':'webserver1'}, 
    {'nodename':'node01',    'servername':'MXServerUI1.1'}
]
execfile('wsadminlib.py')

webservers = listServersOfType('WEB_SERVER')

# reinstall apps
for a in apps:
 
    if 'cluster' in a.keys():
		cluster = [a['cluster']]
    else:
		cluster = []
 
    deleteApplicationByNameIfExists(a['name'])
    save()
 
    options = [
		'-MapModulesToServers', [['.*','.*','WebSphere:cell=cell01,cluster=MAXIMOUI+WebSphere:cell=cell01,node=node01,server=webserver1']],
        '-MapWebModToVH', [['.*', '.*', a['vhost']]],
        '-appname', a['name']
    ]
 
    # /path/to/<span class="skimlinks-unlinked">maximo.ear</span>, webserverarray, clusterarray, other options
    installApplication(a['file'], servers, cluster, options)
     
    print 'Saving config'
    save()
    
print 'Wating 120 seconds for copy processes to finish'	
time.sleep(120)