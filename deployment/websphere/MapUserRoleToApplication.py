role="maximouser"
groups="maximousers"
application="MAXIMO"

roleMapping = [role, "no", "yes", "", groups]
print "==> Mapping " + application + " " + role + " role"
AdminApp.edit(application, ["-MapRolesToUsers", [roleMapping]])
    
# Commit changes to the configuration repository 
AdminConfig.save()