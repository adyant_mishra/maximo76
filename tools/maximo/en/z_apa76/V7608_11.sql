/***************************************************************************
	AUTHOR: Trent Oxley
	CREATE DATE: 30/04/2018
	UPDATE DATE: 23/05/2018 

	Maximo 7.6 UAT Defect 132: Start Time is showing as number on Work View in GAM
	Maximo 7.6 UAT Defect 164: GAM - Colour changes for GAM for colourblindness compatability
	Maximo 7.6 UAT Defect 171: Job Plans - Load Sheets from PMO200 failing to load
	Maximo 7.6 UAT Defect 177: PM - Not taken to chosen Job Plan when chevron used from PM
	Maximo 7.6 UAT Defect 194: Autoscript change in BitBucket	
	Maximo 7.6 UAT Defect 200: GAM security group setup
	Maximo 7.6 UAT - No defect: Sets access to view modify queries in woplan screen to TNSTATUS
		
*******************************************************************************/

--Defect 132
update skdproperty
   set persistent = 1
  where skdobjectname = 'SKDACTIVITY'
   and propertyname = 'startTime'
   and persistent = 0;

--Defect 164   
update maxpropvalue
   set propvalue = 'eeee45' , changeby = 'MAXADMIN' , changedate = sysdate
  where propname in  ('skd.amcrew.waitasgn.background','skd.assignment.waitasgn.background');
				   
--Defect 171
--fix alias conflicts on APA_DMJOBPLANTASKLAB
insert into maxintobjalias (MAXINTOBJALIASID,INTOBJECTNAME,NAME,ALIASNAME,OBJECTID,HIERARCHYPATH)
values (maxintobjaliasseq.nextval,'APA_DMJOBPLANTASKLAB','INSPFORMNUM','JOBPLAN_INSPFORMNUM','1','JOBPLAN');
insert into maxintobjalias (MAXINTOBJALIASID,INTOBJECTNAME,NAME,ALIASNAME,OBJECTID,HIERARCHYPATH)
values (maxintobjaliasseq.nextval,'APA_DMJOBPLANTASKLAB','INTSHIFT','JOBPLAN_INTSHIFT','1','JOBPLAN');				   

--Defect 177
update jobplan set orgid='APAGROUP' where orgid is null;
update maximo.maxattributecfg set defaultvalue = 'APAGROUP' where objectname = 'JOBPLAN' and attributename = 'ORGID';
update maximo.maxattribute set defaultvalue = 'APAGROUP' where objectname = 'JOBPLAN' and attributename = 'ORGID';

--Defect 200
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'ASSOCFOLD', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'BMXSQLWHER', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'BMXVIEWMANAGEWHER', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'BMXVIEWMANAGEWHERRO', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'BOOKMARK', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'CLEAR', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'COMPARE', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'CONFIGPROP', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'CONFOPTSRV', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'COPYSCHEDU', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'CREATESLR', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'DELETE', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'DUPLICATE', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'DYNAMIC', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'EMRGWO', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'INSERT', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'LABORPARAM', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'MANAGEFOLD', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'MANAGELIB', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'MANRESULTS', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'NEXT', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'ODMERUNMON', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'ODMESCHD', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'ODMEVIEW', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'POPUMATRIX', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'PREVIOUS', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'PUBLISH', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'READ', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'REFRESH', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'REPFACILITY', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'ROLLPRJ', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'RUNEMRG', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'RUNOPTMZ', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'RUNREPORTS', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'SAVE', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'SCENARIO', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'SCENARIOS', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'SEARCHBOOK', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'SEARCHMORE', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'SEARCHSQRY', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'SEARCHTIPS', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'SEARCHVMQR', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'SEARCHWHER', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'SHOWODMECFG', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'SLRAPPLY', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'SLRCRON', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'SLRDELETE', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'SPATLPARAM', applicationauthseq.nextval, null, '666');
insert into applicationauth (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values ('SCHEDULER', 'RLASSIGN', 'VMAINSNRIO', applicationauthseq.nextval, null, '666');

--No Defect. Identified by Jason Robinson
insert into applicationauth
  (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values
  ('TNSTATUS',
   'APAWOPLAN',
   'BMXVIEWMANAGEWHERRO',
   applicationauthseq.nextval,
   null,
   '17110562926');

insert into applicationauth
  (GROUPNAME, APP, OPTIONNAME, APPLICATIONAUTHID, CONDITIONNUM, ROWSTAMP)
values
  ('TNSTATUS',
   'APAWOPLAN',
   'BMXVIEWMANAGEWHER',
   applicationauthseq.nextval,
   null,
   '17110562928');

commit;