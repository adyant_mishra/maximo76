#########################################################
# Copyright IBM Corp. 2014
#
# APA HSE Project
#
# Script APAHSE_OBJ_INCIDENT_INIT
# Author:acherem@au1.ibm.com
# mgiang@au1.ibm.com Updated for OIM
##########################################################

from psdi.mbo import MboConstants

def setInvestigationDetailsReadonly():
	mbo.setFieldFlag("PLUSGINVESTLEAD",MboConstants.READONLY,True)
	mbo.setFieldFlag("PLUSGINVLEADNAME",MboConstants.READONLY,True)
	mbo.setFieldFlag("APAHSEINCDISCUSSED",MboConstants.READONLY,True)

def loadWorkAreaDomain():
	domainMap = {"ID":"APAHSEIDWORKAREA",
				"NETWORKS":"APAHSENETWORKAREA",
				"TRANSMISSION":"APAHSETRANWORKAREA",
				"POWER ASSETS":"APAHSEPOWWORKAREA",
				"CORPORATE":"NONE"
				}
	listener = mbo.getMboValue("APAHSEWORKAREA").getMboValueInfo().getDomainInfo().getDomainObject(mbo.getMboValue("APAHSEWORKAREA"))
	smsnum = mbo.getString("APAHSESMSNUM")
	if smsnum is not None and not smsnum in ("None",""):
		where = "domainid = '" + domainMap[smsnum] + "'"
		if smsnum in ("ID","NETWORKS","TRANSMISSION","POWER ASSETS"):
			mbo.setFieldFlag("APAHSEWORKAREA",MboConstants.REQUIRED,True)
	elif smsnum is None or smsnum == "" or smsnum=="None":
		where = "1=0"
	if listener is not None:
		listener.setListCriteria(where)
		listener.getList().setWhere(where)

def clearWorkAreaDomain():
	# Clear the work area domain, by default it loads the table domain with everything merged
	listener = mbo.getMboValue("APAHSEWORKAREA").getMboValueInfo().getDomainInfo().getDomainObject(mbo.getMboValue("APAHSEWORKAREA"))
	where = "1=0"
	if listener is not None:
		listener.setListCriteria(where)
		listener.getList().setWhere(where)
def setAssetStrikeReadOnlyFlags(flagValue):
	mbo.setFieldFlag("APAHSESTRIKEGAS",MboConstants.READONLY,flagValue)
	mbo.setFieldFlag("APAHSESTRIKEELEC",MboConstants.READONLY,flagValue)
	mbo.setFieldFlag("APAHSESTRIKETEL",MboConstants.READONLY,flagValue)
	mbo.setFieldFlag("APAHSESTRIKEWAT",MboConstants.READONLY,flagValue)
	mbo.setFieldFlag("APAHSESTRIKESEW",MboConstants.READONLY,flagValue)
	mbo.setFieldFlag("APAHSESTRIKEOTH",MboConstants.READONLY,flagValue)
	mbo.setFieldFlag("APAHSESTRIKEDETAIL",MboConstants.READONLY,flagValue)
	mbo.setFieldFlag("APAHSEDEVICEUSED",MboConstants.READONLY,flagValue)
def setLossOfContainmentReadOnlyFlags(flagValue):
  mbo.setFieldFlag("APAHSELIQUIDVAL",MboConstants.READONLY,flagValue)
  mbo.setFieldFlag("APAHSEGASESVAL",MboConstants.READONLY,flagValue)
  mbo.setFieldFlag("APAHSESOLIDSVAL",MboConstants.READONLY,flagValue)
  mbo.setFieldFlag("APAHSELOSSCONT",MboConstants.READONLY,flagValue)	

if app == "APAHSEINC":
	status = mbo.getString("STATUS");
	# classification and incident type are always required
	mbo.setFieldFlag("CLASSSTRUCTURE.HIERARCHYPATH",MboConstants.REQUIRED,True)
	mbo.setFieldFlag("PLUSGINCTYPE",MboConstants.REQUIRED,True)

	incType = mbo.getString("PLUSGINCTYPE")
	classSet = mbo.getMboSet("CLASSSTRUCTURE")
	# The Incident Date field is mandatory 
	mbo.setFieldFlag("APAHSEINCIDENTDATE",MboConstants.REQUIRED,True)

	# Incident summary and details are mandatory
	mbo.setFieldFlag("DESCRIPTION",MboConstants.REQUIRED,True)
	mbo.setFieldFlag("DESCRIPTION_LONGDESCRIPTION",MboConstants.REQUIRED,True)

	# The Location and HSE Management System fields are mandatory
	mbo.setFieldFlag("LOCATION",MboConstants.REQUIRED,True)
	mbo.setFieldFlag("APAHSESMSNUM",MboConstants.REQUIRED,True)
	
	# Default reported by to the current user for newly created incidents, 
	# By default maximo copies reportedby into affected person, we cant just suppress action 
	# because we need reporters details (name, phone,etc, still crossed over)
	# so we just cleaning up the affected person

	if mbo.isNew():
		# The following fields will be read-only by default:
		# Employee Affected,Affected Person,Name,Vendor,Phone,E-mail,Person Affected is
		mbo.setValue("REPORTEDBYID",mbo.getUserInfo().getPersonId())
		mbo.setValueNull("AFFECTEDPERSONID",MboConstants.NOACCESSCHECK)
		
		# The 'Regulatory Report Details' field is read-only when an incident is created. It will only be editable when the Reportable to Regulator checkbox is ticked.
		mbo.setFieldFlag("APAHSEREGDETAILS",MboConstants.READONLY,True)
		mbo.setFieldFlag("APAHSEREGDETAILS",MboConstants.REQUIRED,False)
		mbo.setFieldFlag("APAHSEREGULATORNOTIFIED",MboConstants.READONLY,True)
		mbo.setFieldFlag("APAHSEREGULATORNOTIFIED",MboConstants.REQUIRED,False)
		# The 'Regulatory Received Details' field is read-only when an incident is created. It will only be editable when the Regulatory Received field is populated.
		mbo.setFieldFlag("APAHSEREGRECEIVEDDETAILS",MboConstants.READONLY,True)
		mbo.setFieldFlag("APAHSEREGRECEIVEDDETAILS",MboConstants.REQUIRED,False)
		# The 'Environmental Near miss' field is read-only when an incident is created. It will only be editable when the Classification is set to Environmental
		mbo.setFieldFlag("APAHSEENVNEARMISS",MboConstants.READONLY,True)
		
		mbo.setFieldFlag("AFFECTEDPERSONID",MboConstants.READONLY,True)
		mbo.setFieldFlag("AFFECTEDUSERNAME",MboConstants.READONLY,True)
		mbo.setFieldFlag("APAHSECONTRACTORNAME",MboConstants.READONLY,True)
		mbo.setFieldFlag("APAHSEPRINCIPLECONTRACTOR",MboConstants.READONLY,True)
		mbo.setFieldFlag("VENDOR",MboConstants.READONLY,True)
		mbo.setFieldFlag("AFFECTEDPHONE",MboConstants.READONLY,True)
		mbo.setFieldFlag("AFFECTEDEMAIL",MboConstants.READONLY,True)
		mbo.setFieldFlag("APAHSEQUESTION2",MboConstants.READONLY,True)
		mbo.setFieldFlag("APAHSEQUESTION3",MboConstants.READONLY,True)
		mbo.setFieldFlag("APAHSEQUESTION4",MboConstants.READONLY,True)
		mbo.setFieldFlag("APAHSEQUESTION5",MboConstants.READONLY,True)
		# Unit test fix: mgiang@au1.ibm.com added APAHSEAFFECTEDPRSTYPE
		mbo.setFieldFlag("APAHSEAFFECTEDPRSTYPE", MboConstants.READONLY,True)
		
		setInvestigationDetailsReadonly()
		clearWorkAreaDomain()
		setAssetStrikeReadOnlyFlags(True)
		mbo.setFieldFlag("APAHSEELECVOLT",MboConstants.READONLY,True)
		mbo.setFieldFlag("APAHSEGASPRESS",MboConstants.READONLY,True)
		mbo.setFieldFlag("APAIMMCSOTHRDESC",MboConstants.READONLY,True)
		# Make readonly fields for Loss of Containment section field selection, If its an New record
		setLossOfContainmentReadOnlyFlags(True)
	else:
		#Not a new MBO
		# For Sensitive Incidents we need to hide the information from view
		if mbo.evaluateCondition("APAHSESENSI1"):
			mbo.setFlag(MboConstants.DISCARDABLE, True)
			# Requirement was to replace the description with descriptive text
			mbo.setValue("DESCRIPTION","This is a Sensitive Incident and is hidden from view",MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
			# fields have been hidden using sigoptions, but on the list tab they need to be cleared out
			mbo.setValueNull("CLASSSTRUCTUREID",MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
			mbo.setValueNull("LOCATION",MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
			mbo.setValueNull("APAHSEHAZARDID",MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
			# These fields need to be cleared there is an opportunity for the fields to be shown when a Communication template is selected to be sent on the record. The data restriction didnt hide these from the commtemplates
			mbo.setValueNull("APAHSEENVNEARMISS",MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
			mbo.setValueNull("APAHSEINCIDENTDATE",MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
			mbo.setValueNull("APAHSESUBLOCATION",MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
			mbo.setValueNull("APAHSESMSNUM",MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
			mbo.setValueNull("APAHSEINCRATING",MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
			mbo.setValueNull("VENDOR",MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
			mbo.setValue("PLUSGREPORTABLE","0", MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
			mbo.setValueNull("APAHSEREGULATORNOTIFIED",MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
			mbo.setValueNull("APAHSEPROJECTCODE",MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
			mbo.setValueNull("APAHSEAFFECTEDPRSTYPE",MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)	
			mbo.setValueNull("APAHSEWORKAREA",MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)	
			mbo.setValueNull("APAHSEUNDERPERMIT",MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)	
			mbo.setValueNull("APAHSEPERMITNUM",MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)	
			mbo.setValueNull("APAHSEPRINCIPLECONTRACTOR",MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)	
			# These need to be hidden, security restrictions intermitently didnt work along side this autoscript
			mbo.getMboValue("APAHSELOCKATTACHMENTS").setFlag(MboConstants.HIDDEN, True)
			mbo.getMboValue("STATUS").setFlag(MboConstants.HIDDEN, True)
			mbo.getMboValue("REPORTDATE").setFlag(MboConstants.HIDDEN, True)
			mbo.getMboValue("APAHSEENVNEARMISS").setFlag(MboConstants.HIDDEN, True)
			mbo.getMboValue("APAHSEQUESTION1").setFlag(MboConstants.HIDDEN, True)
			mbo.getMboValue("APAHSEQUESTION2").setFlag(MboConstants.HIDDEN, True)
			mbo.getMboValue("APAHSEQUESTION3").setFlag(MboConstants.HIDDEN, True)
			mbo.getMboValue("APAHSEQUESTION4").setFlag(MboConstants.HIDDEN, True)
			mbo.getMboValue("APAHSEQUESTION5").setFlag(MboConstants.HIDDEN, True)
			mbo.getMboValue("APAHSEINCTYPEMOD").setFlag(MboConstants.HIDDEN, True)
			# INC0062930 - Need to hide the Description's longdescription
			mbo.getMboValue("DESCRIPTION_LONGDESCRIPTION").setFlag(MboConstants.HIDDEN, True)
		else:
			# If the 'Reportable to Regulator?' flag was selected, the 'Regulatory Report Details' field will 
			# become editable and mandatory. If flag is cleared, 'Regulatory Report Details' is also cleared and made read-only again.
			reportable = mbo.getBoolean("PLUSGREPORTABLE")
			if reportable:
				mbo.setFieldFlag("APAHSEREGDETAILS",MboConstants.READONLY,False)
				mbo.setFieldFlag("APAHSEREGDETAILS",MboConstants.REQUIRED,True)
				mbo.setFieldFlag("APAHSEREGULATORNOTIFIED",MboConstants.READONLY,False)
				mbo.setFieldFlag("APAHSEREGULATORNOTIFIED",MboConstants.REQUIRED,True)
			else:
				mbo.setFieldFlag("APAHSEREGDETAILS",MboConstants.READONLY,True)
				mbo.setFieldFlag("APAHSEREGDETAILS",MboConstants.REQUIRED,False)
				mbo.setFieldFlag("APAHSEREGULATORNOTIFIED",MboConstants.READONLY,True)
				mbo.setFieldFlag("APAHSEREGULATORNOTIFIED",MboConstants.REQUIRED,False)
				
			# If the 'Regulatory Notice Received' field is populated, the 'Regulatory Notice Received Details' field will 
			# become editable and mandatory. If flag is cleared, 'Regulatory Notice Received Details' is also cleared and made read-only again.
			if not mbo.isNull("APAHSEREGULATORRECEIVED"):
				mbo.setFieldFlag("APAHSEREGRECEIVEDDETAILS",MboConstants.READONLY,False)
				mbo.setFieldFlag("APAHSEREGRECEIVEDDETAILS",MboConstants.REQUIRED,True)
			else:
				mbo.setFieldFlag("APAHSEREGRECEIVEDDETAILS",MboConstants.READONLY,True)
				mbo.setFieldFlag("APAHSEREGRECEIVEDDETAILS",MboConstants.REQUIRED,False)
				
			if mbo.getBoolean("APAHSEINCDISCUSSED"):
				mbo.setFieldFlag("PLUSGINVESTLEAD",MboConstants.READONLY,False)
				mbo.setFieldFlag("PLUSGINVESTLEAD",MboConstants.REQUIRED,True)
				mbo.setFieldFlag("PLUSGINVLEADNAME",MboConstants.READONLY,False)
				mbo.setFieldFlag("PLUSGINVLEADNAME",MboConstants.REQUIRED,True)
			else:
				mbo.setFieldFlag("PLUSGINVESTLEAD",MboConstants.READONLY,True)
				mbo.setFieldFlag("PLUSGINVLEADNAME",MboConstants.READONLY,True)
				
			afType = mbo.getString("APAHSEAFFECTEDPRSTYPE")
			if len(afType)>0:
				if afType == "EMPLOYEE":
					mbo.setFieldFlag("AFFECTEDPERSONID",MboConstants.READONLY,False)
					mbo.setFieldFlag("AFFECTEDPERSONID",MboConstants.REQUIRED,True)
					
					#clean up other fields
					mbo.setFieldFlag("VENDOR",MboConstants.READONLY,True)
					mbo.setFieldFlag("VENDOR",MboConstants.REQUIRED,False)
					mbo.setFieldFlag("APAHSECONTRACTORNAME",MboConstants.READONLY,True)
					mbo.setFieldFlag("APAHSECONTRACTORNAME",MboConstants.REQUIRED,False)	
					mbo.setFieldFlag("APAHSEPRINCIPLECONTRACTOR",MboConstants.READONLY,True)
				elif afType in("CONTRACT","SUBCONT"):
					#clean up id
					mbo.setFieldFlag("AFFECTEDPERSONID",MboConstants.READONLY,True)
					mbo.setFieldFlag("AFFECTEDPERSONID",MboConstants.REQUIRED,False)
					
					mbo.setFieldFlag("VENDOR",MboConstants.READONLY,False)
					mbo.setFieldFlag("VENDOR",MboConstants.REQUIRED,True)
					mbo.setFieldFlag("AFFECTEDUSERNAME",MboConstants.READONLY,True)
					mbo.setFieldFlag("AFFECTEDUSERNAME",MboConstants.REQUIRED,False)
					mbo.setFieldFlag("APAHSECONTRACTORNAME",MboConstants.READONLY,False)
					mbo.setFieldFlag("APAHSECONTRACTORNAME",MboConstants.REQUIRED,True)
					mbo.setFieldFlag("APAHSEPRINCIPLECONTRACTOR",MboConstants.READONLY,False)
					mbo.setFieldFlag("APAHSEPRINCIPLECONTRACTOR",MboConstants.REQUIRED,True)
					mbo.setFieldFlag("AFFECTEDPHONE",MboConstants.READONLY,False)
					mbo.setFieldFlag("AFFECTEDEMAIL",MboConstants.READONLY,False)
				elif afType in("VISITOR","PUBLIC"):
					#clean up id
					mbo.setFieldFlag("AFFECTEDPERSONID",MboConstants.READONLY,True)
					mbo.setFieldFlag("AFFECTEDPERSONID",MboConstants.REQUIRED,False)
					
					mbo.setFieldFlag("VENDOR",MboConstants.READONLY,True)
					mbo.setFieldFlag("VENDOR",MboConstants.REQUIRED,False)
					
					mbo.setFieldFlag("AFFECTEDUSERNAME",MboConstants.READONLY,True)
					mbo.setFieldFlag("AFFECTEDUSERNAME",MboConstants.REQUIRED,False)
					mbo.setFieldFlag("APAHSECONTRACTORNAME",MboConstants.READONLY,False)
					mbo.setFieldFlag("APAHSECONTRACTORNAME",MboConstants.REQUIRED,True)
					mbo.setFieldFlag("APAHSEPRINCIPLECONTRACTOR",MboConstants.READONLY,True)
					mbo.setFieldFlag("AFFECTEDPHONE",MboConstants.READONLY,False)
					mbo.setFieldFlag("AFFECTEDEMAIL",MboConstants.READONLY,False)
			else:
				mbo.setFieldFlag("AFFECTEDPERSONID",MboConstants.READONLY,True)
				mbo.setFieldFlag("AFFECTEDPERSONID",MboConstants.REQUIRED,False)	
				mbo.setFieldFlag("VENDOR",MboConstants.READONLY,True)
				mbo.setFieldFlag("VENDOR",MboConstants.REQUIRED,False)
				mbo.setFieldFlag("AFFECTEDUSERNAME",MboConstants.READONLY,True)
				mbo.setFieldFlag("AFFECTEDUSERNAME",MboConstants.REQUIRED,False)
				mbo.setFieldFlag("APAHSECONTRACTORNAME",MboConstants.READONLY,True)
				mbo.setFieldFlag("APAHSECONTRACTORNAME",MboConstants.REQUIRED,False)
				mbo.setFieldFlag("APAHSEPRINCIPLECONTRACTOR",MboConstants.READONLY,True)
				mbo.setFieldFlag("AFFECTEDPHONE",MboConstants.READONLY,True)
				mbo.setFieldFlag("AFFECTEDEMAIL",MboConstants.READONLY,True)
				
			# The Investigation Details section will be read-only when status of the incident is  
			# NEW,PENDING,REJECTED,RESOLVED
			if status in ("NEW","PENDING","REJECTED","RESOLVED"):
				setInvestigationDetailsReadonly()
			elif mbo.getBoolean("APAHSEINCDISCUSSED"):
				mbo.setFieldFlag("PLUSGINVESTLEAD",MboConstants.READONLY,False)
				mbo.setFieldFlag("PLUSGINVESTLEAD",MboConstants.REQUIRED,True)
				mbo.setFieldFlag("PLUSGINVLEADNAME",MboConstants.READONLY,False)
				mbo.setFieldFlag("PLUSGINVLEADNAME",MboConstants.REQUIRED,True)
			else:
				#Investigation Discussed has not been ticked yet
				mbo.setFieldFlag("PLUSGINVESTLEAD",MboConstants.READONLY,True)
				mbo.setFieldFlag("PLUSGINVLEADNAME",MboConstants.READONLY,True)	
			loadWorkAreaDomain()
			if (mbo.getBoolean("APAHSEASSETSTRIKE")):
				setAssetStrikeReadOnlyFlags(False)
				mbo.setFieldFlag("APAHSESTRIKEDETAIL",MboConstants.REQUIRED,True)
				mbo.setFieldFlag("APAHSEDEVICEUSED",MboConstants.REQUIRED,True)
				if (mbo.getBoolean("APAHSESTRIKEELEC")):
					mbo.setFieldFlag("APAHSEELECVOLT",MboConstants.REQUIRED,True)
					mbo.setFieldFlag("APAHSEELECVOLT",MboConstants.READONLY,False)
				else:
					mbo.setFieldFlag("APAHSEELECVOLT",MboConstants.READONLY,True)
				if (mbo.getBoolean("APAHSESTRIKEGAS")):
					mbo.setFieldFlag("APAHSEGASPRESS",MboConstants.REQUIRED,True)
					mbo.setFieldFlag("APAHSEGASPRESS",MboConstants.READONLY,False)
				else:
					mbo.setFieldFlag("APAHSEGASPRESS",MboConstants.READONLY,True)
			else:
				setAssetStrikeReadOnlyFlags(True)
				mbo.setFieldFlag("APAHSEELECVOLT",MboConstants.READONLY,True)
				mbo.setFieldFlag("APAHSEGASPRESS",MboConstants.READONLY,True)
			# Question set are to remain readonly when the Incident has already been saved.	
			mbo.setFieldFlag("APAHSEQUESTION1",MboConstants.READONLY,True)
			mbo.setFieldFlag("APAHSEQUESTION2",MboConstants.READONLY,True)
			mbo.setFieldFlag("APAHSEQUESTION3",MboConstants.READONLY,True)
			mbo.setFieldFlag("APAHSEQUESTION4",MboConstants.READONLY,True)
			mbo.setFieldFlag("APAHSEQUESTION5",MboConstants.READONLY,True)
			if (not mbo.getBoolean("APAIMMCSOTHR")):
				mbo.setFieldFlag("APAIMMCSOTHRDESC",MboConstants.READONLY,True)
			# Make readonly fields for Loss of Containment section field selection, If it is not New record		
		if (mbo.getBoolean("APAHSELIQUID") or mbo.getBoolean("APAHSEGASES") or mbo.getBoolean("APAHSESOLIDS")):
			mbo.setFieldFlag("APAHSELOSSCONT",MboConstants.READONLY,False)
		else:
			mbo.setFieldFlag("APAHSELOSSCONT",MboConstants.READONLY,True)		
		if mbo.getBoolean("APAHSELIQUID"):
			mbo.setFieldFlag("APAHSELIQUIDVAL",MboConstants.REQUIRED,True)
			mbo.setFieldFlag("APAHSELIQUIDVAL",MboConstants.READONLY,False)
			#mbo.setFieldFlag("APAHSEGASESVAL",MboConstants.READONLY,True)
			#mbo.setFieldFlag("APAHSESOLIDSVAL",MboConstants.READONLY,True)
		else:
			mbo.setFieldFlag("APAHSELIQUIDVAL",MboConstants.READONLY,True)	
		if mbo.getBoolean("APAHSEGASES"):
			mbo.setFieldFlag("APAHSEGASESVAL",MboConstants.REQUIRED,True)
			mbo.setFieldFlag("APAHSEGASESVAL",MboConstants.READONLY,False)
			#mbo.setFieldFlag("APAHSESOLIDSVAL",MboConstants.READONLY,True)
			#mbo.setFieldFlag("APAHSELIQUIDVAL",MboConstants.READONLY,True)
		else:
			mbo.setFieldFlag("APAHSEGASESVAL",MboConstants.READONLY,True)	
		if mbo.getBoolean("APAHSESOLIDS"):	
			mbo.setFieldFlag("APAHSESOLIDSVAL",MboConstants.REQUIRED,True)
			mbo.setFieldFlag("APAHSESOLIDSVAL",MboConstants.READONLY,False)
			#mbo.setFieldFlag("APAHSELIQUIDVAL",MboConstants.READONLY,True)
			#mbo.setFieldFlag("APAHSEGASESVAL",MboConstants.READONLY,True)
		else:
			mbo.setFieldFlag("APAHSESOLIDSVAL",MboConstants.READONLY,True)	
		
	# INC0092022 Duplicate Incident has no TicketId or classstructureid set when the Init script is launched.
	if (not mbo.isNull("TICKETID")):
		if mbo.getMboSet("$classancestor","classancestor","ancestorclassid = 'OP' and classstructureid=:classstructureid").isEmpty():
			# HSE incident
			mbo.setFieldFlag("APAHSEAFFECTEDPRSTYPE",MboConstants.REQUIRED,True)
			if (mbo.getString("APAHSESMSNUM") == "ID"):
				mbo.setFieldFlag("APAHSEPROJECTCODE",MboConstants.REQUIRED,True)
			#Removed the following from the UI so its not mandatory for OIM
			mbo.setFieldFlag("APAHSESUBLOCATION",MboConstants.REQUIRED,True)
			mbo.setFieldFlag("ASSETSITEID",MboConstants.READONLY,True)
			
			# DMND0003542 Moved this restriction to HSE, OIM dont want the record locked out when its under Review.
			# QC-2768 Moved Global Data restriction logic to the Automation script, to allow for Attachments to be added by the reported by
			# when the Incident is assigned to the Supervisor/HSE Advisor and in the WF already.
			if mbo.evaluateCondition("APAHSE_INCWF"):
				mbo.setFlag(MboConstants.READONLY, True)
				if mbo.getString("REPORTEDBYID") == mbo.getUserInfo().getPersonId():
					mbo.setFlag(MboConstants.NOADD,False)
			
			if not mbo.getMboSet("$classancestor","classancestor","ancestorclassid = 'ENVIRONMENTAL' and classstructureid=:classstructureid").isEmpty():
				# classification is ENVIRONMENTAL
				
				mbo.setFieldFlag("APAHSEQUESTION1",MboConstants.READONLY,True)  
				mbo.setFieldFlag("APAHSEQUESTION2",MboConstants.READONLY,True)  
				mbo.setFieldFlag("APAHSEQUESTION3",MboConstants.READONLY,True) 
				mbo.setFieldFlag("APAHSEQUESTION4",MboConstants.READONLY,True) 
				mbo.setFieldFlag("APAHSEQUESTION5",MboConstants.READONLY,True) 
				mbo.setFieldFlag("APAHSEMTRDETAIL",MboConstants.READONLY,True) 
				mbo.setFieldFlag("APAHSEEMERGENCYSRV",MboConstants.READONLY,True) 
				#mgiang@au1.ibm.com Comment out the following line because it was raised as a System test defect 
				#mbo.setFieldFlag("PLUSGREPORTABLE",MboConstants.READONLY,True) 
				#mgiang@au1.ibm.com Comment out the following line because these fields should not be related to the classification
				#mbo.setFieldFlag("APAHSEREGDETAILS",MboConstants.READONLY,True)
				mbo.setFieldFlag("APAHSEENVNEARMISS",MboConstants.READONLY,False)
				mbo.setFieldFlag("APAHSESHIFTTIME",MboConstants.REQUIRED,False)
			else:
				# classification is WHS
				
				# this is a bit tricky - we need to allow the entry of missed hours even for closed incidents
				# hours are added in a dialog that pops up via the select action menu
				# if incident is closed, the core maximo doesnt allow to create any records 
				# associated to the incident via a relationships (i.e. new missed hours records)
				# to avoid this we are resetting the flag NOADD on the mbo
				# Note: this may affect other associated record as well (not just missed hours), which is not good
				# tested in RelatedRecords tab and Log tab - it doesnt allow additions for closed incidents there
				if not mbo.isNew() and len(incType) > 0 and mbo.getBoolean("HISTORYFLAG") and incType in ("MOTI","LTI","FAT"):
					mbo.setFlag(MboConstants.NOADD,False)
					
				if not classSet.isEmpty() and classSet.getMbo(0).getString("classificationid") == 'DRIVING':
					mbo.setFieldFlag("APAHSEISDRIVINFRING",MboConstants.READONLY,False)
				else:
					mbo.setFieldFlag("APAHSEISDRIVINFRING",MboConstants.READONLY,True)

				# The field Time in Shift will be mandatory when the classification of the incident is WHS.
				mbo.setFieldFlag("APAHSESHIFTTIME",MboConstants.REQUIRED,True)
					
				# The field 'Details of Medical Treatment Received (Not First Aid)' is read-only by default.
				# If Incident Type = Moderate Medical Treatment Injury (MOTI) or Minor Medical treatment injury (MITI) or 
				# Lost Time Injury (LTI), then field 'Details of Medical Treatment Received (Not First Aid)' 
				# will become editable and mandatory.

				if len(incType) > 0 and incType in ("MOTI","MITI","LTI"):
					mbo.setFieldFlag("APAHSEMTRDETAIL",MboConstants.READONLY,False)
					mbo.setFieldFlag("APAHSEMTRDETAIL",MboConstants.REQUIRED,True)
				else:
					mbo.setFieldFlag("APAHSEMTRDETAIL",MboConstants.READONLY,True)
					mbo.setFieldFlag("APAHSEMTRDETAIL",MboConstants.REQUIRED,False)

				mbo.setFieldFlag("APAHSEENVNEARMISS",MboConstants.READONLY,True)
				
				# End if for WHS Classification
			# End if for HSE Incidents
		else:
			# classification is Operational for OIM incidents
			if not mbo.isNew():
				# DMND0002352 Reclassify HSE and OIM incidents
				# Mbo needs to be opened up for HSEADVISORS so they can steal the OP's Incident if it was incorrectly classified
				GroupUserSet = mbo.getMboSet("$groupUser","GROUPUSER","groupname in ('HSEADVISOR') and userid=:&USERNAME& ")
				GroupUserSet.reset()
				if not GroupUserSet.isEmpty():
					#Reverse the logic that made the mbo readonly when its in the WF and its Operational. We keep the WF condition as is to restrict HSE Advisors changing info that is not in their jurisdiction. 
					mbo.setFlag(MboConstants.READONLY, False)
				GroupUserSet = mbo.getMboSet("$groupUser","GROUPUSER","groupname in ('HSEHEAD','MAXADMIN','HSEADVISOR','OIMADMIN','BUSINESSADM') and userid=:&USERNAME& ")
				GroupUserSet.reset()
				if GroupUserSet.isEmpty() or status not in ("NEW","WOR"):
					# Set classification readonly because it's in the workflow
					mbo.setFieldFlag("CLASSSTRUCTURE.HIERARCHYPATH",MboConstants.READONLY,True)

			# Once an investigation is created or the incident is closed, the Initial Risk Rating and Actual Risk Rating will need to be made read-only for Operational incidents.
			investigationSet = mbo.getMboSet("$Investigations","PROBLEM","origrecordid = :ticketid and origrecordclass = 'INCIDENT' and status <> 'REJECTED'")
			if status in ("CLOSED","RESOLVED","COMPLETED","REJECTED") or not investigationSet.isEmpty():
				mbo.setFieldFlag("APA_ACTUALRISK",MboConstants.READONLY,True)
				mbo.setFieldFlag("APA_INITIALRISK",MboConstants.READONLY,True)
				
			mbo.setFieldFlag("APAHSESUBLOCATION",MboConstants.REQUIRED,False)
			mbo.setFieldFlag("ASSETSITEID",MboConstants.READONLY,False)
			mbo.setFieldFlag("APAHSEAFFECTEDPRSTYPE",MboConstants.REQUIRED,False)
			# DMND0003542 OIM dont want the record locked out when its under Review.
			if status in ("WOR"):
				mbo.setFieldFlag("APAHSESMSNUM",MboConstants.READONLY,True)
			if status in ("INPROG","RESOLVED"):
				mbo.setFieldFlag("APAHSESMSNUM",MboConstants.READONLY,True)
			if status in ("COMPLETED"):
				mbo.setFieldFlag("DESCRIPTION_LONGDESCRIPTION", MboConstants.READONLY, True)
				mbo.setFieldFlag("CLASSSTRUCTUREID",MboConstants.READONLY,True)
				mbo.setFieldFlag("CLASSSTRUCTURE.HIERARCHYPATH",MboConstants.READONLY,True)
				mbo.setFieldFlag("APAHSEINCRATING",MboConstants.READONLY,True)
				mbo.setFieldFlag("APAHSESMSNUM",MboConstants.READONLY,True)
				setInvestigationDetailsReadonly()
			
		# end if for app APAHSEINC
elif app == "APAHSECR":
	# The fields Classification, HSE Management System and Location are mandatory.
	mbo.setFieldFlag("CLASSSTRUCTURE.HIERARCHYPATH",MboConstants.REQUIRED,True)
	
	# The Location and HSE Management System fields are mandatory
	mbo.setFieldFlag("LOCATION",MboConstants.REQUIRED,True)
	mbo.setFieldFlag("APAHSESMSNUM",MboConstants.REQUIRED,True)
	
	# Every new record added will have a ticket class defined as CUSTOM
	if mbo.isNew():
		#mbo.setValue("CLASS","CUSTOM",MboConstants.NOACCESSCHECK)
		
		# The field Reported By will be read-only and automatically populated with details of user creating the custom register 
		mbo.setValue("REPORTEDBYID",mbo.getUserInfo().getPersonId())
		# By default maximo will copy reportedby to the affectedperson, which in turn will cross over current user's site defined in the people application
		# to the CRs site - suppress it by cleaning affected personid and site (we are in new record)
		mbo.setValueNull("AFFECTEDPERSONID",MboConstants.NOACCESSCHECK)
		mbo.setValueNull("ASSETSITEID",MboConstants.NOACCESSCHECK)
		clearWorkAreaDomain()
	else:
		loadWorkAreaDomain()
elif app == "APAHSEHR":
	
	mbo.setFieldFlag("CLASSSTRUCTURE.HIERARCHYPATH",MboConstants.REQUIRED,True)
	if mbo.isNew():
		mbo.setValue("REPORTEDBYID",mbo.getUserInfo().getPersonId())
		mbo.setFieldFlag("VENDOR",MboConstants.READONLY,True)
		# For HR app, this field needs to be pre-populated with EMPLOYEE
		mbo.setFieldFlag("APAHSEAFFECTEDPRSTYPE",MboConstants.REQUIRED,True)
		mbo.setValue("APAHSEAFFECTEDPRSTYPE","EMPLOYEE")
		mbo.setFieldFlag("APAHSEPRINCIPLECONTRACTOR",MboConstants.READONLY,True)
		clearWorkAreaDomain()
	else:
		afType = mbo.getString("APAHSEAFFECTEDPRSTYPE")
		
		if len(afType)>0:
			if afType == "EMPLOYEE":
				mbo.setFieldFlag("VENDOR",MboConstants.READONLY,True)
				mbo.setFieldFlag("VENDOR",MboConstants.REQUIRED,False)
				mbo.setFieldFlag("APAHSEPRINCIPLECONTRACTOR",MboConstants.READONLY,True)
			elif afType in("CONTRACT","SUBCONT"):
				mbo.setFieldFlag("VENDOR",MboConstants.READONLY,False)
				mbo.setFieldFlag("VENDOR",MboConstants.REQUIRED,True)
				mbo.setFieldFlag("APAHSEPRINCIPLECONTRACTOR",MboConstants.READONLY,False)
				mbo.setFieldFlag("APAHSEPRINCIPLECONTRACTOR",MboConstants.REQUIRED,True)
			elif afType in("VISITOR","PUBLIC"):
				mbo.setFieldFlag("VENDOR",MboConstants.READONLY,True)
				mbo.setFieldFlag("VENDOR",MboConstants.REQUIRED,False)
				mbo.setFieldFlag("APAHSEPRINCIPLECONTRACTOR",MboConstants.READONLY,True)
		else:
			mbo.setFieldFlag("VENDOR",MboConstants.READONLY,True)
			mbo.setFieldFlag("VENDOR",MboConstants.REQUIRED,False)
			mbo.setFieldFlag("APAHSEPRINCIPLECONTRACTOR",MboConstants.READONLY,True)
		#mgiang@au1.ibm.com Make the APAHSEHAZARDSAFE checkbox readonly if the record had been already saved
		mbo.setFieldFlag("APAHSEHAZARDSAFE",MboConstants.READONLY,True)
		loadWorkAreaDomain()
		#Restrict the HR record when its in the workflow for approval
		if mbo.evaluateCondition("APAHSE_INCWF"):
			mbo.setFlag(MboConstants.READONLY, True)
			if mbo.getString("REPORTEDBYID") == mbo.getUserInfo().getPersonId():
				mbo.setFlag(MboConstants.NOADD,False)
	
	# The Identified/Incident Date field is mandatory
	mbo.setFieldFlag("APAHSEINCIDENTDATE",MboConstants.REQUIRED,True)
	
	# Incident summary and details are mandatory
	mbo.setFieldFlag("DESCRIPTION",MboConstants.REQUIRED,True)
	mbo.setFieldFlag("DESCRIPTION_LONGDESCRIPTION",MboConstants.REQUIRED,True)
	
	# The Location and HSE Management System fields are mandatory
	mbo.setFieldFlag("LOCATION",MboConstants.REQUIRED,True)
	mbo.setFieldFlag("APAHSESMSNUM",MboConstants.REQUIRED,True)
	
	# The Consequence field is mandatory
	mbo.setFieldFlag("APAHSECONSQNUM",MboConstants.REQUIRED,True)
	mbo.setFieldFlag("APAHSELIKELIHOOD",MboConstants.REQUIRED,True)
	
	if (mbo.getString("APAHSESMSNUM") == "ID"):
		mbo.setFieldFlag("APAHSEPROJECTCODE",MboConstants.REQUIRED,True)