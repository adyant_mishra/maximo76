##########################################################
# Copyright IBM Corp. 2014
#
# APA HSE Project
#
# Script APAHSE_OBJ_PROBLEM_INIT
# Authors: acherem@au1.ibm.com
# ddepaula@au1.ibm.com
# mgiang@au1.ibm.com
##########################################################

# Script set editability of fields depending on values of other fields

from psdi.mbo import MboConstants
from java.util import Calendar

def setAssetStrikeReadOnlyFlags(flagValue):
	mbo.setFieldFlag("APAHSESTRIKEGAS",MboConstants.READONLY,flagValue)
	mbo.setFieldFlag("APAHSESTRIKEELEC",MboConstants.READONLY,flagValue)
	mbo.setFieldFlag("APAHSESTRIKETEL",MboConstants.READONLY,flagValue)
	mbo.setFieldFlag("APAHSESTRIKEWAT",MboConstants.READONLY,flagValue)
	mbo.setFieldFlag("APAHSESTRIKESEW",MboConstants.READONLY,flagValue)
	mbo.setFieldFlag("APAHSESTRIKEOTH",MboConstants.READONLY,flagValue)
	mbo.setFieldFlag("APAHSESTRIKEDETAIL",MboConstants.READONLY,flagValue)
	mbo.setFieldFlag("APAHSEDEVICEUSED",MboConstants.READONLY,flagValue)
def setLossOfContainmentReadOnlyFlags(flagValue):
	mbo.setFieldFlag("APAHSELIQUIDVAL",MboConstants.READONLY,flagValue)
	mbo.setFieldFlag("APAHSEGASESVAL",MboConstants.READONLY,flagValue)
	mbo.setFieldFlag("APAHSESOLIDSVAL",MboConstants.READONLY,flagValue)
	mbo.setFieldFlag("APAHSELOSSCONT",MboConstants.READONLY,flagValue)
	# Additonal Fields 
if app == "APAHSEINV":
	#Set fields to required in case users are creating ad-hoc investigations (restricted to only few groups/users).
	mbo.setFieldFlag("CLASSSTRUCTURE.HIERARCHYPATH", MboConstants.REQUIRED, True)
	mbo.setFieldFlag("LOCATION", MboConstants.REQUIRED, True)
	mbo.setFieldFlag("APAHSESMSNUM", MboConstants.REQUIRED, True)
	mbo.setFieldFlag("DESCRIPTION", MboConstants.REQUIRED, True)
	mbo.setFieldFlag("DESCRIPTION_LONGDESCRIPTION", MboConstants.REQUIRED, True)
	mbo.setFieldFlag("PLUSGINVESTLEAD", MboConstants.REQUIRED, True)

	if (not mbo.isNull("APAHSEISRISKFAIL")) and (not mbo.getBoolean("APAHSEISRISKFAIL")):
		mbo.setFieldFlag("APAHSERISKFAILDET", MboConstants.READONLY, True)
		mbo.setFieldFlag("APAHSERISKFAILDET_LONGDESCRIPTION", MboConstants.READONLY, True)
	if (not mbo.isNull("APAHSEISCONTROLFAIL")) and (not mbo.getBoolean("APAHSEISCONTROLFAIL")):
		mbo.setFieldFlag("APAHSECONTROLFAILDET", MboConstants.READONLY, True)
		mbo.setFieldFlag("APAHSECONTROLFAILDET_LONGDESCRIPTION", MboConstants.READONLY, True)
	if (not mbo.isNull("APAHSEISASSFAIL")) and (not mbo.getBoolean("APAHSEISASSFAIL")):
		mbo.setFieldFlag("APAHSEASSFAILDET", MboConstants.READONLY, True)
		mbo.setFieldFlag("APAHSEASSFAILDET_LONGDESCRIPTION", MboConstants.READONLY, True)

# If APAHSEISCONTROLFAIL is not ticked, all fields in the Control Failure Additional Details area should be disabled
	if mbo.isNew() or not mbo.getBoolean("APAHSEISCONTROLFAIL"):
		allControlFields = ["APAHSEFAILCONT1REASON","APAHSEFAILCONT2REASON","APAHSEFAILCONT3REASON",
			"APAHSEFAILCONT1COM","APAHSEFAILCONT2COM","APAHSEFAILCONT3COM" ]
		for index in allControlFields:
			mbo.setFieldFlag(index,MboConstants.READONLY,True)
		# Clear the work area domain, by default it loads the table domain with everything merged
		listener = mbo.getMboValue("APAHSEWORKAREA").getMboValueInfo().getDomainInfo().getDomainObject(mbo.getMboValue("APAHSEWORKAREA"))
		where = "1=0"
		if listener is not None:
			listener.setListCriteria(where)
			listener.getList().setWhere(where)
		setAssetStrikeReadOnlyFlags(True)
		mbo.setFieldFlag("APAHSEELECVOLT",MboConstants.READONLY,True)
		mbo.setFieldFlag("APAHSEGASPRESS",MboConstants.READONLY,True)		
		# Make readonly fields for Loss of Containment section field selection, If its an New record
		setLossOfContainmentReadOnlyFlags(True)
	else:
		controlMap = {"APAHSEFAILCONT1":["APAHSEFAILCONT1REASON","APAHSEFAILCONT1COM"],
			"APAHSEFAILCONT2":["APAHSEFAILCONT2REASON","APAHSEFAILCONT2COM"],
			"APAHSEFAILCONT3":["APAHSEFAILCONT3REASON","APAHSEFAILCONT3COM"] }
		
		for index in controlMap.keys():
			control = mbo.getString(index)
			if len(control) == 0:
				for index2 in controlMap[index]:
					mbo.setFieldFlag(index2,MboConstants.READONLY,True)
#					If failure control is not entered, lookup for the corresponding reason will return nothing
					where = "1 = 0";
			else:
#				Failure control is entered, need to set relevant domain
# 				Map to find a required domain id based on the value in the Failure Code field
				domainMap = {"PROCESS":"APAHSEFAILCONTRPR",
					"PEOPLE":"APAHSEFAILCONTRPL",
					"PLANT AND EQUIPMENT":"APAHSEFAILCONTRPE"}
				where = "domainid = '" + domainMap[control] + "'";
			listener = mbo.getMboValue(controlMap[index][0]).getMboValueInfo().getDomainInfo().getDomainObject(mbo.getMboValue(controlMap[index][0]))
			if listener is not None:
				listener.setListCriteria(where)
				listener.getList().setWhere(where)
	if not mbo.isNew():
		domainMapWork = {"ID":"APAHSEIDWORKAREA",
				"NETWORKS":"APAHSENETWORKAREA",
				"TRANSMISSION":"APAHSETRANWORKAREA",
				"POWER ASSETS":"APAHSEPOWWORKAREA",
				"CORPORATE":"NONE"
				}
		listener = mbo.getMboValue("APAHSEWORKAREA").getMboValueInfo().getDomainInfo().getDomainObject(mbo.getMboValue("APAHSEWORKAREA"))
		smsnum = mbo.getString("APAHSESMSNUM")
		if smsnum is not None and smsnum != "":
			where = "domainid = '" + domainMapWork[smsnum] + "'"
			if smsnum in ("ID","NETWORKS","TRANSMISSION","POWER ASSETS"):
				mbo.setFieldFlag("APAHSEWORKAREA",MboConstants.REQUIRED,True)
		elif smsnum is None or smsnum == "":
			where = "1=0"
		if listener is not None:
			listener.setListCriteria(where)
			listener.getList().setWhere(where)
		if (mbo.getBoolean("APAHSEASSETSTRIKE")):
			setAssetStrikeReadOnlyFlags(False)
			mbo.setFieldFlag("APAHSESTRIKEDETAIL",MboConstants.REQUIRED,True)
			mbo.setFieldFlag("APAHSEDEVICEUSED",MboConstants.REQUIRED,True)
			if (mbo.getBoolean("APAHSESTRIKEELEC")):
				mbo.setFieldFlag("APAHSEELECVOLT",MboConstants.REQUIRED,True)
				mbo.setFieldFlag("APAHSEELECVOLT",MboConstants.READONLY,False)
			else:
				mbo.setFieldFlag("APAHSEELECVOLT",MboConstants.READONLY,True)
			if (mbo.getBoolean("APAHSESTRIKEGAS")):
				mbo.setFieldFlag("APAHSEGASPRESS",MboConstants.REQUIRED,True)
				mbo.setFieldFlag("APAHSEGASPRESS",MboConstants.READONLY,False)
			else:
				mbo.setFieldFlag("APAHSEGASPRESS",MboConstants.READONLY,True)
		else:
			setAssetStrikeReadOnlyFlags(True)
			mbo.setFieldFlag("APAHSEELECVOLT",MboConstants.READONLY,True)
			mbo.setFieldFlag("APAHSEGASPRESS",MboConstants.READONLY,True)
		if (mbo.getBoolean("APAHSEDBYDAVAIL") or mbo.getBoolean("APAHSEDBYDNOTAVAIL") or mbo.getBoolean("APAHSEDBYDINCORRECT") or mbo.getBoolean("APAHSEDBYDNOTUSED") or mbo.getBoolean("APAHSEDBYDOUTDATE")):
				mbo.setFieldFlag("APAHSEDBYDDETAIL",MboConstants.REQUIRED,True)
		else:
				mbo.setFieldFlag("APAHSEDBYDDETAIL",MboConstants.REQUIRED,False)
		# Make readonly fields for Loss of Containment section field selection, If it is not New record		
		#Start
		if (mbo.getBoolean("APAHSELIQUID") or mbo.getBoolean("APAHSEGASES") or mbo.getBoolean("APAHSESOLIDS")):
			mbo.setFieldFlag("APAHSELOSSCONT",MboConstants.READONLY,False)
		else:
			mbo.setFieldFlag("APAHSELOSSCONT",MboConstants.READONLY,True)	
			
		if mbo.getBoolean("APAHSELIQUID"):
			mbo.setFieldFlag("APAHSELIQUIDVAL",MboConstants.REQUIRED,True)
			mbo.setFieldFlag("APAHSELIQUIDVAL",MboConstants.READONLY,False)
			#mbo.setFieldFlag("APAHSEGASESVAL",MboConstants.READONLY,True)
			#mbo.setFieldFlag("APAHSESOLIDSVAL",MboConstants.READONLY,True)
		else:
			mbo.setFieldFlag("APAHSELIQUIDVAL",MboConstants.REQUIRED,False)
			
		if mbo.getBoolean("APAHSEGASES"):
			mbo.setFieldFlag("APAHSEGASESVAL",MboConstants.REQUIRED,True)
			mbo.setFieldFlag("APAHSEGASESVAL",MboConstants.READONLY,False)
			#mbo.setFieldFlag("APAHSESOLIDSVAL",MboConstants.READONLY,True)
			#mbo.setFieldFlag("APAHSELIQUIDVAL",MboConstants.READONLY,True)
		else:
			mbo.setFieldFlag("APAHSEGASESVAL",MboConstants.READONLY,True)
			
		if mbo.getBoolean("APAHSESOLIDS"):	
			mbo.setFieldFlag("APAHSESOLIDSVAL",MboConstants.REQUIRED,True)
			mbo.setFieldFlag("APAHSESOLIDSVAL",MboConstants.READONLY,False)
			#mbo.setFieldFlag("APAHSELIQUIDVAL",MboConstants.READONLY,True)
			#mbo.setFieldFlag("APAHSEGASESVAL",MboConstants.READONLY,True)
		else:
			mbo.setFieldFlag("APAHSESOLIDSVAL",MboConstants.READONLY,True)
		#End	
					
		# Need to filter out Assets for Near Miss solution was originally done on the Page's relationship, however a side affect was that new records did not show any attributes upon Classification selection.
		# even with the original whereClause set, any new relationship defined did not load any new Attributes on a new MBO
		if mbo.getString("PLUSGINCTYPE") == "NM":
			ticketSpecMboSet = mbo.getMboSet("TICKETSPECCLASS")
			ticketSpecMboSet.setWhere("assetattrid not like ('NAT%') and assetattrid not like ('%BD_PART%')")
			ticketSpecMboSet.reset()
	#Calculate the Investigation Due date based on the days specified in the Rules Matrix
	if not mbo.isNull("REPORTDATE"):
		reportdate = mbo.getDate("REPORTDATE")
		cal_duedate = Calendar.getInstance()
		cal_duedate.setTime(reportdate)
		# This needs to be done regardless from an app or not, and from the Escalation
		if mbo.getInt("APAHSE_INV_DUE_DAYS.OUTPUT1") > 0:
			cal_duedate.add(Calendar.DATE, mbo.getInt("APAHSE_INV_DUE_DAYS.OUTPUT1"))
			mbo.setValue("APAHSEDUEDATE",cal_duedate.getTime(), MboConstants.NOACCESSCHECK)
	if not mbo.isNew() and mbo.evaluateCondition("APAHSESENSP1"):
		# For Sensitive Incidents we need to hide the information from view
		mbo.setFlag(MboConstants.DISCARDABLE, True)
		# Requirement was to replace the description with descriptive text
		mbo.setValue("DESCRIPTION","This is a Sensitive Investigation and is hidden from view",MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
		# fields have been hidden using sigoptions, but on the list tab they need to be cleared out
		mbo.setValueNull("CLASSSTRUCTUREID",MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
		mbo.setValueNull("LOCATION",MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
		# Investigation lead name still showed on the List tab, when I changed it to the one that gets cleared on the main page, the filter option went missing
		mbo.setValueNull("PLUSGINVESTLEAD",MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
		mbo.setValueNull("APAHSESMSNUM",MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
		# These fields need to be cleared there is an opportunity for the fields to be shown when a Communication template is selected to be sent on the record. The data restriction didnt hide these from the commtemplates
		mbo.setValueNull("APAHSEINCRATING",MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
		mbo.setValueNull("REPORTDATE",MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
		mbo.setValueNull("APAHSEWORKAREA",MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
		incidentSet = mbo.getMboSet("ORIGTICKET")
		incidentSet.setFlag(MboConstants.DISCARDABLE, True)
		if not incidentSet.isEmpty(): 
			incidentMbo = incidentSet.getMbo(0)
			incidentMbo.setFlag(MboConstants.DISCARDABLE, True)
			incidentMbo.setValueNull("APAHSEINCIDENTDATE", MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
			incidentMbo.setValueNull("APAHSESUBLOCATION", MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
			incidentMbo.setValueNull("APAHSEAFFECTEDPRSTYPE", MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
			incidentMbo.setValueNull("VENDOR", MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
			incidentMbo.setValueNull("APAHSEREGULATORNOTIFIED", MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
			incidentMbo.setValue("PLUSGREPORTABLE","0",MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
		# These need to be hidden, security restrictions intermitently didnt work along side this autoscript
		mbo.getMboValue("STATUS").setFlag(MboConstants.HIDDEN, True)
		mbo.getMboValue("APAHSEESCALATIONACTIVE").setFlag(MboConstants.HIDDEN, True)
		mbo.getMboValue("APAHSELOCKATTACHMENTS").setFlag(MboConstants.HIDDEN, True)
		mbo.getMboValue("PLUSGWHATACTUAL").setFlag(MboConstants.HIDDEN, True)
		mbo.getMboValue("APAHSEINVTEAMDETAILS").setFlag(MboConstants.HIDDEN, True)
		mbo.getMboValue("APAHSETRAININGPROVIDED").setFlag(MboConstants.HIDDEN, True)
		mbo.getMboValue("APAHSETIMEROLE").setFlag(MboConstants.HIDDEN, True)
		# These fields in the "Dates" section need to be hidden, because the sigoption on the Dates section on the UI did not work. Tried on every part of the section and nothing worked with the sigoption that is already working for the other tabs.
		mbo.getMboValue("CHANGEDATE").setFlag(MboConstants.HIDDEN, True)
		mbo.getMboValue("APAHSEDUEDATE").setFlag(MboConstants.HIDDEN, True)
		mbo.getMboValue("CHANGEBY").setFlag(MboConstants.HIDDEN, True)
		# INC0062930 - Need to hide the Description's longdescription
		mbo.getMboValue("DESCRIPTION_LONGDESCRIPTION").setFlag(MboConstants.HIDDEN, True)
