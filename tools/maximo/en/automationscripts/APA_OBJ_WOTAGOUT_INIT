#description=This script sets editablity based on whether the Removed attribute is ticked or not
#language=jython
#log=DEBUG
#name=APA_OBJ_WOTAGOUT_INIT
#status=Active
#launchpoint.1.active=1
#launchpoint.1.description=This script sets editablity based on whether the Removed attribute is ticked or not
#launchpoint.1.event=1
#launchpoint.1.name=APA_OBJ_WOTAGOUT_INIT
#launchpoint.1.object=APA_WOTAGOUT
#launchpoint.1.type=OBJECT
##

##########################################################
# Copyright IBM Corp. 2015
#
# APA EAM Project
#
# Script APA_OBJ_WOTAGOUT_INIT
# Author: mgiang@au1.ibm.com
##########################################################

# This script sets editablity based on whether the Removed attribute is ticked or not
# also makes attributes readonly after Applied or Removed details entered.

from psdi.mbo import MboConstants

if mbo.isNew() or (mbo.getString("APPLIEDDATE") is None or mbo.getString("APPLIEDDATE")=="" or mbo.getString("APPLIEDBY") is None or mbo.getString("APPLIEDBY")=="" ):
	mbo.setFieldFlag("APPLIEDBY",MboConstants.READONLY,False)
	mbo.setFieldFlag("APPLIEDDATE",MboConstants.READONLY,False)
	mbo.setFieldFlag("APPLIEDBY",MboConstants.REQUIRED,True)
	mbo.setFieldFlag("APPLIEDDATE",MboConstants.REQUIRED,True)
else:
	if (mbo.getString("APPLIEDDATE") is not None and mbo.getString("APPLIEDBY") is not None):
		mbo.setFieldFlag("APPLIEDBY",MboConstants.READONLY,True)
		mbo.setFieldFlag("APPLIEDDATE",MboConstants.READONLY,True)
		mbo.setFieldFlag("APPLIEDBY",MboConstants.REQUIRED,False)
		mbo.setFieldFlag("APPLIEDDATE",MboConstants.REQUIRED,False)
		mbo.setFieldFlag("TAGID",MboConstants.READONLY,True)
		mbo.setFieldFlag("TAGTYPE",MboConstants.READONLY,True)
		mbo.setFieldFlag("DESCRIPTION",MboConstants.READONLY,True)
		mbo.setFieldFlag("TAGID",MboConstants.REQUIRED,False)
		mbo.setFieldFlag("TAGTYPE",MboConstants.REQUIRED,False)
		mbo.setFieldFlag("DESCRIPTION",MboConstants.REQUIRED,False)

if mbo.getString("FGEQP") is None or mbo.getString("FGEQP")=="":
	mbo.setFieldFlag("RISKASSESSMENT",MboConstants.READONLY,True)
	mbo.setFieldFlag("EXPREMDATE",MboConstants.READONLY,True)
else:
	if mbo.getBoolean("FGEQP"):
		mbo.setFieldFlag("RISKASSESSMENT",MboConstants.READONLY,False)
		mbo.setFieldFlag("EXPREMDATE",MboConstants.READONLY,False)
		mbo.setFieldFlag("EXPREMDATE",MboConstants.REQUIRED,True)
	else:
		mbo.setFieldFlag("RISKASSESSMENT",MboConstants.READONLY,True)
		mbo.setFieldFlag("EXPREMDATE",MboConstants.READONLY,True)
		mbo.setFieldFlag("EXPREMDATE",MboConstants.REQUIRED,False)

if mbo.getString("REMOVED") is None or mbo.getString("REMOVED")=="":
	mbo.setFieldFlag("REMOVED",MboConstants.READONLY,False)
	mbo.setFieldFlag("REMOVEDBY",MboConstants.READONLY,True)
	mbo.setFieldFlag("REMOVEDDATE",MboConstants.READONLY,True)
	mbo.setFieldFlag("REMOVEDWO",MboConstants.READONLY,True)
	mbo.setFieldFlag("REMOVEDBY",MboConstants.REQUIRED,False)
	mbo.setFieldFlag("REMOVEDDATE",MboConstants.REQUIRED,False)
else:
	if mbo.getBoolean("REMOVED") and (mbo.getString("REMOVEDDATE") is None or mbo.getString("REMOVEDBY") is None):
		mbo.setFieldFlag("REMOVEDBY",MboConstants.READONLY,False)
		mbo.setFieldFlag("REMOVEDDATE",MboConstants.READONLY,False)
		mbo.setFieldFlag("REMOVEDWO",MboConstants.READONLY,False)
		mbo.setFieldFlag("REMOVEDDATE",MboConstants.REQUIRED,True)
		mbo.setFieldFlag("REMOVEDBY",MboConstants.REQUIRED,True)
	else:
		mbo.setFieldFlag("REMOVEDBY",MboConstants.READONLY,True)
		mbo.setFieldFlag("REMOVEDDATE",MboConstants.READONLY,True)
		mbo.setFieldFlag("REMOVEDWO",MboConstants.READONLY,True)
		mbo.setFieldFlag("REMOVEDBY",MboConstants.REQUIRED,False)
		mbo.setFieldFlag("REMOVEDDATE",MboConstants.REQUIRED,False)

	if mbo.getBoolean("REMOVED") and mbo.getString("REMOVEDDATE") is not None and mbo.getString("REMOVEDBY") is not None:
		mbo.setFieldFlag("REMOVED",MboConstants.READONLY,True)
		mbo.setFieldFlag("REMOVEDBY",MboConstants.READONLY,True)
		mbo.setFieldFlag("REMOVEDDATE",MboConstants.READONLY,True)
		mbo.setFieldFlag("REMOVEDWO",MboConstants.READONLY,True)
		mbo.setFieldFlag("REMOVEDBY",MboConstants.REQUIRED,False)
		mbo.setFieldFlag("REMOVEDDATE",MboConstants.REQUIRED,False)
		mbo.setFieldFlag("FGEQP",MboConstants.READONLY,True)
		mbo.setFieldFlag("RISKASSESSMENT",MboConstants.READONLY,True)
		mbo.setFieldFlag("EXPREMDATE",MboConstants.READONLY,True)
		mbo.setFieldFlag("FGEQP",MboConstants.REQUIRED,False)
		mbo.setFieldFlag("RISKASSESSMENT",MboConstants.REQUIRED,False)
		mbo.setFieldFlag("EXPREMDATE",MboConstants.REQUIRED,False)