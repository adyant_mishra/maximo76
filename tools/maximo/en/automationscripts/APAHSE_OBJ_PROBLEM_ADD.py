##########################################################
# Copyright IBM Corp. 2014
#
# APA HSE Project
#
# APAHSE_OBJ_PROBLEM_ADD
# Authors:	mgiang@au1.ibm.com
# ddepaula@au1.ibm.com
##########################################################
from psdi.mbo import MboConstants
from psdi.plusg.util import PlusGUtil

print "In APAHSE_OBJ_PROBLEM_ADD"

util = PlusGUtil();

if app is not None and app =="APAHSEINV":
   mbo.setValue("PLUSGISINVESTIG", 1, MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
   mbo.setValue("CLASS", util.getExternalValue("TKCLASS", "INVESTIGATION", mbo), MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)

origrecordclass = mbo.getMboValue("origrecordclass").getString();

#incidentmbo = mbo.getMboValue("origrecordclass").getMbo().getOwner();
incidentid = mbo.getMboValue("ORIGRECORDID").getString();

if origrecordclass == "INCIDENT" and incidentid is not None:

		incidentSet = mbo.getMboSet("incidentname","INCIDENT","ticketid=:ORIGRECORDID");
		incidentSet.setWhere("TICKETID="+incidentid);
		incidentSet.reset()
		
		incidentmbo = incidentSet.getMbo(0)
		print incidentmbo.getString("ticketid")

		#Get the values from the Incident object and copy them across, the other fields were already copied across out of the box
		managSystemId = incidentmbo.getString("APAHSEMGTSYSTEMID");
		managSystemNum = incidentmbo.getString("APAHSESMSNUM"); 
		incidenttype = incidentmbo.getString("plusginctype");
		investigationLead = incidentmbo.getString("plusginvestlead");
		investigationLeadName = incidentmbo.getString("plusginvleadname");
		site = incidentmbo.getString("assetsiteid");
		assetLocsite = incidentmbo.getString("assetsiteid");
		projectcode = incidentmbo.getString("APAHSEPROJECTCODE");
		incRating = incidentmbo.getString("APAHSEINCRATING");
		workarea = incidentmbo.getString("APAHSEWORKAREA");
		assetStrike = incidentmbo.getBoolean("APAHSEASSETSTRIKE");
		assetStrikeGas = incidentmbo.getBoolean("APAHSESTRIKEGAS");
		assetStrikeElec = incidentmbo.getBoolean("APAHSESTRIKEELEC");
		assetStrikeTel = incidentmbo.getBoolean("APAHSESTRIKETEL");
		assetStrikeWat = incidentmbo.getBoolean("APAHSESTRIKEWAT");
		assetStrikeSew = incidentmbo.getBoolean("APAHSESTRIKESEW");
		assetStrikeOth = incidentmbo.getBoolean("APAHSESTRIKEOTH");
		assetStrikeDet = incidentmbo.getString("APAHSESTRIKEDETAIL");
		voltage = incidentmbo.getString("APAHSEELECVOLT");
		pressure = incidentmbo.getString("APAHSEGASPRESS");
		deviceused = incidentmbo.getString("APAHSEDEVICEUSED");
		liquid = incidentmbo.getString("APAHSELIQUID");
		gases = incidentmbo.getString("APAHSEGASES");
		solids	= incidentmbo.getString("APAHSESOLIDS");
		liquidVal = incidentmbo.getString("APAHSELIQUIDVAL");
		gasesVal = incidentmbo.getString("APAHSEGASESVAL");
		solidsVal = incidentmbo.getString("APAHSESOLIDSVAL");
		lossContDet = incidentmbo.getString("APAHSELOSSCONT");
		mbo.setValue("APAHSEMGTSYSTEMID",managSystemId,MboConstants.NOACCESSCHECK)
		mbo.setValue("APAHSESMSNUM",managSystemNum,MboConstants.NOACCESSCHECK)
		mbo.setValue("plusginctype",incidenttype,MboConstants.NOACCESSCHECK)
		mbo.setValue("plusginvestlead",investigationLead,MboConstants.NOACCESSCHECK)
		mbo.setValue("plusginvleadname",investigationLeadName,MboConstants.NOACCESSCHECK)
		mbo.setValue("siteid", site, MboConstants.NOACCESSCHECK)
		mbo.setValue("assetsiteid", assetLocsite, MboConstants.NOACCESSCHECK)
		#Projectcode is copied for Reporting efficiency because it is required as an Input parameter for Investigation based reports
		mbo.setValue("APAHSEPROJECTCODE", projectcode, MboConstants.NOACCESSCHECK)
		# No Action to avoid infinite looping
		mbo.setValue("APAHSEINCRATING", incRating, MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
		mbo.setValue("APAHSEWORKAREA", workarea, MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
		mbo.setValue("APAHSEASSETSTRIKE", assetStrike, MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
		mbo.setValue("APAHSESTRIKEGAS", assetStrikeGas, MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
		mbo.setValue("APAHSESTRIKEELEC", assetStrikeElec, MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
		mbo.setValue("APAHSESTRIKETEL", assetStrikeTel, MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
		mbo.setValue("APAHSESTRIKEWAT", assetStrikeWat, MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
		mbo.setValue("APAHSESTRIKESEW", assetStrikeSew, MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
		mbo.setValue("APAHSESTRIKEOTH", assetStrikeOth, MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
		mbo.setValue("APAHSESTRIKEDETAIL", assetStrikeDet, MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
		mbo.setValue("APAHSEELECVOLT", voltage, MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
		mbo.setValue("APAHSEGASPRESS", pressure, MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
		mbo.setValue("APAHSEDEVICEUSED", deviceused, MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
		mbo.setValue("APAHSELIQUID", liquid, MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
		mbo.setValue("APAHSEGASES", gases, MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
		mbo.setValue("APAHSESOLIDS", solids, MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
		mbo.setValue("APAHSELIQUIDVAL", liquidVal, MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
		mbo.setValue("APAHSEGASESVAL", gasesVal, MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
		mbo.setValue("APAHSESOLIDSVAL", solidsVal, MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
		mbo.setValue("APAHSELOSSCONT", lossContDet, MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)