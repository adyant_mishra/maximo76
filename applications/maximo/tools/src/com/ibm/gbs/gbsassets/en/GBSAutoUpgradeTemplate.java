/**
* IBM Confidential
*
* OCO Source Materials
*
* (C) COPYRIGHT IBM CORP. 2013
*
* The source code for this program is not published or otherwise
* divested of its trade secrets, irrespective of what has been
* deposited with the U.S. Copyright Office.
*
*/
package com.ibm.gbs.gbsassets.en;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.io.StringReader;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import java.util.Scanner;

import psdi.script.industrysolution.AutoUpgradeTemplateIS;

/**
 * Extension Auto Upgrade Template to load Automation Scripts.
 * 
 * @author yuvaraj.anandakumar@au1.ibm.com
 *
 */
public class GBSAutoUpgradeTemplate extends AutoUpgradeTemplateIS {

	/**
	 * Constructor
	 * 
	 * @param paramConnection
	 * @throws Exception
	 */
	public GBSAutoUpgradeTemplate(Connection paramConnection) throws Exception
	{
		this(paramConnection, null, null);
	}

	/**
	 * Constructor
	 * 
	 * @param paramConnection
	 * @param paramPrintStream
	 * @throws Exception
	 */
	public GBSAutoUpgradeTemplate(Connection paramConnection, PrintStream paramPrintStream) throws Exception
	{
		this(paramConnection, null, paramPrintStream);
	}

	/**
	 * Constructor
	 * 
	 * @param paramConnection
	 * @param paramHashMap
	 * @param paramPrintStream
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public GBSAutoUpgradeTemplate(Connection paramConnection, HashMap paramHashMap, PrintStream paramPrintStream)
			throws Exception

	{
		super(paramConnection, paramHashMap, paramPrintStream);
	}

	/**
	 * Method used to load automation scripts from specific directory and sub
	 * directories
	 * 
	 * @return void
	 * @throws Exception
	 * 
	 */
	public void loadAutomationScripts() throws Exception {
		// Find the rootfolder
		// File rootFolder = new
		// File("en/"+getProductDirFromClass()+"/automationscripts");
		// using common folder
		File rootFolder = new File("en/automationscripts");

		// Create an array list
		ArrayList<File> listofFiles = new ArrayList<File>();

		// Load the list with all automation script files from directory and sub
		// directories
		loadAllFiles(rootFolder, listofFiles);

		// Process loaded files
		processFiles(listofFiles);
	}

	/**
	 * This method is used to process all the files in the array list.
	 * Processing includes parsing the file into properties and executing insert
	 * statements to insert the automation scripts, variables and launch point
	 * information into Maximo
	 * 
	 * @param listofFiles
	 * @throws Exception
	 */
	private void processFiles(ArrayList<File> listofFiles) throws Exception {

		// Loop through the list of files
		for (File file : listofFiles) {
			System.out.println("GBS Deployment - Loading automation script..." + file.getName());
			
			Scanner scFile = null;
			try {
				// Scan the entire file... \\Z doesn't guarantee that the entire
				// file will be read in all cases. Still need to iterate.
				StringBuilder sb = new StringBuilder();
				scFile = new Scanner(file);
				scFile.useDelimiter("\\Z");
				while (scFile.hasNext())
					sb.append(scFile.next());
				String scannedFile = sb.toString();
				// String scannedFile = new
				// Scanner(file).useDelimiter("\\Z").next();
				// System.out.println(file+"..."+scannedFile);

				// Split the file based on delimiter
				String[] splitFile = scannedFile.split("##");

				String scriptCode = scannedFile.substring(scannedFile.indexOf("##")+2);
				
				// Get the header properties
				Properties header = parsePropertiesString(splitFile[0]);

				// Code
				String code = scriptCode.replace("'", "''");

				//fix for windows line break chars
				//code = code.replace("\r\n", "[LINEBREAK]");
				//No need for line breaks
				//fix for mac and linux line break chars
				//code = code.replace("\n", "[LINEBREAK]");  

				// Remove all references to Automation Script being deployed
				executeSql("delete from autoscript where autoscript = '" + header.getProperty("name") + "'");
				executeSql("delete from SCRIPTLAUNCHPOINT where autoscript = '" + header.getProperty("name") + "'");
				executeSql("delete from AUTOSCRIPTVARS where autoscript = '" + header.getProperty("name") + "'");
				executeSql("delete from LAUNCHPOINTVARS where autoscript = '" + header.getProperty("name") + "'");
				
				//executeSql(
						/*"INSERT INTO AUTOSCRIPT (AUTOSCRIPT,STATUS,SCHEDULEDSTATUS,COMMENTS,OWNERID,OWNERNAME,OWNERPHONE,OWNEREMAIL,CREATEDBYID,DESCRIPTION,ORGID,SITEID,ACTION,SOURCE,CREATEDDATE,VERSION,CATEGORY,STATUSDATE,CHANGEDATE,CREATEDBYPHONE,CREATEDBYNAME,CREATEDBYEMAIL,OWNER,CREATEDBY,CHANGEBY,AUTOSCRIPTID,HASLD,LANGCODE,BINARYSCRIPTSOURCE,SCRIPTLANGUAGE,USERDEFINED,LOGLEVEL,INTERFACE) VALUES ("
								+ getValueOrNull(header, "name") + "," + getValueOrNull(header, "status")
								+ ",null,null,null,null,null,'',null," + getValueOrNull(header, "description")
								+ ",null,null,null," + 
								"cast(replace('" + code  + "','[LINEBREAK]', CHAR(10)) as TEXT)" + 
								"," + this.util.getNativeDateDefault() + ",null,null,"
								+ this.util.getNativeDateDefault() + "," + this.util.getNativeDateDefault()
								+ ",null,null,'','MAXADMIN','MAXADMIN','MAXADMIN',AUTOSCRIPTSEQ.NEXTVAL,0,'EN','',"
								+ getValueOrNull(header, "language") + ",1,'" + header.getProperty("log") + "', " + getValueOrZero(header, "interface") + ")");*/
				
								executeSql("DECLARE v_source CLOB; BEGIN " +
								"v_source := '" + code + "'; INSERT INTO " +
								"AUTOSCRIPT " +
								"(AUTOSCRIPT,STATUS,SCHEDULEDSTATUS,COMMENTS,OWNERID,OWNERNAME,OWNERPHONE,OWNEREMAIL,CREATEDBYID,DESCRIPTION,ORGID,SITEID,ACTION,SOURCE,CREATEDDATE,VERSION,CATEGORY,STATUSDATE,CHANGEDATE,CREATEDBYPHONE,CREATEDBYNAME,CREATEDBYEMAIL,OWNER,CREATEDBY,CHANGEBY,AUTOSCRIPTID,HASLD,LANGCODE,BINARYSCRIPTSOURCE,SCRIPTLANGUAGE,USERDEFINED,LOGLEVEL,INTERFACE,ACTIVE) " +
								"VALUES ("+getValueOrNull(header,"name")+","+getValueOrNull(header,"status")+",null,null,null,null,null,'',null, " +
										getValueOrNull(header,"description")+",null,null,null,v_source,"+this.util.getNativeDateDefault()+",null,null,"+
										this.util.getNativeDateDefault()+","+this.util.getNativeDateDefault()+",null,null,'','MAXADMIN','MAXADMIN','MAXADMIN',AUTOSCRIPTSEQ.NEXTVAL,0,'EN','',"+
										getValueOrNull(header,"language")+",1,'"+header.getProperty("log")+"',"+getValueOrZero(header, "interface")+",1);END;");

				// Loop through the keys and find properties
				int counter = 1;
				boolean lpComp = false, lpVarsComp = false, autoScriptVarComp = false;

				// Do an infinite loop
				while (counter != 0) {
					// If launch point is not null
					if (header.getProperty("launchpoint." + counter + ".name") != null) {
						// Insert a new launch point
						executeSql(
								"INSERT INTO SCRIPTLAUNCHPOINT (LAUNCHPOINTNAME,AUTOSCRIPT,DESCRIPTION,LAUNCHPOINTTYPE,OBJECTNAME,OBJECTEVENT,ATTRIBUTENAME,SCRIPTLAUNCHPOINTID,CONDITION,ACTIVE) VALUES ("
										+ getValueOrNull(header, "launchpoint." + counter + ".name") + ","
										+ getValueOrNull(header, "name") + ","
										+ getValueOrNull(header, "launchpoint." + counter + ".description") + ","
										+ getValueOrNull(header, "launchpoint." + counter + ".type") + ","
										+ getValueOrNull(header, "launchpoint." + counter + ".object") + ","
										+ getValueOrNull(header, "launchpoint." + counter + ".event") + ","
										+ getValueOrNull(header, "launchpoint." + counter + ".attribute")
										+ ",SCRIPTLAUNCHPOINTSEQ.NEXTVAL,"
										+ getValueOrNull(header, "launchpoint." + counter + ".condition") + ","
										+ header.getProperty("launchpoint." + counter + ".active") + ")");
					} else
						// If launch point is null, set the launch point
						// complete to true
						lpComp = true;

					// If script variable is not null
					if (header.getProperty("variable." + counter + ".name") != null) {
						executeSql(
								"INSERT INTO AUTOSCRIPTVARS (AUTOSCRIPTVARSID,AUTOSCRIPT,VARNAME,VARBINDINGVALUE,VARBINDINGTYPE,VARTYPE,DESCRIPTION,ALLOWOVERRIDE,LITERALDATATYPE,ACCESSFLAG) VALUES (AUTOSCRIPTVARSSEQ.NEXTVAL,"
										+ getValueOrNull(header, "name") + ","
										+ getValueOrNull(header, "variable." + counter + ".name") + ","
										+ getValueOrNull(header, "variable." + counter + ".bindingvalue") + ","
										+ getValueOrNull(header, "variable." + counter + ".bindtype") + ","
										+ getValueOrNull(header, "variable." + counter + ".type") + ","
										+ getValueOrNull(header, "variable." + counter + ".description") + ","
										+ header.getProperty("variable." + counter + ".allowoverride") + ","
										/* A null was being passed previously - sanjog.raina@au1.ibm.com*/
										+ getValueOrNull(header, "variable." + counter + ".literaldatatype") + ","
										+ header.getProperty("variable." + counter + ".flag") + ")");
					} else
						// if variable is null, set the variable complete to
						// true
						autoScriptVarComp = true;

					// If script variable is not null
					if (header.getProperty("launchpointvar." + counter + ".varname") != null) {
						executeSql(
								"INSERT INTO LAUNCHPOINTVARS (LAUNCHPOINTNAME,AUTOSCRIPT,VARNAME,VARBINDINGVALUE,LAUNCHPOINTVARSID) VALUES("
										+ getValueOrNull(header, "launchpointvar." + counter + ".launchpointname") + ","
										+ getValueOrNull(header, "name") + ","
										+ getValueOrNull(header, "launchpointvar." + counter + ".varname") + ","
										+ getValueOrNull(header, "launchpointvar." + counter + ".varbindingvalue")
										+ ",LAUNCHPOINTVARSSEQ.NEXTVAL)");
					} else
						// if variable is null, set the variable complete to
						// true
						lpVarsComp = true;

					// if launch point and variable are complete, get out of the
					// infinite loop
					if (lpComp && autoScriptVarComp & lpVarsComp)
						break;
					else
						// else increment the counter and go to the next set of
						// launch points and variables
						counter++;
				}
				// 20160302 JWM close scFile.
				scFile.close();
				scFile = null;
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} finally {
				if(scFile != null) {
					scFile.close();
				}
				
			}
		}
	}

	/**
	 * This extracts a sql formatted value or the string null (if there is no
	 * value) for a key. Aux method for {@link #processFiles(ArrayList)}.
	 * 
	 * @param header
	 * @param key
	 * @return
	 */
	private String getValueOrNull(Properties header, String key) {
		String value = header.getProperty(key);
		if (value == null)
			return "null";
		else
			return "'" + value.replaceAll("'", "''") + "'";
	}
	
	/**
	 * This extracts a sql formatted value or the string null (if there is no
	 * value) for a key. Aux method for {@link #processFiles(ArrayList)}.
	 * 
	 * @param header
	 * @param key
	 * @return
	 */
	private String getValueOrZero(Properties header, String key) {
		return getValueOrNull(header, key).equals("null") ? "0": "1";
	}	

	/**
	 * This method is used to scan directories and sub directories for files
	 * 
	 * @param folder
	 * @param listofFiles
	 */
	public void loadAllFiles(File folder, ArrayList<File> listofFiles) {
		// Loop through list of files
		for (File fileEntry : folder.listFiles()) {
			// if file is a directory
			if (fileEntry.isDirectory()) {
				// load all files from that directory
				loadAllFiles(fileEntry, listofFiles);
			} else {
				// if it is a file, add it to the list of files
				listofFiles.add(fileEntry);
			}
		}
	}

	/**
	 * Used to get the product directory
	 * 
	 * @return String
	 */
	@SuppressWarnings("unused")
	private String getProductDirFromClass() {
		// Return the product directory
		return super.getClass().getPackage().getName().split("\\.")[1];
	}

	/**
	 * Method used to parse properties file
	 * 
	 * @param script
	 * @return
	 * @throws IOException
	 */
	private Properties parsePropertiesString(String script) throws IOException {
		// Properties
		Properties scriptProperties = new Properties();

		// If the script is null, return null property
		if (script == null)
			return scriptProperties;

		// Else, load the properties and replace the # from automation script
		scriptProperties.load(new StringReader(script.replaceAll("#", "")));

		// Return the property
		return scriptProperties;
	}
}
