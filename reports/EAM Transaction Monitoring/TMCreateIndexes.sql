------------------------
-- Create TM_SA indexes
------------------------
CREATE INDEX tm_sa.idx_asetxnackaccept 
ON tm_sa.asetxnackaccept (initiatingtransactionid)
STORAGE (
  INITIAL 4M 
  NEXT 1M
)
TABLESPACE TM_MED_IDX
/

CREATE INDEX tm_sa.idx_asetxnackpartial
ON tm_sa.asetxnackpartial (initiatingtransactionid)
STORAGE (
  INITIAL 4M 
  NEXT 1M
)
TABLESPACE TM_MED_IDX
/

CREATE INDEX tm_sa.idx_asetxnackreject
ON tm_sa.asetxnackreject (initiatingtransactionid)
STORAGE (
  INITIAL 4M 
  NEXT 1M
)
TABLESPACE TM_MED_IDX
/

CREATE INDEX tm_sa.idx_ebtxnacknowledgment 
ON tm_sa.ebtxnacknowledgment (refmessageid)
STORAGE (
  INITIAL 128M 
  NEXT 1M
)
TABLESPACE TM_LRG_IDX
/

CREATE INDEX tm_sa.idx_ebtxnmessageerror 
ON tm_sa.ebtxnmessageerror (refmessageid)
STORAGE (
  INITIAL 4M 
  NEXT 1M
)
TABLESPACE TM_MED_IDX
/

CREATE INDEX tm_sa.idx_sordrequestnew
ON tm_sa.sordrequestnew (serviceorderno)
STORAGE (
  INITIAL 4M 
  NEXT 1M
)
TABLESPACE TM_MED_IDX
/

CREATE INDEX tm_sa.idx_sordrequestcancel
ON tm_sa.sordrequestcancel (initiatingtransactionid)
STORAGE (
  INITIAL 4M 
  NEXT 1M
)
TABLESPACE TM_MED_IDX
/

CREATE INDEX tm_sa.idx2_sordrequestcancel
ON tm_sa.sordrequestcancel (serviceorderno)
STORAGE (
  INITIAL 4M 
  NEXT 1M
)
TABLESPACE TM_MED_IDX
/

CREATE INDEX tm_sa.idx_sordresponseinitial
ON tm_sa.sordresponseinitial (initiatingtransactionid)
STORAGE (
  INITIAL 4M 
  NEXT 1M
)
TABLESPACE TM_MED_IDX
/

CREATE INDEX tm_sa.idx2_sordresponseinitial
ON tm_sa.sordresponseinitial (serviceorderno)
STORAGE (
  INITIAL 4M 
  NEXT 1M
)
TABLESPACE TM_MED_IDX
/

CREATE INDEX tm_sa.idx_sordresponseclosure
ON tm_sa.sordresponseclosure (initiatingtransactionid)
STORAGE (
  INITIAL 4M 
  NEXT 1M
)
TABLESPACE TM_MED_IDX
/

CREATE INDEX tm_sa.idx2_sordresponseclosure
ON tm_sa.sordresponseclosure (serviceorderno)
STORAGE (
  INITIAL 4M 
  NEXT 1M
)
TABLESPACE TM_MED_IDX
/

------------------------
-- Create TM_VIC indexes
------------------------
CREATE INDEX tm_vic.idx_asetxnackaccept 
ON tm_vic.asetxnackaccept (initiatingtransactionid)
STORAGE (
  INITIAL 4M 
  NEXT 1M
)
TABLESPACE TM_MED_IDX
/

CREATE INDEX tm_vic.idx_asetxnackpartial
ON tm_vic.asetxnackpartial (initiatingtransactionid)
STORAGE (
  INITIAL 4M 
  NEXT 1M
)
TABLESPACE TM_MED_IDX
/

CREATE INDEX tm_vic.idx_asetxnackreject
ON tm_vic.asetxnackreject (initiatingtransactionid)
STORAGE (
  INITIAL 4M 
  NEXT 1M
)
TABLESPACE TM_MED_IDX
/

CREATE INDEX tm_vic.idx_ebtxnacknowledgment 
ON tm_vic.ebtxnacknowledgment (refmessageid)
STORAGE (
  INITIAL 128M 
  NEXT 1M
)
TABLESPACE TM_LRG_IDX
/

CREATE INDEX tm_vic.idx_ebtxnmessageerror 
ON tm_vic.ebtxnmessageerror (refmessageid)
STORAGE (
  INITIAL 4M 
  NEXT 1M
)
TABLESPACE TM_MED_IDX
/

CREATE INDEX tm_vic.idx_sordrequestnew
ON tm_vic.sordrequestnew (serviceorderno)
STORAGE (
  INITIAL 4M 
  NEXT 1M
)
TABLESPACE TM_MED_IDX
/

CREATE INDEX tm_vic.idx_sordrequestcancel
ON tm_vic.sordrequestcancel (initiatingtransactionid)
STORAGE (
  INITIAL 4M 
  NEXT 1M
)
TABLESPACE TM_MED_IDX
/

CREATE INDEX tm_vic.idx2_sordrequestcancel
ON tm_vic.sordrequestcancel (serviceorderno)
STORAGE (
  INITIAL 4M 
  NEXT 1M
)
TABLESPACE TM_MED_IDX
/

CREATE INDEX tm_vic.idx_sordresponseinitial
ON tm_vic.sordresponseinitial (initiatingtransactionid)
STORAGE (
  INITIAL 4M 
  NEXT 1M
)
TABLESPACE TM_MED_IDX
/

CREATE INDEX tm_vic.idx2_sordresponseinitial
ON tm_vic.sordresponseinitial (serviceorderno)
STORAGE (
  INITIAL 4M 
  NEXT 1M
)
TABLESPACE TM_MED_IDX
/

CREATE INDEX tm_vic.idx_sordresponseclosure
ON tm_vic.sordresponseclosure (initiatingtransactionid)
STORAGE (
  INITIAL 4M 
  NEXT 1M
)
TABLESPACE TM_MED_IDX
/

CREATE INDEX tm_vic.idx2_sordresponseclosure
ON tm_vic.sordresponseclosure (serviceorderno)
STORAGE (
  INITIAL 4M 
  NEXT 1M
)
TABLESPACE TM_MED_IDX
/


------------------------
-- Create TM_QLD indexes
------------------------
CREATE INDEX tm_qld.idx_sordrequestnew
ON tm_qld.sordrequestnew (serviceorderno)
STORAGE (
  INITIAL 4M 
  NEXT 1M
)
TABLESPACE TM_MED_IDX
/

CREATE INDEX tm_qld.idx2_sordrequestcancel
ON tm_qld.sordrequestcancel (serviceorderno)
STORAGE (
  INITIAL 4M 
  NEXT 1M
)
TABLESPACE TM_MED_IDX
/

CREATE INDEX tm_qld.idx2_sordresponseinitial
ON tm_qld.sordresponseinitial (serviceorderno)
STORAGE (
  INITIAL 4M 
  NEXT 1M
)
TABLESPACE TM_MED_IDX
/

CREATE INDEX tm_qld.idx2_sordresponseclosure
ON tm_qld.sordresponseclosure (serviceorderno)
STORAGE (
  INITIAL 4M 
  NEXT 1M
)
TABLESPACE TM_MED_IDX
/


------------------------
-- Create TM_MAN indexes
------------------------
CREATE INDEX tm_man.idx2_sordrequestcancel
ON tm_man.sordrequestcancel (serviceorderno)
STORAGE (
  INITIAL 4M 
  NEXT 1M
)
TABLESPACE TM_MED_IDX
/

CREATE INDEX tm_man.idx2_sordresponseinitial
ON tm_man.sordresponseinitial (serviceorderno)
STORAGE (
  INITIAL 4M 
  NEXT 1M
)
TABLESPACE TM_MED_IDX
/

CREATE INDEX tm_man.idx2_sordresponseclosure
ON tm_man.sordresponseclosure (serviceorderno)
STORAGE (
  INITIAL 4M 
  NEXT 1M
)
TABLESPACE TM_MED_IDX
/
