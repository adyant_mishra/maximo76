--
-- To be run as a DBA
--

-- Create readonly public link to CCB database
-- Service name of CCB DB to be changed according to environment being deployed to
CREATE PUBLIC DATABASE LINK cdx_link
CONNECT TO readonly IDENTIFIED BY &CCBREADONLYPASSWORD
USING '&CCBDB'
/

-- Create readonly public link to Maximo 7 database
-- Service name of Maximo DB to be changed according to environment being deployed to
CREATE PUBLIC DATABASE LINK mx7_link
CONNECT TO readonly IDENTIFIED BY &MXOREADONLYPASSWORD
USING '&MXODB'
/


-- Remove private database link to CCB as this has been replaced by public link
-- Password of tm_sa user may need to be changed according to environment being deployed to
CONNECT tm_sa/tm_sardmp

DROP DATABASE LINK cdx_link
/
