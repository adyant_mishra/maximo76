--
-- To be run as a DBA
--

SET SERVEROUTPUT ON

DECLARE
  CURSOR cr_tm_tables IS
    SELECT *
    FROM   all_tables
    WHERE  owner IN ('TM_SA','TM_VIC','TM_QLD','TM_MAN')
    AND    table_name IN (
                          'ASETXN',
                          'ASETXNACKACCEPT',
                          'ASETXNACKPARTIAL',
                          'ASETXNACKREJECT',
                          'CATSCHANGEREQUEST',
                          'CATSCHANGERESPONSE',
                          'CATSDATAREQUEST',
                          'CATSNOTIFICATION',
                          'CUSTCUSTOMERDETAILSNOTIFY',
                          'EBTXN',
                          'EBTXNACKNOWLEDGMENT',
                          'EBTXNMESSAGEERROR',
                          'MDMTACCOUNTCREATIONNOTIFY',
                          'MDMTMETERDATAMISSINGNOTIFY',
                          'MDMTMETERDATANOTIFICATION',
                          'MDMTMETERDATARESPONSE',
                          'MDMTMETERDATAVERIFYREQUEST',
                          'MDMTMETERDATAVERIFYRESPONSE',
                          'MDMTMETEREDSUPPLYUPDATE',
                          'MDMTMETERREADINPUTNOTIFICATION',
                          'MDMTSPECIALREADREQUEST',
                          'MDMTSPECIALREADRESPONSE',
                          'NETBDUOSBILLINGNOTIFICATION',
                          'NMIDISCOVERYREQUEST',
                          'NMIDISCOVERYRESPONSE',
                          'NMISTANDINGDATAREQUEST',
                          'NMISTANDINGDATARESPONSE',
                          'NMISTANDINGDATAUPDATENOTIFY',
                          'SITEAMENDMETERROUTEDETAILS',
                          'SORDGASMETERNOTIFICATION',
                          'SORDREQUESTCANCEL',
                          'SORDREQUESTNEW',
                          'SORDRESPONSECLOSURE',
                          'SORDRESPONSEINITIAL',
                          'WMCAPTURELOG'
                        )
    ORDER BY table_name, owner;
    CURSOR cr_tm_tab_columns(crv_table_name VARCHAR2) IS
      SELECT *
      FROM   all_tab_columns
      WHERE  owner = 'TM_VIC'
      AND    table_name = crv_table_name
      ORDER BY column_id;

    lv_sa_view_section VARCHAR2(4000);
    lv_vic_view_section VARCHAR2(4000);
    lv_qld_view_section VARCHAR2(4000);
    lv_man_view_section VARCHAR2(4000);
BEGIN
  FOR lv_table_rec IN cr_tm_tables LOOP
    -- Grant select access on all tables to new TM user
    BEGIN
      DBMS_OUTPUT.PUT_LINE('GRANT SELECT ON '|| LOWER(lv_table_rec.owner || '.' || lv_table_rec.table_name) || ' TO tm');
      EXECUTE IMMEDIATE 'GRANT SELECT ON '|| LOWER(lv_table_rec.owner || '.' || lv_table_rec.table_name) || ' TO tm';
      DBMS_OUTPUT.PUT_LINE('GRANT SUCCEEDED');
    EXCEPTION  
      WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('GRANT FAILED: ' || SQLERRM);
    END;   
    -- When processing VIC instance of table, create a view under TM user to define union of all instances of the table
    IF lv_table_rec.owner = 'TM_VIC' THEN
      FOR lv_column_rec IN cr_tm_tab_columns(crv_table_name => lv_table_rec.table_name) LOOP
        IF lv_column_rec.column_id = 1 THEN 
          lv_sa_view_section := 'CREATE OR REPLACE VIEW tm.'|| LOWER(lv_table_rec.table_name) || ' AS SELECT ''SA'' AS gdndivision';
          lv_vic_view_section := CHR(10) || 'UNION ALL ' || CHR(10) || 'SELECT ''VIC'' AS gdndivision';
          lv_qld_view_section := CHR(10) || 'UNION ALL ' || CHR(10) || 'SELECT ''QLDE'' AS gdndivision'; 
          lv_man_view_section := CHR(10) || 'UNION ALL ' || CHR(10) || 'SELECT ''QLDA'' AS gdndivision';
        END IF;
        lv_sa_view_section  :=  lv_sa_view_section  || CHR(10) || ',' || LOWER(lv_column_rec.column_name);
        lv_vic_view_section :=  lv_vic_view_section || CHR(10) || ',' || LOWER(lv_column_rec.column_name);
        lv_qld_view_section :=  lv_qld_view_section || CHR(10) || ',' || LOWER(lv_column_rec.column_name);
        lv_man_view_section :=  lv_man_view_section || CHR(10) || ',' || LOWER(lv_column_rec.column_name);
      END LOOP;
      lv_sa_view_section  :=  lv_sa_view_section  || CHR(10) || 'FROM tm_sa.' || LOWER(lv_table_rec.table_name);
      lv_vic_view_section :=  lv_vic_view_section || CHR(10) || 'FROM tm_vic.' || LOWER(lv_table_rec.table_name);
      lv_qld_view_section :=  lv_qld_view_section || CHR(10) || 'FROM tm_qld.' || LOWER(lv_table_rec.table_name);
      lv_man_view_section :=  lv_man_view_section || CHR(10) || 'FROM tm_man.' || LOWER(lv_table_rec.table_name);
      BEGIN
        DBMS_OUTPUT.PUT_LINE(lv_sa_view_section || lv_vic_view_section || lv_qld_view_section || lv_man_view_section);
        EXECUTE IMMEDIATE(lv_sa_view_section || lv_vic_view_section || lv_qld_view_section || lv_man_view_section);
        DBMS_OUTPUT.PUT_LINE('VIEW CREATED');
      EXCEPTION  
        WHEN OTHERS THEN
          DBMS_OUTPUT.PUT_LINE('VIEW CREATION FAILED: ' || SQLERRM);
      END;   
    END IF;
  END LOOP;
END;
/
