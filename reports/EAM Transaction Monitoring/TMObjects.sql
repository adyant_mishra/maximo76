CREATE TABLE tm.txnlifecycle (
  transactionprocessingcomplete VARCHAR2(1) NOT NULL,
  gdndivision                   VARCHAR2(4) NOT NULL,
  finishtoclosureresponsedays   INTEGER,
  wmserviceorder                VARCHAR2(64),
  wmjobenquirycode              VARCHAR2(64),
  wmmirn                        VARCHAR2(64),
  wmrecipient                   VARCHAR2(64),
  wmsender                      VARCHAR2(64),
  wmoriginatingtransactionid    VARCHAR2(64),
  wmoriginatingtransactiondate  DATE,
  wmcancellationtransactionid   VARCHAR2(64),
  wmcancellationtransactiondate DATE,
  wmpreferredappointmentdate    DATE,  
  wmrejecteddate                DATE,
  wminitialresponsedate         DATE,  
  wmclosureresponsedate         DATE,
  cdxgmntoaemodate              DATE
  cdxwfprocid                   VARCHAR2(10),
  cdxwfcreationdate             DATE,
  cdxwfstatus                   VARCHAR2(60),
  cdxfaid                       VARCHAR2(10),
  cdxfacreationdate             DATE,
  cdxfastatus                   VARCHAR2(60),
  cdxfastagestatus              VARCHAR2(1),
  cdxfastageerror               VARCHAR2(254),
  cdxcancellationwfprocid       VARCHAR2(10),
  cdxcancellationwfcreationdate DATE,
  mxosrnum                      VARCHAR2(10),
  mxosrcreationdate             DATE,
  mxowonum                      VARCHAR2(10),
  mxowocreationdate             DATE,
  mxowocompletedstatusdate      DATE,
  mxowoactfinishdate            DATE,
  mxowoclosedstatusdate         DATE
)
/

CREATE INDEX tm.txnlifecycle_idx1 
ON tm.txnlifecycle (gdndivision, wmserviceorder)
/

CREATE INDEX tm.txnlifecycle_idx2
ON tm.txnlifecycle (wmoriginatingtransactiondate, wmjobenquirycode)
/

CREATE SEQUENCE tm.txnpopulationseq CACHE 2 ORDER 
/

CREATE TABLE tm.txnpopulationhistory (
  txnpopid       INTEGER NOT NULL,
  startdate      DATE NOT NULL,
  enddate        DATE,
  latestsorddate DATE,
  runstatus      VARCHAR2(100),
  CONSTRAINT txnpop_pk PRIMARY KEY (txnpopid)
)
/


CREATE TABLE tm.txnpopulationstephistory (
  txnpopid        INTEGER NOT NULL,
  stepid          INTEGER NOT NULL,
  stepdescription VARCHAR2(100) NOT NULL,
  startdate       DATE NOT NULL,
  enddate         DATE,
  recordcount     INTEGER,
  CONSTRAINT txnpopstephist_pk PRIMARY KEY (txnpopid, stepid)
)
/


CREATE TABLE tm.gdn_division_xref (
  gdnlocation     VARCHAR(48) NOT NULL,
  gdndivision     VARCHAR2(10),
  calnum           VARCHAR2(8)
)
/

INSERT INTO tm.gdn_division_xref (
  gdnlocation,
  gdndivision,
  calnum
)
WITH gdn AS
    (
      SELECT loc.location AS gdnlocation,
             CASE 
               WHEN loc.description LIKE '%QLD%' THEN 'QLDE'
               WHEN loc.description LIKE '%VIC%' THEN 'VIC'
               WHEN loc.description LIKE '%SA%' THEN 'SA'
               WHEN loc.description LIKE '%GDI%' THEN 'QLDA'
             END AS gdndivision,
             CASE 
               WHEN loc.description LIKE '%QLD%' THEN 'QLD'
               WHEN loc.description LIKE '%VIC%' THEN 'VIC'
               WHEN loc.description LIKE '%SA%' THEN 'SA'
               WHEN loc.description LIKE '%GDI%' THEN 'QLD'
             END AS calnum
      FROM   maximo.locations@mx7_link loc
      JOIN   maximo.classstructure@mx7_link cs
         ON  cs.classstructureid = loc.classstructureid
         AND cs.classificationid = 'GDN'
    )
SELECT gdnlocation,
       gdndivision,
       calnum
FROM   gdn
/


CREATE TABLE tm.jeccompthreshold (
  gdndivision                   VARCHAR2(4) NOT NULL,
  jobenquirycode                VARCHAR2(10) NOT NULL,
  completionresponsethreshold   INTEGER NOT NULL,
  CONSTRAINT jeccompthreshold_pk PRIMARY KEY (gdndivision, jobenquirycode)
)
/

INSERT INTO tm.jeccompthreshold (
  gdndivision,
  jobenquirycode,
  completionresponsethreshold
)
SELECT gdn.gdndivision,
       cs.classificationid AS jobenquirycode,
       CASE
         WHEN gdn.gdndivision = 'SA' AND cs.classificationid = 'MFX' THEN 2
         ELSE 5
       END AS completionresponsethreshold
FROM   maximo.classstructure@mx7_link cs
CROSS JOIN tm.gdn_division_xref gdn
WHERE  1=1
AND    cs.classstructureid IN
       (
         SELECT cuw.classstructureid
         FROM   maximo.classusewith@mx7_link cuw
         WHERE  cuw.objectvalue = 'SR'
       )
AND EXISTS
       (
         SELECT 'x'
         FROM   maximo.classspec@mx7_link csp
         WHERE  csp.classstructureid = cs.classstructureid
         AND    csp.assetattrid = 'JEC'
       )
/


GRANT SELECT ON tm.txnlifecycle TO readonly
/
GRANT SELECT ON tm.txnpopulationhistory TO readonly
/
GRANT SELECT ON tm.txnpopulationstephistory TO readonly
/
GRANT SELECT ON tm.jeccompthreshold TO readonly
/
GRANT SELECT ON tm.gdn_division_xref TO readonly
/
