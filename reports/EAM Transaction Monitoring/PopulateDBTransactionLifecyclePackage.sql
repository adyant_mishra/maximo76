CREATE OR REPLACE PACKAGE tm.populate_dbtxnlifecycle AS

  PROCEDURE main;

END populate_dbtxnlifecycle;
/

SHOW ERRORS
/

CREATE OR REPLACE PACKAGE BODY tm.populate_dbtxnlifecycle AS  

  pkv_txnpopid INTEGER;
  pkv_sord_cutoff_date DATE;
  
  ------------------------------------------------------------------------------------------------------------------
  -- Write history record
  ------------------------------------------------------------------------------------------------------------------
  FUNCTION fn_create_run_history
    RETURN INTEGER IS
    CURSOR cr_get_lastrun_latestsorddate IS
      SELECT latestsorddate
      FROM   db_txnpopulationhistory
      WHERE  runstatus = 'COMPLETE'
      AND    txnpopid = (SELECT MAX(txnpopid)
                         FROM   db_txnpopulationhistory);
  BEGIN
    -- Set up cutoff date for query of work orders using date populated in last complete run
    -- Subtract one day to allow for any misalignment of dates 
    OPEN cr_get_lastrun_latestsorddate;
    FETCH cr_get_lastrun_latestsorddate INTO pkv_sord_cutoff_date;
    CLOSE cr_get_lastrun_latestsorddate;
    IF pkv_sord_cutoff_date IS NULL THEN
      pkv_sord_cutoff_date := SYSDATE - 1;
    END IF;  
    INSERT INTO db_txnpopulationhistory (
      txnpopid,
      startdate,
      runstatus
    ) VALUES (
      db_txnpopulationseq.NEXTVAL,
      SYSDATE,
      'STARTED'
    );
    COMMIT;
    RETURN db_txnpopulationseq.CURRVAL;    
  END fn_create_run_history;
  
  ------------------------------------------------------------------------------------------------------------------
  -- Create history step to record timings
  ------------------------------------------------------------------------------------------------------------------
  PROCEDURE pr_create_run_history_step(
    prv_step               INTEGER,
    prv_step_description   VARCHAR2
  ) IS
  BEGIN
    INSERT INTO db_txnpopulationstephistory (
      txnpopid,
      stepid,
      stepdescription,
      startdate
    ) VALUES (
      pkv_txnpopid,
      prv_step,
      prv_step_description,
      SYSDATE
    );
    COMMIT;
  END pr_create_run_history_step;

  ------------------------------------------------------------------------------------------------------------------
  -- Create history step to record timings
  ------------------------------------------------------------------------------------------------------------------
  PROCEDURE pr_update_run_history_step(
    prv_step         VARCHAR2,
    prv_record_count INTEGER DEFAULT NULL
  ) IS
  BEGIN
    UPDATE db_txnpopulationstephistory
    SET    enddate = SYSDATE,
           recordcount = prv_record_count
    WHERE  txnpopid = pkv_txnpopid
    AND    stepid = prv_step;
    COMMIT;
  END pr_update_run_history_step;

  ------------------------------------------------------------------------------------------------------------------
  -- Update status in db_txnpopulationhistory record
  ------------------------------------------------------------------------------------------------------------------
  PROCEDURE pr_update_run_history(
    prv_status  VARCHAR2,
    prv_enddate DATE
  ) IS
    CURSOR cr_get_latest_sorddate IS
      SELECT MAX(mxowocompletedstatusdate) AS maxsorddate
      FROM   db_txnlifecycle;
  BEGIN
    OPEN cr_get_latest_sorddate;
    FETCH cr_get_latest_sorddate INTO pkv_sord_cutoff_date;
    CLOSE cr_get_latest_sorddate;
    UPDATE db_txnpopulationhistory
    SET    runstatus = prv_status,
           enddate = prv_enddate,
           latestsorddate = pkv_sord_cutoff_date
    WHERE  txnpopid = pkv_txnpopid;
    COMMIT;
  END pr_update_run_history;

  ------------------------------------------------------------------------------------------------------------------
  -- Add new distributor generated work order to lifecycle table
  ------------------------------------------------------------------------------------------------------------------
  PROCEDURE pr_insert_new_dbwo IS
    lv_record_count INTEGER := 0;
  BEGIN
    pr_create_run_history_step(
      prv_step              => '10',
      prv_step_description  => 'Insert new distributor work orders');
     
    -- Insert new records where GDN and work order has not been encountered previously
    INSERT INTO db_txnlifecycle (
           transactionprocessingcomplete,
           gdndivision,
           mxowonum,
           mxowocreationdate,
           mxowocompletedstatusdate,
           mxowoactfinishdate,
		   mxomirn
	)
		-- Work orders selection must exclude those that already exist in txnlifecycle table
		-- must exclude work orders that location is not required to be sent to CCB
		SELECT 'N',
			   anc.ancestor AS gdndivision,
			   wo.wonum,
			   wo.reportdate,
			   wos.changedate,
			   wo.actfinishdate,
			   mirn.alnvalue
		FROM maximo.workorder@mx7_link wo
		JOIN maximo.wostatus@mx7_link wos
		  ON wo.siteid = wos.siteid
		 AND wo.wonum = wos.wonum
		 AND wos.status = 'COMP'
		JOIN (SELECT loca.location, loca.ancestor, loca.siteid
			  FROM maximo.locancestor@mx7_link loca
			  JOIN maximo.locations@mx7_link lcn
				ON loca.ancestor = lcn.location
			   AND  loca.siteid = lcn.siteid
			  WHERE loca.systemid = 'NETWORKS'
				AND loca.siteid = 'APA'
				AND EXISTS (SELECT 1
							FROM maximo.classstructure@mx7_link cl
							WHERE cl.classstructureid = lcn.classstructureid
							AND cl.classificationid = 'GDN'
							)
			 ) anc
		  ON anc.siteid = wo.siteid
		 AND anc.location = wo.location
		JOIN maximo.locationspec@mx7_link mirn
		  ON mirn.siteid = wo.siteid
		 AND mirn.location = wo.location
		 AND mirn.assetattrid = 'MIRN'
		WHERE 
		wo.siteid = 'APA'
		AND wo.istask = 0
		AND wo.woclass = 'WORKORDER'
		AND NOT EXISTS 
				(SELECT 1
				  FROM tm.txnlifecycle rbtm
				  WHERE rbtm.mxowonum = wo.wonum
				 )
		AND NOT EXISTS 
				(SELECT 1
				  FROM maximo.locationspec@mx7_link lsp
				  WHERE lsp.siteid = wo.siteid
				  AND lsp.location = wo.location
				  AND lsp.assetattrid = 'SEND_TO_CCB'
				  AND lsp.alnvalue = 'N'
				 )
		AND NOT EXISTS 
			   (SELECT 1
				FROM maximo.wostatus@mx7_link wos2
				WHERE wos2.siteid = wos.siteid
				AND wos2.wonum = wos.wonum
				AND wos2.status = 'COMP'
				AND wos2.changedate < wos.changedate
			   );
    lv_record_count := SQL%ROWCOUNT;
    pr_update_run_history_step(
      prv_step => '10',
      prv_record_count => lv_record_count);
  EXCEPTION
    WHEN OTHERS THEN NULL;
  END pr_insert_new_dbwo;

  ------------------------------------------------------------------------------------------------------------------
  -- Update service orders with details of MXO service request and work order creation
  ------------------------------------------------------------------------------------------------------------------
  PROCEDURE pr_mxo_sr IS
    lv_record_count INTEGER := 0;
    
  BEGIN
    pr_create_run_history_step(
      prv_step              => '20',
      prv_step_description  => 'MXO service request for work order');
    
    -- Update with service request number
    UPDATE db_txnlifecycle tlc
    SET    (mxosrnum, mxosrcreationdate) =
           (
            SELECT MAX(tkt.ticketid),
                    MAX(tkt.reportdate)                  
			 FROM   maximo.workorder@mx7_link wo
			 JOIN   maximo.ticket@mx7_link tkt
			   ON   wo.siteid = tkt.siteid
			  AND   wo.origrecordid = tkt.ticketid
			WHERE   wo.siteid = 'APA'
			  AND   wo.istask = 0
			  AND   wo.woclass = 'WORKORDER'
			  AND   wo.origrecordclass = 'SR'
			  AND   wo.wonum = tlc.mxowonum
           )
    WHERE  tlc.transactionprocessingcomplete = 'N'
    AND    tlc.mxowonum IS NOT NULL
    AND    tlc.mxosrnum IS NULL;
    lv_record_count := lv_record_count + SQL%ROWCOUNT;
    pr_update_run_history_step(
      prv_step => '20',
      prv_record_count => lv_record_count);
  EXCEPTION
    WHEN OTHERS THEN NULL;
  END pr_mxo_sr;
  
  ------------------------------------------------------------------------------------------------------------------
  -- Update work orders with details of field activity and CCB workflow
  ------------------------------------------------------------------------------------------------------------------
  PROCEDURE pr_cdx_wf_fa IS
    lv_record_count INTEGER := 0;
    
    CURSOR cr_incomplete_txns IS
      SELECT *
      FROM   db_txnlifecycle tlc
      WHERE  tlc.transactionprocessingcomplete = 'N'
      AND    (tlc.cdxfaid IS NULL 
			  OR tlc.cdxwfprocid is NULL)
      FOR UPDATE OF cdxfaid;

    CURSOR cr_mxo_details(cv_mxwonum  VARCHAR2) IS
         SELECT	fa.fa_id,
				'M'||regexp_substr(fa.descr254,'[0-9]+',1,1) AS mxwo,
				faid.fa_type_cd AS fa_jobenquirycode,
				faid.Cre_Dttm AS fa_creation_date,
				wf.wf_proc_id,
				wf.cre_dttm AS wf_creation_date
	       FROM CISADM.CI_FA_STAGE_UP fa
	       JOIN	cisadm.ci_fa faid
	         ON	fa.fa_id = faid.fa_id
		 LEFT OUTER JOIN cisadm.ci_wf_proc_ctxt wffa
             ON rpad(fa.fa_id,50) = wffa.ctxt_val
            AND wffa.wf_proc_ctxt_flg = 'SPRF'
	  LEFT OUTER JOIN cisadm.ci_wf_proc wf
             ON wffa.wf_proc_id = wf.wf_proc_id
	  WHERE	regexp_like (fa.descr254,'M'||regexp_substr(fa.descr254,'[0-9]+',1,1))
	  AND LENGTH('M'||regexp_substr(fa.descr254,'[0-9]+',1,1)) > 1
	  AND 'M'||regexp_substr(fa.descr254,'[0-9]+',1,1) = cv_mxwonum;
    lv_cdx_rec cr_mxo_details%ROWTYPE;
  BEGIN
    pr_create_run_history_step(
      prv_step              => '30',
      prv_step_description  => 'Field Activity and CCB Workflow for work orders');
    
    FOR lv_txn_rec IN cr_incomplete_txns LOOP
      OPEN cr_mxo_details(cv_mxwonum  => lv_txn_rec.mxowonum);
      FETCH cr_mxo_details INTO lv_cdx_rec;
      IF cr_mxo_details%FOUND THEN
        UPDATE db_txnlifecycle tlc
        SET    cdxfaid           	= lv_cdx_rec.fa_id,
			   cdxfajobenquirycode	= lv_cdx_rec.fa_jobenquirycode,
               cdxfacreationdate 	= lv_cdx_rec.fa_creation_date,
			   cdxwfprocid			= lv_cdx_rec.wf_proc_id
			   cdxwfcreationdate	= lv_cdx_rec.wf_creation_date
			   
        WHERE CURRENT OF cr_incomplete_txns;
        lv_record_count := lv_record_count + 1;
      END IF;
      CLOSE cr_mxo_details;
    END LOOP;  
    pr_update_run_history_step(
      prv_step => '30',
      prv_record_count => lv_record_count);
  EXCEPTION
    WHEN OTHERS THEN NULL;
  END pr_cdx_wf_fa;

  ------------------------------------------------------------------------------------------------------------------
  -- Update work orders with status of field activity and CCB workflow 
  ------------------------------------------------------------------------------------------------------------------
  PROCEDURE pr_cdx_wf_fa_status IS
    lv_record_count INTEGER := 0;
    
  BEGIN
    pr_create_run_history_step(
      prv_step              => '35',
      prv_step_description  => 'CCB workflow and field activity current status');
    
    UPDATE db_txnlifecycle tlc
    SET    tlc.cdxwfstatus =
           (
             SELECT lok.descr
             FROM   cisadm.ci_wf_proc@cdx_link wf
             JOIN   cisadm.ci_lookup@cdx_link lok
                ON  lok.field_name = 'WF_STAT_RSN_FLG'
                AND wf.wf_stat_rsn_flg = lok.field_value
             WHERE  wf.wf_proc_id = tlc.cdxwfprocid
           )
    WHERE  tlc.transactionprocessingcomplete = 'N'
    AND    tlc.cdxwfprocid IS NOT NULL;
    lv_record_count := SQL%ROWCOUNT;

    UPDATE db_txnlifecycle tlc
    SET    tlc.cdxfastatus =
           (
             SELECT lok.descr
             FROM   cisadm.ci_fa@cdx_link fa
             JOIN   cisadm.ci_lookup@cdx_link lok
                ON  lok.field_name = 'FA_STATUS_FLG'
                AND fa.fa_status_flg = lok.field_value
             WHERE  fa.fa_id = tlc.cdxfaid
           ),
           tlc.cdxfastagestatus =
           (
             SELECT fa_up_status_flg
             FROM   cisadm.ci_fa_stage_up@cdx_link fsu
             WHERE  fsu.fa_id = tlc.cdxfaid
           ),
           tlc.cdxfastageerror =
           (
             SELECT fse.exp_msg
             FROM   cisadm.ci_fa_stgup_exc@cdx_link fse
             WHERE  fse.fa_id = tlc.cdxfaid
             AND    fse.message_cat_nbr = '9'
           )
    WHERE  tlc.transactionprocessingcomplete = 'N'
    AND    tlc.cdxfaid IS NOT NULL;
    lv_record_count := lv_record_count + SQL%ROWCOUNT;

    pr_update_run_history_step(
      prv_step => '35',
      prv_record_count => lv_record_count);
  EXCEPTION
    WHEN OTHERS THEN NULL;
  END pr_cdx_wf_fa_status;

   ------------------------------------------------------------------------------------------------------------------
  -- Update work orders with date that closure response was sent to retailer
  ------------------------------------------------------------------------------------------------------------------
  PROCEDURE pr_sord_closure_response IS
    lv_record_count INTEGER := 0;
  BEGIN
    pr_create_run_history_step(
      prv_step              => '60',
      prv_step_description  => 'Closure response on work order');
     
    UPDATE db_txnlifecycle tlc
    SET    wmclosureresponsedate = 
           (
            SELECT MAX(src.transactiondate)
			FROM   tm.sordresponseclosure src
			JOIN   cisadm.ci_nt_dwn@cdx_link closase
			  ON   'Envestra-Msg-'||closase.nt_dwn_id = src.messageid
			JOIN   cisadm.ci_wf_evt_ctxt@cdx_link sordclosure
			  ON   closase.nt_dwn_id =RPAD(sordclosure.ctxt_val,50)
			 AND   sordclosure.evt_seq = '950'
			WHERE  src.gdndivision = tlc.gdndivision
			AND    sordclosure.wf_proc_id = tlc.cdxwfprocid
			AND		EXISTS (SELECT 1 FROM cisadm.ci_wf_proc@cdx_link wfp WHERE sordclosure.wf_proc_id = wfp.wf_proc_id AND wfp.wf_proc_tmpl_cd = 'SRVCORDCOMPL')
           )
    WHERE  tlc.transactionprocessingcomplete = 'N'
    AND    tlc.cdxwfprocid IS NOT NULL;

 /*  
    -- Where SORDRESPONSECLOSURE do not have the required value, perform an update where transaction date can be derived by getting
    -- actual NDS ID and linking to ASETXN
    UPDATE db_txnlifecycle tlc
    SET    wmclosureresponsedate = 
    	   (
    	     SELECT MAX(closase.cre_dttm)
    	     FROM   cisadm.ci_wf_evt_ctxt@cdx_link sordclosure
    	     JOIN   cisadm.ci_nt_dwn@cdx_link closase
    	       ON   closase.nt_dwn_id = trim(sordclosure.ctxt_val)
    	     WHERE  sordclosure.wf_proc_id = tlc.cdxwfprocid 
    	     AND    sordclosure.evt_seq = '950'
			 AND 	EXISTS (SELECT 1 FROM cisadm.ci_wf_proc@cdx_link wfp WHERE sordclosure.wf_proc_id = wfp.wf_proc_id AND wfp.wf_proc_tmpl_cd = 'SRVCORDCOMPL')
    	   )
    WHERE  tlc.transactionprocessingcomplete = 'N'
    AND    tlc.wmserviceorder IS NOT NULL    	   
    AND    tlc.wmclosureresponsedate IS NULL;    
 */
    -- Perform a second update to calculate the number of working days that have elapsed from the actual finish date defined
    -- on the work order to the date that the closure response was sent to the retailer
    UPDATE db_txnlifecycle tlc
    SET    finishtoclosureresponsedays = GREATEST(NEXT_DAY(mxowoactfinishdate,'MON') - mxowoactfinishdate - 2,0) +
                                         (NEXT_DAY(wmclosureresponsedate,'MON') - NEXT_DAY(mxowoactfinishdate,'MON'))/7*5 - 
                                         GREATEST(NEXT_DAY(wmclosureresponsedate,'MON') - wmclosureresponsedate - 3,0) -
                                         (
                                           SELECT COUNT(*)
                                           FROM   maximo.workperiod@mx7_link wp
                                           JOIN   gdn_division_xref gdx
                                              ON  gdx.calnum = wp.calnum
                                           WHERE  gdx.gdndivision = tlc.gdndivision
                                           AND    wp.workdate BETWEEN TRUNC(tlc.mxowoactfinishdate) AND TRUNC(tlc.wmclosureresponsedate)
                                           AND    wp.shiftnum = 'HOLIDAY'
                                         )
    WHERE  tlc.transactionprocessingcomplete = 'N'
    AND    tlc.mxowoactfinishdate IS NOT NULL
    AND    tlc.wmclosureresponsedate IS NOT NULL;    

    -- Perform a third update to indicate that processing is complete on service orders that
    -- have had their closure response date populated.  This prevents further processing of these
    -- service orders in subsequent runs
    UPDATE db_txnlifecycle tlc
    SET    transactionprocessingcomplete = 'Y'
    WHERE  tlc.transactionprocessingcomplete = 'N'
    AND    tlc.mxowonum IS NOT NULL
    AND    tlc.wmclosureresponsedate IS NOT NULL;    
    lv_record_count := SQL%ROWCOUNT;
    pr_update_run_history_step(
      prv_step => '60',
      prv_record_count => lv_record_count);
  EXCEPTION
    WHEN OTHERS THEN NULL;
  END pr_sord_closure_response;

 ------------------------------------------------------------------------------------------------------------------
  -- Main controlling procedure for population process
  ------------------------------------------------------------------------------------------------------------------
  PROCEDURE main IS
  BEGIN
    pkv_txnpopid := fn_create_run_history;
/*
    pr_insert_new_sords;
    pr_insert_new_gmns;
    pr_gmn_update;
    pr_sord_rejected;
    pr_sord_initial_response;
*/
	pr_insert_new_dbwo;
	pr_mxo_sr;
    pr_cdx_wf_fa;
    pr_cdx_wf_fa_status;
 /*
	pr_cdx_gmntoaemo;
    pr_sord_cancellation;
 */
    pr_sord_closure_response;
    pr_update_run_history(
      prv_status => 'COMPLETE',
      prv_enddate => SYSDATE);
  END main;

END populate_dbtxnlifecycle;
/

SHOW ERRORS
/