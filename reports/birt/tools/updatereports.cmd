@echo off

set JAVA_HOME=..\..\..\tools\java\jre

rem ------------- set Ant specific settings --------------
rem 
set ANT_HOME=..\..\..\tools\ant
set ANT_OPTS=%ANT_OPTS% -Xmx256m
rem 
rem ------------------------------------------------------

set MBOCLASSES=..\..\..\applications\maximo\businessobjects\classes

if "%1%"=="" (
	rem updatereports
	call %ANT_HOME%\bin\ant -lib "%MBOCLASSES%" -buildfile .\updatereports.xml
	goto end
)

if "%1%"=="savechanges" (
	rem updatereports savechanges
	call %ANT_HOME%\bin\ant -lib "%MBOCLASSES%" -buildfile .\updatereports.xml -Dmaximo.report.birt.savechanges=true
	goto end
)

if "%1%"=="app" (
	if "%2"=="" (
		goto usage
	)
	if "%3"=="savechanges" (
		rem updatereports app [appname] savechanges
		call %ANT_HOME%\bin\ant -lib "%MBOCLASSES%" -buildfile .\updatereports.xml -Dmaximo.report.birt.appname=%2 -Dmaximo.report.birt.savechanges=true %1 
		goto end
	) 

	rem updatereports app [appname]
	call %ANT_HOME%\bin\ant -lib "%MBOCLASSES%" -buildfile .\updatereports.xml -Dmaximo.report.birt.appname=%2 %1 
	goto end
)

if "%1%"=="type" (
	if "%2"=="" (
		goto usage
	)
	if "%3"=="savechanges" (
		rem updatereports type [updatetype] savechanges
		call %ANT_HOME%\bin\ant -lib "%MBOCLASSES%" -buildfile .\updatereports.xml -Dmaximo.report.birt.update.type=%2 -Dmaximo.report.birt.savechanges=true
		goto end
	)
	if "%3%"=="app" (
		if "%4"=="" (
			goto usage
		)
		if "%5"=="savechanges" (
			rem updatereports type [updatetype] app [appname] savechanges
			call %ANT_HOME%\bin\ant -lib "%MBOCLASSES%" -buildfile .\updatereports.xml -Dmaximo.report.birt.update.type=%2 -Dmaximo.report.birt.appname=%4 -Dmaximo.report.birt.savechanges=true %3 
			goto end
		)
	
		rem updatereports type [updatetype] app [appname]
		call %ANT_HOME%\bin\ant -lib "%MBOCLASSES%" -buildfile .\updatereports.xml -Dmaximo.report.birt.update.type=%2 -Dmaximo.report.birt.appname=%4 %3
		goto end
	)
	
	rem updatereports type [updatetype]
	call %ANT_HOME%\bin\ant -lib "%MBOCLASSES%" -buildfile .\updatereports.xml -Dmaximo.report.birt.update.type=%2
	goto end
)

:usage
echo Usage:
echo updatereports
echo    Updates all reports with all available updates.
echo or
echo updatereports savechanges
echo    Updates reports with all updates and saves the modified reports to the database.
echo or
echo updatereports app [appname]
echo    Updates all reports for the specified app with all updates.
echo or
echo updatereports app [appname] savechanges 
echo    Updates all reports for the specified app with all updates and saves the modified reports to the database.
echo or
echo updatereports type [updatetype]
echo    Updates reports with the specified update.
echo or
echo updatereports type [updatetype] savechanges
echo    Updates reports with the specified update and saves the modified reports to the database.
echo or
echo updatereports type [updatetype] app [appname]
echo    Updates all reports for the specified app with the specified update.
echo or
echo updatereports type [updatetype] app [appname] savechanges 
echo    Updates all reports for the specified app with the specified update and saves the modified reports to the database.

echo or
echo updatereports help
echo    Displays this message
goto end

:end
@endlocal
