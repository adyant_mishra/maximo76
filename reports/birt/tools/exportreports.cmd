@echo off

set JAVA_HOME=..\..\..\tools\java\jre

rem ------------- set Ant specific settings --------------
rem 
set ANT_HOME=..\..\..\tools\ant
set ANT_OPTS=%ANT_OPTS% -Xmx256m
rem 
rem ------------------------------------------------------

set MBOCLASSES=..\..\..\applications\maximo\businessobjects\classes

if "%1%"=="report" (

call %ANT_HOME%\bin\ant -lib "%MBOCLASSES%" -buildfile .\exportreports.xml  %1  
goto end
)

if "%1%"=="library" (
call %ANT_HOME%\bin\ant -lib "%MBOCLASSES%" -buildfile .\exportreports.xml %1  
goto end
)

if "%1%"=="app" (
if "%2%"=="" (
goto usage
)

call %ANT_HOME%\bin\ant -lib "%MBOCLASSES%" -buildfile .\exportreports.xml -Dmaximo.report.birt.appname=%2  %1   
goto end
)

if "%1%"=="" (
call %ANT_HOME%\bin\ant -lib "%MBOCLASSES%" -buildfile .\exportreports.xml all  
goto end
)


goto usage


:usage
echo Usage:
echo exportreports
echo      Exports all libraries and reports.
echo or
echo exportreports report
echo      Exports all reports.
echo or
echo exportreports library
echo      Exports all libraries.
echo or
echo exportreports app [appname]
echo      Exports all reports for the specified app.
echo or
echo exportreports help
echo      Displays this message.
goto end
:end
@endlocal
