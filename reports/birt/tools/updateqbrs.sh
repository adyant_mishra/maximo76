#!/bin/sh

showUsageMessage()
{
	echo 'Usage:'
	echo 'updateqbrs'
	echo    'Updates all QBR report designs with all available updates.'
	echo 'or'
	echo 'updateqbrs type [updatetype]'
	echo    'Updates all QBR report designs with the specified update.'
	echo 'or'
	echo 'updateqbrs help'
	echo    'Displays this message'
}

export JAVA_HOME=../../../tools/java/jre

# ------------- export Ant specific settings --------------
# 
export ANT_HOME=../../../tools/ant
export ANT_OPTS="$ANT_OPTS -Xmx256m"
# 
# ------------------------------------------------------

export MBOCLASSES=../../../applications/maximo/businessobjects/classes

if [ -z "$1" ];then
	$ANT_HOME/bin/ant -lib "$MBOCLASSES" -buildfile ./updateqbrs.xml 
elif [ "$1" = "type" ]; then
	if [ -z "$2" ]; then
		showUsageMessage
	else
		$ANT_HOME/bin/ant -lib "$MBOCLASSES" -buildfile ./updateqbrs.xml -Dmaximo.report.birt.update.type=$2 
	fi
else
	showUsageMessage
fi


