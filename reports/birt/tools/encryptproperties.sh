#!/bin/sh

showUsageMessage()
{
	echo 'Usage:'
	echo 'encryptproperties'
	echo   'Encrypts the properties from ./../scriptlibrary/classes/mxreportdatasources.properties to ./../scriptlibrary/classes/mxreportdatasources_enc.properties'
	echo 'or'
	echo 'encryptproperties [properties file]'
	echo    'Encrypts the given file properties'
	echo 'or'
	echo 'encryptproperties [source] [target]'
	echo    'Encrypts the given source to the target file'
	echo 'encryptproperties help'
	echo    'Displays this message'
}

export JAVA_HOME=../../../tools/java/jre

# ------------- export Ant specific settings --------------
# 
export ANT_HOME=../../../tools/ant
export ANT_OPTS="$ANT_OPTS -Xmx256m"
# 
# ------------------------------------------------------

export MBOCLASSES=../../../applications/maximo/businessobjects/classes

if [ -z "$1" ];then
	$ANT_HOME/bin/ant -lib "$MBOCLASSES" -buildfile ./encryptproperties.xml -Dsource=./../scriptlibrary/classes/mxreportdatasources.properties -Dtarget=./../scriptlibrary/classes/mxreportdatasources.properties
elif [ -n "$1" -a -s "$1" ];then
	if [ -z "$2" ];then
		$ANT_HOME/bin/ant -lib "$MBOCLASSES" -buildfile ./encryptproperties.xml -Dsource=$1 -Dtarget=$1
	else
		$ANT_HOME/bin/ant -lib "$MBOCLASSES" -buildfile ./encryptproperties.xml -Dsource=$1 -Dtarget=$2
	fi
else
	showUsageMessage
fi

