@echo off

set JAVA_HOME=..\..\..\tools\java\jre

rem ------------- set Ant specific settings --------------
rem 
set ANT_HOME=..\..\..\tools\ant
set ANT_OPTS=%ANT_OPTS% -Xmx256m
rem 
rem ------------------------------------------------------

set MBOCLASSES=..\..\..\applications\maximo\businessobjects\classes

if "%1"=="" (
	call %ANT_HOME%\bin\ant -lib "%MBOCLASSES%" -buildfile .\encryptproperties.xml -Dsource="./../scriptlibrary/classes/mxreportdatasources.properties" -Dtarget="./../scriptlibrary/classes/mxreportdatasources.properties"
	goto end
) else (
	if exist "%1" (
		if "%2"=="" ( 
			call %ANT_HOME%\bin\ant -lib "%MBOCLASSES%" -buildfile .\encryptproperties.xml -Dsource=%1 -Dtarget=%1
			goto end
		) else (
			call %ANT_HOME%\bin\ant -lib "%MBOCLASSES%" -buildfile .\encryptproperties.xml -Dsource=%1 -Dtarget=%2
			goto end
		)
	) else (
		goto usage
	)
)
goto end

:usage
echo 'Usage:'
	echo 'encryptproperties'
	echo   'Encrypts the properties from ./../scriptlibrary/classes/mxreportdatasources.properties to ./../scriptlibrary/classes/mxreportdatasources_enc.properties'
	echo 'or'
	echo 'encryptproperties [properties file]'
	echo    'Encrypts the given file properties'
	echo 'or'
	echo 'encryptproperties [source] [target]'
	echo    'Encrypts the given source to the target file'
	echo 'encryptproperties help'
	echo    'Displays this message'
goto end

:end
@endlocal
