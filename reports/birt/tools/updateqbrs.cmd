@echo off

set JAVA_HOME=..\..\..\tools\java\jre

rem ------------- set Ant specific settings --------------
rem 
set ANT_HOME=..\..\..\tools\ant
set ANT_OPTS=%ANT_OPTS% -Xmx256m
rem 
rem ------------------------------------------------------

set MBOCLASSES=..\..\..\applications\maximo\businessobjects\classes

if "%1"=="" (
	call %ANT_HOME%\bin\ant -lib "%MBOCLASSES%" -buildfile .\updateqbrs.xml
	goto end
)

if "%1%"=="type" (
		if "%2"=="" (
		goto usage
	)
	call %ANT_HOME%\bin\ant -lib "%MBOCLASSES%" -buildfile .\updateqbrs.xml -Dmaximo.report.birt.update.type=%2 
	goto end
)


:usage
echo Usage:
echo updateqbrs
echo    Updates all QBR report designs with all available updates.
echo or
echo updateqbrs type [updatetype]
echo    Updates all QBR report designs with the specified update.

echo or
echo updateqbrs help
echo    Displays this message
goto end

:end
@endlocal
