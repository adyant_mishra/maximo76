#!/bin/sh

showUsageMessage()
{
	echo 'Usage:'
	echo 'exportreports'
	echo      'Exports all libraries and reports.'
	echo 'or'
	echo 'exportreports report'
	echo      'Exports all reports.'
	echo 'or'
	echo 'exportreports library'
	echo      'Exports all libraries.'
	echo 'or'
	echo 'exportreports app [appname]'
	echo      'Exports all reports for the specified app.'
	echo 'or'
	echo 'exportreports help'
	echo      'Displays this message.'
}

export JAVA_HOME=../../../tools/java/jre

# ------------- export Ant specific settings --------------
# 
export ANT_HOME=../../../tools/ant
export ANT_OPTS="$ANT_OPTS -Xmx256m"
# 
# ------------------------------------------------------

export MBOCLASSES=../../../applications/maximo/businessobjects/classes

if [ "$1" = "report" ] ; then

	$ANT_HOME/bin/ant -lib "$MBOCLASSES" -buildfile ./exportreports.xml  $1  

elif [ "$1" = "library" ] ; then

	$ANT_HOME/bin/ant -lib "$MBOCLASSES" -buildfile ./exportreports.xml $1  

elif [ "$1" = "app" ] ; then
	if [ -z "$2" ];then
		showUsageMessage
	else
		$ANT_HOME/bin/ant -lib "$MBOCLASSES" -buildfile ./exportreports.xml -Dmaximo.report.birt.appname=$2  $1
	fi

elif [ -z "$1" ];then
	$ANT_HOME/bin/ant -lib "$MBOCLASSES" -buildfile ./exportreports.xml all  
else
	showUsageMessage
fi
