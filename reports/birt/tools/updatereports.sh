#!/bin/sh

showUsageMessage() {
	echo 'Usage:'
	echo 'updatereports'
	echo    'Updates all reports with all available updates.'
	echo 'or'
	echo 'updatereports savechanges'
	echo    'Updates reports with all updates and saves the modified reports to the database.'
	echo 'or'
	echo 'updatereports app [appname]'
	echo    'Updates all reports for the specified app with all updates.'
	echo 'or'
	echo 'updatereports app [appname] savechanges '
	echo    'Updates all reports for the specified app with all updates and saves the modified reports to the database.'
	echo 'or'
	echo 'updatereports type [updatetype]'
	echo    'Updates reports with the specified update.'
	echo 'or'
	echo 'updatereports type [updatetype] savechanges'
	echo    'Updates reports with the specified update and saves the modified reports to the database.'
	echo 'or'
	echo 'updatereports type [updatetype] app [appname]'
	echo    Updates all reports for the specified app with the specified update.
	echo 'or'
	echo 'updatereports type [updatetype] app [appname] savechanges'
	echo    'Updates all reports for the specified app with the specified update and saves the modified reports to the database.'
	echo 'or'
	echo 'updatereports help'
	echo    'Displays this message'


}

export JAVA_HOME=../../../tools/java/jre

# ------------- export Ant specific settings --------------
# 
export ANT_HOME=../../../tools/ant
export ANT_OPTS="$ANT_OPTS -Xmx256m"
# 
# ------------------------------------------------------

export MBOCLASSES=../../../applications/maximo/businessobjects/classes

if [ -z "$1" ]; then
	$ANT_HOME/bin/ant -lib "$MBOCLASSES" -buildfile ./updatereports.xml   
elif [ "$1" = "savechanges" ]; then
	$ANT_HOME/bin/ant -lib "$MBOCLASSES" -buildfile ./updatereports.xml  -Dmaximo.report.birt.savechanges=true
elif [ "$1" = "app" ]; then
	if [ -z "$2" ]; then
		showUsageMessage
	else
		$ANT_HOME/bin/ant -lib "$MBOCLASSES" -buildfile ./updatereports.xml -Dmaximo.report.birt.appname=$2 -Dmaximo.report.birt.savechanges=true $1
	fi
elif [ "$1" = "type" ]; then
	if [ -z "$2" ]; then
		showUsageMessage
	elif [ "$3" = "savechanges" ]; then
		$ANT_HOME/bin/ant -lib "$MBOCLASSES" -buildfile ./updatereports.xml -Dmaximo.report.birt.update.type=$2 -Dmaximo.report.birt.savechanges=true
	elif [ "$3" = "app" ]; then
		if [ -z "$4" ]; then
			showUsageMessage
		elif [ "$5" = "savechanges" ]; then
			$ANT_HOME/bin/ant -lib "$MBOCLASSES" -buildfile ./updatereports.xml -Dmaximo.report.birt.update.type=$2 -Dmaximo.report.birt.appname=$4 -Dmaximo.report.birt.savechanges=true
		else
			$ANT_HOME/bin/ant -lib "$MBOCLASSES" -buildfile ./updatereports.xml -Dmaximo.report.birt.update.type=$2 -Dmaximo.report.birt.appname=$4
		fi
	else
		$ANT_HOME/bin/ant -lib "$MBOCLASSES" -buildfile ./updatereports.xml -Dmaximo.report.birt.update.type=$2 -Dmaximo.report.birt.savechanges=true
	fi	
else
	showUsageMessage
fi