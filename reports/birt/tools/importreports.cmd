@echo off

set JAVA_HOME=..\..\..\tools\java\jre

rem ------------- set Ant specific settings --------------
rem 
set ANT_HOME=..\..\..\tools\ant
set ANT_OPTS=%ANT_OPTS% -Xmx256m
rem 
rem ------------------------------------------------------

set MBOCLASSES=..\..\..\applications\maximo\businessobjects\classes

if "%1%"=="" (
	call %ANT_HOME%\bin\ant -lib "%MBOCLASSES%" -buildfile .\importreports.xml  
	goto end
)

if "%1%"=="reports" (
	call %ANT_HOME%\bin\ant -lib "%MBOCLASSES%" -buildfile .\importreports.xml %1 
	goto end
)

if "%1%"=="libraries" (
	call %ANT_HOME%\bin\ant -lib "%MBOCLASSES%" -buildfile .\importreports.xml %1 
	goto end
)

if "%1%"=="report" (
	if "%2"=="" (
		goto usage
	)
	if "%3"=="" (
		goto usage
	)
	call %ANT_HOME%\bin\ant -lib "%MBOCLASSES%" -buildfile .\importreports.xml -Dmaximo.report.birt.appname=%2 -Dmaximo.report.birt.reportname=%3 singlereport
	goto end
)

if "%1%"=="app" (
	if "%2"=="" (
		goto usage
	)

	call %ANT_HOME%\bin\ant -lib "%MBOCLASSES%" -buildfile .\importreports.xml -Dmaximo.report.birt.appname=%2 %1 
	goto end
)



:usage
echo Usage:
echo importreports
echo   Imports all libraries and reports
echo or
echo importreports reports
echo    Imports all reports
echo or
echo importreports report [appname] [report design name]
echo    Imports single report
echo or
echo importreports app [appname]
echo    Imports all reports for the given app
echo or
echo importreports libraries
echo    Imports all libraries
echo or
echo importreports help
echo    Displays this message
goto end

:end
@endlocal
