CONNECT datarec/&MXODATARECPASSWORD

CREATE OR REPLACE PACKAGE maximo_ccb_datarec AS
  PROCEDURE main;
END maximo_ccb_datarec;
/

-- JR 24/11/2016 - DMND0002398
-- Change method of accessing CCB data.  Replace use of ccbdatarec dblink with mbxp dblink, which accesses CCB database directly.
-- In addition, replace use of CCB view names with direct access to table names.

-- JR 14/10/2016 - DMND0002289
-- Further refinement of address comparison.  Strip 'STRALIA' from end of formatted address during comparison to handle case where one address uses AU and  
-- and the other uses AUSTRALIA.

-- JR 28/09/2016 - DMND0002233
-- Exclude state field from address comparison.  Currently, the GIS system is not sending the State field to Maximo whilst it IS sending this to CCB.
-- As such, state needs to be ignored until both systems are consistently populated.
--
-- JR 26/07/2016 - INC0061188
-- Updates to queries involving selecting of MIRN Market Status (MS) from Maximo.  Need to handle case where Market Status has been updated directly in
-- Maximo as opposed to updates that have been sourced via the interface to the Customer Care and Billing system.
-- Derivation of MS (apa_gspstatus) requires incorporating similar logic to that implemented by IBM for non-persistant MS attribute in LOCATIONS object.
-- This involves querying the A_LOCATIONS audit table to find the most recent record based on the apa_marketactfinish column.  However, in the case where 
-- MS has been amended directly by the user in Maximo then apa_marketactfinish will not be populated and eaudittimestamp needs to be used instead.  
-- If no audit record exists for a given location then the apa_gspstatus should be taken from LOCATIONS instead.
CREATE OR REPLACE PACKAGE BODY maximo_ccb_datarec AS

  pkv_recon_id INTEGER;
  
  ------------------------------------------------------------------------------------------------------------------
  -- Write history record
  ------------------------------------------------------------------------------------------------------------------
  FUNCTION fn_create_reconciliation_hist
    RETURN INTEGER IS
  BEGIN
    INSERT INTO ccbdatarec_history (
      recon_id,
      start_date,
      recon_status
    ) VALUES (
      ccbdatarec_seq.NEXTVAL,
      SYSDATE,
      'STARTED'
    );
    COMMIT;
    RETURN ccbdatarec_seq.CURRVAL;    
  END fn_create_reconciliation_hist;
  
  ------------------------------------------------------------------------------------------------------------------
  -- Create history step to record timings
  ------------------------------------------------------------------------------------------------------------------
  PROCEDURE pr_create_recon_hist_step(
    prv_step               INTEGER,
    prv_step_description   VARCHAR2,
    prv_reconcile_issue_id INTEGER DEFAULT NULL
  ) IS
  BEGIN
    INSERT INTO ccbdatarec_history_steps (
      recon_id,
      recon_step_id,
      step_description,
      start_date,
      reconcile_issue_id
    ) VALUES (
      pkv_recon_id,
      prv_step,
      prv_step_description,
      SYSDATE,
      prv_reconcile_issue_id
    );
    COMMIT;
  END pr_create_recon_hist_step;

  ------------------------------------------------------------------------------------------------------------------
  -- Create history step to record timings
  ------------------------------------------------------------------------------------------------------------------
  PROCEDURE pr_update_recon_hist_step(
    prv_step         VARCHAR2,
    prv_record_count INTEGER DEFAULT NULL
  ) IS
  BEGIN
    UPDATE ccbdatarec_history_steps
    SET    end_date = SYSDATE,
           record_count = prv_record_count
    WHERE  recon_id = pkv_recon_id
    AND    recon_step_id = prv_step;
    COMMIT;
  END pr_update_recon_hist_step;

  ------------------------------------------------------------------------------------------------------------------
  -- Update status in ccbdatarec history record
  ------------------------------------------------------------------------------------------------------------------
  PROCEDURE pr_update_reconciliation_hist(
    prv_status   VARCHAR2,
    prv_end_date DATE
  ) IS
  BEGIN
    UPDATE ccbdatarec_history
    SET    recon_status = prv_status,
           end_date = prv_end_date
    WHERE  recon_id = pkv_recon_id;
    COMMIT;
  END pr_update_reconciliation_hist;

  ------------------------------------------------------------------------------------------------------------------
  -- Drop data from previous run
  ------------------------------------------------------------------------------------------------------------------
  PROCEDURE pr_drop_previous_run_data IS
  BEGIN
    pr_create_recon_hist_step(
      prv_step              => '1',
      prv_step_description  => 'Drop previous run data');
    EXECUTE IMMEDIATE 'TRUNCATE TABLE ccb_mirn_details';
    EXECUTE IMMEDIATE 'TRUNCATE TABLE ccb_meter_config_details';
    EXECUTE IMMEDIATE 'TRUNCATE TABLE ccbdatarec_issues';
    EXECUTE IMMEDIATE 'TRUNCATE TABLE maximo_location_gdns';
    pr_update_recon_hist_step(prv_step => '1');
  EXCEPTION
    WHEN OTHERS THEN
      pr_update_recon_hist_step(prv_step => '1');
  END pr_drop_previous_run_data;

  ------------------------------------------------------------------------------------------------------------------
  -- This table is used when reconciling Maximo data to improve performance
  ------------------------------------------------------------------------------------------------------------------
  PROCEDURE pr_populate_location_gdns IS
    lv_record_count INTEGER;
  BEGIN
    pr_create_recon_hist_step(
      prv_step              => '2',
      prv_step_description  => 'Repopulate location GDNs');
    INSERT INTO maximo_location_gdns (
           location,
           gdn_location,
           gdn_division
    )
    SELECT loca.location,
           gdn.gdn_location,
           gdn.gdn_division
    FROM   maximo.locancestor loca
    JOIN   gdn_division_xref gdn
       ON  loca.ancestor = gdn.gdn_location;
    lv_record_count := SQL%ROWCOUNT;
    EXECUTE IMMEDIATE 'ANALYZE TABLE maximo_location_gdns ESTIMATE STATISTICS';
    pr_update_recon_hist_step(
      prv_step => '2',
      prv_record_count => lv_record_count);
  EXCEPTION
    WHEN OTHERS THEN NULL;
  END pr_populate_location_gdns;

  ------------------------------------------------------------------------------------------------------------------
  -- Populate temporary tables with CCB MIRN data
  ------------------------------------------------------------------------------------------------------------------
  PROCEDURE pr_copy_ccb_mirn_data IS
    lv_record_count INTEGER;
    lv_dummy VARCHAR2(1);
  BEGIN
    pr_create_recon_hist_step(
      prv_step              => '10',
      prv_step_description  => 'Copy CCB MIRN data');
    -- Perform a dummy select from the CCB database to force synchronisation of system change numbers across databases
    SELECT 'x'
    INTO   lv_dummy
    FROM   cisadm.ci_sp@mbxp sp
    WHERE  rownum < 2;
    
    INSERT INTO ccb_mirn_details (
           sp_id,
           mirn,
           mirn_status,
           mirn_status_eff_dttm,
           prem_id,
           prem_address,
           prem_cust_class,
           cis_division
    )
    SELECT /*+ USE_HASH(sp, geo, spc, spc2, prem, prc, prc2) */
           sp.sp_id,
           geo.geo_val AS mirn,
           spc.char_val AS mirn_status,
           spc.effdt AS mirn_status_eff_dttm,
           sp.prem_id,
           UPPER(SUBSTR(REPLACE(prem.address1,'(',' ('),1,INSTR(REPLACE(prem.address1,'(',' ('),' ')-1) || ' ' || 
             prem.address2 || SUBSTR(REPLACE(prem.address1,'(',' ('),INSTR(REPLACE(prem.address1,'(',' ('),' ')) || ' ' || 
             prem.city || ' ' || TRUNC(prem.postal) || ' ' || 
             (CASE WHEN country='AUS' THEN 'AUSTRALIA' ELSE country END)) AS prem_address,
           prc.char_val AS prem_cust_class,  
           RTRIM(prem.cis_division) AS cis_division
    FROM   cisadm.ci_sp@mbxp sp
    JOIN   cisadm.ci_sp_geo@mbxp geo
       ON  geo.sp_id = sp.sp_id
       AND geo.geo_type_cd = 'MIRNNBR'
    JOIN   cisadm.ci_sp_char@mbxp spc
       ON  spc.sp_id = sp.sp_id
       AND spc.char_type_cd = 'MIRNSTUS'
    JOIN   cisadm.ci_prem@mbxp prem
       ON  prem.prem_id = sp.prem_id
    LEFT OUTER JOIN cisadm.ci_prem_char@mbxp prc
       ON  prc.prem_id = prem.prem_id
       AND prc.char_type_cd = 'CUSTCLAS'
    WHERE NOT EXISTS
          (
            SELECT 'x'
            FROM   cisadm.ci_sp_char@mbxp spc2
            WHERE  spc2.sp_id = spc.sp_id
            AND    spc2.char_type_cd = 'MIRNSTUS'
            AND    spc2.effdt > spc.effdt
          )
    AND NOT EXISTS
          (
            SELECT 'x'
            FROM   cisadm.ci_prem_char@mbxp prc2
            WHERE  prc2.prem_id = prc.prem_id
            AND    prc2.char_type_cd = 'CUSTCLAS'
            AND    prc2.effdt > prc.effdt
          );
    lv_record_count := SQL%ROWCOUNT;
    EXECUTE IMMEDIATE 'ANALYZE TABLE ccb_mirn_details ESTIMATE STATISTICS';
    pr_update_recon_hist_step(
      prv_step => '10',
      prv_record_count => lv_record_count);
  END pr_copy_ccb_mirn_data;

  ------------------------------------------------------------------------------------------------------------------
  -- Populate temporary tables with CCB meter data
  ------------------------------------------------------------------------------------------------------------------
  PROCEDURE pr_copy_ccb_meter_config_data IS
    lv_record_count INTEGER;
  BEGIN
    pr_create_recon_hist_step(
      prv_step               => '11',
      prv_step_description  => 'Copy CCB Meter Config data');
    INSERT INTO ccb_meter_config_details (
           mtr_id,
           badge_nbr,
           serial_nbr,
           mtr_config_id,
           eff_dttm,
           install_date,
           install_const,
           pcf_zone,
           sp_mtr_hist_id,
           sp_id,
           removal_dttm,
           mirn,
           mirn_status,
           mirn_status_eff_dttm,
           prem_id,
           prem_address,
           cis_division
    )
    SELECT /*+ USE_HASH(smh, sme, mr, sp, geo, spc, spc2, prem, prc, prc2) */
           con.mtr_id,
           RTRIM(mtr.badge_nbr),
           RTRIM(mtr.serial_nbr),
           mr.mtr_config_id,
           con.eff_dttm,
           mr.read_dttm AS install_date,
           smh.install_const,
           RTRIM(prc.char_val) AS pcf_zone,
           smh.sp_mtr_hist_id,
           smh.sp_id,
           smh.removal_dttm,
           geo.geo_val AS mirn,
           spc.char_val AS mirn_status,
           spc.effdt AS mirn_status_eff_dttm,
           sp.prem_id,
           UPPER(SUBSTR(prem.address1,1,INSTR(prem.address1,' ')-1) || ' ' || 
             prem.address2 || SUBSTR(prem.address1,INSTR(prem.address1,' ')) || ' ' || 
             prem.city || ' ' || TRUNC(prem.postal) || ' ' || 
             (CASE WHEN country='AUS' THEN 'AUSTRALIA' ELSE country END)) AS prem_address,
           RTRIM(prem.cis_division) AS cis_division
    FROM   cisadm.ci_sp_mtr_hist@mbxp smh
    JOIN   cisadm.ci_sp_mtr_evt@mbxp sme
       ON  sme.sp_mtr_hist_id = smh.sp_mtr_hist_id
    JOIN   cisadm.ci_mr@mbxp mr
       ON  mr.mr_id = sme.mr_id
    JOIN   cisadm.ci_mtr_config@mbxp con
       ON  con.mtr_config_id = smh.mtr_config_id
    JOIN   cisadm.ci_mtr@mbxp mtr
       ON  mtr.mtr_id = con.mtr_id
    JOIN   cisadm.ci_sp@mbxp sp
       ON  sp.sp_id = smh.sp_id
    JOIN   cisadm.ci_sp_geo@mbxp geo
       ON  geo.sp_id = smh.sp_id
       AND geo.geo_type_cd = 'MIRNNBR'
    JOIN   cisadm.ci_sp_char@mbxp spc
       ON  spc.sp_id = sp.sp_id
       AND spc.char_type_cd = 'MIRNSTUS'
    JOIN   cisadm.ci_prem@mbxp prem
       ON  prem.prem_id = sp.prem_id
    LEFT OUTER JOIN cisadm.ci_prem_char@mbxp prc
       ON  prc.prem_id = prem.prem_id
       AND prc.char_type_cd = 'PCFZONE'
    WHERE  sme.sp_mtr_evt_flg = 'I'
    AND    smh.removal_dttm IS NULL
    AND NOT EXISTS
          (
            SELECT 'x'
            FROM   cisadm.ci_sp_char@mbxp spc2
            WHERE  spc2.sp_id = spc.sp_id
            AND    spc2.char_type_cd = 'MIRNSTUS'
            AND    spc2.effdt > spc.effdt
          )
    AND NOT EXISTS
          (
            SELECT 'x'
            FROM   cisadm.ci_prem_char@mbxp prc2
            WHERE  prc2.prem_id = prc.prem_id
            AND    prc2.char_type_cd = 'PCFZONE'
            AND    prc2.effdt > prc.effdt
          );
    lv_record_count := SQL%ROWCOUNT;
    EXECUTE IMMEDIATE 'ANALYZE TABLE ccb_meter_config_details ESTIMATE STATISTICS';
    pr_update_recon_hist_step(
      prv_step => '11',
      prv_record_count => lv_record_count);
  END pr_copy_ccb_meter_config_data;

  ------------------------------------------------------------------------------------------------------------------
  -- Reconcile MIRNs
  ------------------------------------------------------------------------------------------------------------------
  PROCEDURE pr_reconcile_mirns IS
    lv_record_count INTEGER;
  BEGIN
    ---------------------------------------------------------
    -- No MIRN defined on Maximo location that is supposed to be sent to CCB
    ---------------------------------------------------------
    pr_create_recon_hist_step(
      prv_step               => '20',
      prv_step_description   => 'MIRN id missing on Maximo location',
      prv_reconcile_issue_id => '10');
    INSERT INTO ccbdatarec_issues (
           reconcile_issue_id,
           maximo_location,
           maximo_location_status,
           maximo_location_status_date,
           maximo_market_status,
           maximo_gdn
    )
    SELECT /*+ USE_HASH(loc, lspccb, lspmirn, gdn, aloc aloc2) */
           '10',
           loc.location,
           loc.status,
           loc.statusdate,
           COALESCE(aloc.apa_gspstatus,loc.apa_gspstatus),
           gdn.gdn_division
    FROM   maximo.locations loc
    LEFT OUTER JOIN maximo.a_locations aloc
       ON  aloc.siteid = loc.siteid
       AND aloc.location = loc.location
    JOIN   maximo.locationspec lspccb
       ON  loc.siteid = lspccb.siteid
       AND loc.location = lspccb.location
       AND lspccb.assetattrid = 'SEND_TO_CCB'
       AND lspccb.alnvalue = 'Y'
    LEFT OUTER JOIN maximo.locationspec lspmirn
       ON  loc.siteid = lspmirn.siteid
       AND loc.location = lspmirn.location
       AND lspmirn.assetattrid = 'MIRN'
    LEFT OUTER JOIN maximo_location_gdns gdn
       ON  loc.location = gdn.location
    WHERE  lspmirn.alnvalue IS NULL
    AND NOT EXISTS
           (
             SELECT 'x'
             FROM   maximo.a_locations aloc2
             WHERE  aloc2.siteid = aloc.siteid
             AND    aloc2.location = aloc.location
             AND    aloc2.apa_marketactfinish = aloc.apa_marketactfinish
             AND    aloc2.eaudittimestamp > aloc.eaudittimestamp
           )
    AND NOT EXISTS
           (
             SELECT 'x'
             FROM   maximo.a_locations aloc2
             WHERE  aloc2.siteid = aloc.siteid
             AND    aloc2.location = aloc.location
             AND    COALESCE(aloc2.apa_marketactfinish,aloc2.eaudittimestamp) > COALESCE(aloc.apa_marketactfinish,aloc.eaudittimestamp)
           );
    lv_record_count := SQL%ROWCOUNT;
    pr_update_recon_hist_step(
      prv_step => '20',
      prv_record_count => lv_record_count);
    ---------------------------------------------------------

    ---------------------------------------------------------
    -- No GDN associated with Maximo location that is supposed to be sent to CCB
    ---------------------------------------------------------
    pr_create_recon_hist_step(
      prv_step               => '21',
      prv_step_description   => 'No GDN associated with Maximo location',
      prv_reconcile_issue_id => '11');
    INSERT INTO ccbdatarec_issues (
           reconcile_issue_id,
           maximo_location,
           maximo_location_status,
           maximo_location_status_date,
           maximo_market_status,
           maximo_gdn
    )
    SELECT /*+ USE_HASH(loc, lspccb, lspmirn, gdn, aloc aloc2) */
           '11',
           loc.location,
           loc.status,
           loc.statusdate,
           COALESCE(aloc.apa_gspstatus,loc.apa_gspstatus),
           gdn.gdn_division
    FROM   maximo.locations loc
    LEFT OUTER JOIN maximo.a_locations aloc
       ON  aloc.siteid = loc.siteid
       AND aloc.location = loc.location
    JOIN   maximo.locationspec lspccb
       ON  loc.siteid = lspccb.siteid
       AND loc.location = lspccb.location
       AND lspccb.assetattrid = 'SEND_TO_CCB'
       AND lspccb.alnvalue = 'Y'
    LEFT OUTER JOIN maximo.locationspec lspmirn
       ON  loc.siteid = lspmirn.siteid
       AND loc.location = lspmirn.location
       AND lspmirn.assetattrid = 'MIRN'
    LEFT OUTER JOIN maximo_location_gdns gdn
       ON  loc.location = gdn.location
    WHERE  gdn.gdn_division IS NULL
    AND NOT EXISTS
           (
             SELECT 'x'
             FROM   maximo.a_locations aloc2
             WHERE  aloc2.siteid = aloc.siteid
             AND    aloc2.location = aloc.location
             AND    aloc2.apa_marketactfinish = aloc.apa_marketactfinish
             AND    aloc2.eaudittimestamp > aloc.eaudittimestamp
           )
    AND NOT EXISTS
           (
             SELECT 'x'
             FROM   maximo.a_locations aloc2
             WHERE  aloc2.siteid = aloc.siteid
             AND    aloc2.location = aloc.location
             AND    COALESCE(aloc2.apa_marketactfinish,aloc2.eaudittimestamp) > COALESCE(aloc.apa_marketactfinish,aloc.eaudittimestamp)
           );
    lv_record_count := SQL%ROWCOUNT;
    pr_update_recon_hist_step(
      prv_step => '21',
      prv_record_count => lv_record_count);
    ---------------------------------------------------------

    ---------------------------------------------------------
    -- Maximo MIRN not found in CCB
    ---------------------------------------------------------
    pr_create_recon_hist_step(
      prv_step               => '22',
      prv_step_description   => 'Maximo MIRN missing in CCB',
      prv_reconcile_issue_id => '12');
    lv_record_count := NULL;
    INSERT INTO ccbdatarec_issues (
           reconcile_issue_id,
           maximo_location,
           maximo_location_status,
           maximo_location_status_date,
           maximo_market_status,
           maximo_mirn,
           maximo_gdn
    )
    SELECT /*+ USE_HASH(loc, lspccb, lspmirn, gdn, cmd, aloc aloc2) */
           '12',
           loc.location,
           loc.status,
           loc.statusdate,
           COALESCE(aloc.apa_gspstatus,loc.apa_gspstatus),
           lspmirn.alnvalue AS mirn,
           gdn.gdn_division
    FROM   maximo.locations loc
    LEFT OUTER JOIN maximo.a_locations aloc
       ON  aloc.siteid = loc.siteid
       AND aloc.location = loc.location
    JOIN   maximo.locationspec lspccb
       ON  loc.siteid = lspccb.siteid
       AND loc.location = lspccb.location
       AND lspccb.assetattrid = 'SEND_TO_CCB'
       AND lspccb.alnvalue = 'Y'
    JOIN maximo.locationspec lspmirn
       ON  loc.siteid = lspmirn.siteid
       AND loc.location = lspmirn.location
       AND lspmirn.assetattrid = 'MIRN'
       AND lspmirn.alnvalue IS NOT NULL
    LEFT OUTER JOIN maximo_location_gdns gdn
       ON  loc.location = gdn.location
    LEFT OUTER JOIN ccb_mirn_details cmd
       ON  cmd.mirn =  lspmirn.alnvalue
    WHERE  cmd.mirn IS NULL
    AND NOT EXISTS
           (
             SELECT 'x'
             FROM   maximo.a_locations aloc2
             WHERE  aloc2.siteid = aloc.siteid
             AND    aloc2.location = aloc.location
             AND    aloc2.apa_marketactfinish = aloc.apa_marketactfinish
             AND    aloc2.eaudittimestamp > aloc.eaudittimestamp
           )
    AND NOT EXISTS
           (
             SELECT 'x'
             FROM   maximo.a_locations aloc2
             WHERE  aloc2.siteid = aloc.siteid
             AND    aloc2.location = aloc.location
             AND    COALESCE(aloc2.apa_marketactfinish,aloc2.eaudittimestamp) > COALESCE(aloc.apa_marketactfinish,aloc.eaudittimestamp)
           );
    lv_record_count := SQL%ROWCOUNT;
    pr_update_recon_hist_step(
      prv_step => '22',
      prv_record_count => lv_record_count);
    ---------------------------------------------------------

    ---------------------------------------------------------
    -- CCB MIRN not found in Maximo
    ---------------------------------------------------------
    pr_create_recon_hist_step(
      prv_step               => '23',
      prv_step_description   => 'CCB MIRN missing in Maximo',
      prv_reconcile_issue_id => '13');
    lv_record_count := NULL;
    INSERT INTO ccbdatarec_issues (
           reconcile_issue_id,
           ccb_prem_id,
           ccb_sp_id,
           ccb_mirn,
           ccb_mirn_status,
           ccb_mirn_status_eff_dttm,
           ccb_cis_division
    )
    SELECT /*+ USE_HASH(lspmirn, cmd) */
           '13',
           cmd.prem_id,
           cmd.sp_id,
           cmd.mirn,
           cmd.mirn_status,
           cmd.mirn_status_eff_dttm,
           cmd.cis_division
    FROM   ccb_mirn_details cmd
    LEFT OUTER JOIN maximo.locationspec lspmirn
       ON  lspmirn.assetattrid = 'MIRN'
       AND lspmirn.alnvalue = cmd.mirn
    WHERE  lspmirn.alnvalue IS NULL;
    lv_record_count := SQL%ROWCOUNT;
    pr_update_recon_hist_step(
      prv_step => '23',
      prv_record_count => lv_record_count);
    ---------------------------------------------------------

    ---------------------------------------------------------
    -- MIRN/Address mismatch
    ---------------------------------------------------------
    pr_create_recon_hist_step(
      prv_step               => '24',
      prv_step_description   => 'MIRN/Address mismatch',
      prv_reconcile_issue_id => '14');
    lv_record_count := NULL;
    INSERT INTO ccbdatarec_issues (
           reconcile_issue_id,
           maximo_location,
           maximo_location_status,
           maximo_location_status_date,
           maximo_market_status,
           maximo_location_address,
           maximo_mirn,
           maximo_gdn,
           ccb_prem_id,
           ccb_prem_address,
           ccb_sp_id,
           ccb_mirn,
           ccb_mirn_status,
           ccb_mirn_status_eff_dttm,
           ccb_cis_division
    )
    SELECT /*+ USE_HASH(loc, lspmirn, gdn, cmd, sad, aloc aloc2) */
           '14',
           loc.location,
           loc.status,
           loc.statusdate,
           COALESCE(aloc.apa_gspstatus,loc.apa_gspstatus),
           UPPER(REGEXP_REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(sad.formattedaddress,' ' || stateprovince || ' ',' '),' ' || stateprovince || ',',' '),',',' '),'(',' ('),'(Lot ','(L'),'( ){2,}', ' ')) AS maximo_location_address,
           cmd.mirn,
           gdn.gdn_division,
           cmd.prem_id,
           REGEXP_REPLACE(cmd.prem_address,'( ){2,}', ' ') AS ccb_prem_address,
           cmd.sp_id,
           cmd.mirn,
           cmd.mirn_status,
           cmd.mirn_status_eff_dttm,
           cmd.cis_division
    FROM   ccb_mirn_details cmd
    JOIN   maximo.locationspec lspmirn
       ON  lspmirn.assetattrid = 'MIRN'
       AND cmd.mirn = lspmirn.alnvalue
    JOIN   maximo.locations loc
       ON  loc.siteid = lspmirn.siteid
       AND loc.location = lspmirn.location
    LEFT OUTER JOIN maximo.a_locations aloc
       ON  aloc.siteid = loc.siteid
       AND aloc.location = loc.location
    JOIN   maximo.serviceaddress sad
       ON  loc.siteid = sad.siteid
       AND loc.saddresscode = sad.addresscode
    LEFT OUTER JOIN maximo_location_gdns gdn
       ON  loc.location = gdn.location
    WHERE  RTRIM(LTRIM(REGEXP_REPLACE(
            CASE
              WHEN INSTR(SUBSTR(cmd.prem_address,1,INSTR(cmd.prem_address,' ')-1),'-') > 0 THEN SUBSTR(cmd.prem_address,1,INSTR(cmd.prem_address,'-')-1) || SUBSTR(cmd.prem_address,INSTR(cmd.prem_address,' '))
              ELSE cmd.prem_address
            END
           ,'( ){2,}', ' ')),'STRALIA') != 
           RTRIM(LTRIM(UPPER(REGEXP_REPLACE(REPLACE(REPLACE(
            CASE
              WHEN INSTR(SUBSTR(sad.formattedaddress,1,INSTR(REPLACE(sad.formattedaddress,'(',' ('),' ')-1),'-') > 0 THEN REPLACE(REPLACE(SUBSTR(sad.formattedaddress,1,INSTR(sad.formattedaddress,'-')-1) || SUBSTR(REPLACE(sad.formattedaddress,'(',' ('),INSTR(REPLACE(sad.formattedaddress,'(',' ('),' ')),' ' || stateprovince || ' ',' '),' ' || stateprovince || ',',' ')
              ELSE REPLACE(REPLACE(REPLACE(sad.formattedaddress,'(',' ('),' ' || stateprovince || ' ',' '),' ' || stateprovince || ',',' ')
            END
           ,',',' '),'(Lot ','(L'),'( ){2,}', ' '))),'STRALIA')
    AND NOT EXISTS
           (
             SELECT 'x'
             FROM   maximo.a_locations aloc2
             WHERE  aloc2.siteid = aloc.siteid
             AND    aloc2.location = aloc.location
             AND    aloc2.apa_marketactfinish = aloc.apa_marketactfinish
             AND    aloc2.eaudittimestamp > aloc.eaudittimestamp
           )
    AND NOT EXISTS
           (
             SELECT 'x'
             FROM   maximo.a_locations aloc2
             WHERE  aloc2.siteid = aloc.siteid
             AND    aloc2.location = aloc.location
             AND    COALESCE(aloc2.apa_marketactfinish,aloc2.eaudittimestamp) > COALESCE(aloc.apa_marketactfinish,aloc.eaudittimestamp)
           );
    lv_record_count := SQL%ROWCOUNT;
    pr_update_recon_hist_step(
      prv_step => '24',
      prv_record_count => lv_record_count);
    ---------------------------------------------------------

    ---------------------------------------------------------
    -- MIRN/Status mismatch
    ---------------------------------------------------------
    pr_create_recon_hist_step(
      prv_step               => '25',
      prv_step_description   => 'MIRN/Status mismatch',
      prv_reconcile_issue_id => '15');
    lv_record_count := NULL;
    INSERT INTO ccbdatarec_issues (
           reconcile_issue_id,
           maximo_location,
           maximo_location_status,
           maximo_location_status_date,
           maximo_market_status,
           maximo_mirn,
           maximo_gdn,
           ccb_prem_id,
           ccb_sp_id,
           ccb_mirn,
           ccb_mirn_status,
           ccb_mirn_status_eff_dttm,
           ccb_cis_division
    )
    SELECT /*+ USE_HASH(loc, lspmirn, gdn, cmd, aloc aloc2) */
           '15',
           loc.location,
           loc.status,
           loc.statusdate,
           COALESCE(aloc.apa_gspstatus,loc.apa_gspstatus),
           cmd.mirn,
           gdn.gdn_division,
           cmd.prem_id,
           cmd.sp_id,
           cmd.mirn,
           cmd.mirn_status,
           cmd.mirn_status_eff_dttm,
           cmd.cis_division
    FROM   ccb_mirn_details cmd
    JOIN   maximo.locationspec lspmirn
       ON  lspmirn.assetattrid = 'MIRN'
       AND cmd.mirn = lspmirn.alnvalue
    JOIN   maximo.locations loc
       ON  loc.siteid = lspmirn.siteid
       AND loc.location = lspmirn.location
    LEFT OUTER JOIN maximo.a_locations aloc
       ON  aloc.siteid = loc.siteid
       AND aloc.location = loc.location
    LEFT OUTER JOIN maximo_location_gdns gdn
       ON  loc.location = gdn.location
    WHERE  RTRIM(cmd.mirn_status) != COALESCE(aloc.apa_gspstatus,loc.apa_gspstatus)
    AND NOT EXISTS
           (
             SELECT 'x'
             FROM   maximo.a_locations aloc2
             WHERE  aloc2.siteid = aloc.siteid
             AND    aloc2.location = aloc.location
             AND    aloc2.apa_marketactfinish = aloc.apa_marketactfinish
             AND    aloc2.eaudittimestamp > aloc.eaudittimestamp
           )
    AND NOT EXISTS
           (
             SELECT 'x'
             FROM   maximo.a_locations aloc2
             WHERE  aloc2.siteid = aloc.siteid
             AND    aloc2.location = aloc.location
             AND    COALESCE(aloc2.apa_marketactfinish,aloc2.eaudittimestamp) > COALESCE(aloc.apa_marketactfinish,aloc.eaudittimestamp)
           );
    lv_record_count := SQL%ROWCOUNT;
    pr_update_recon_hist_step(
      prv_step => '25',
      prv_record_count => lv_record_count);
    ---------------------------------------------------------

    ---------------------------------------------------------
    -- MIRN/Pressure mismatch
    ---------------------------------------------------------
    pr_create_recon_hist_step(
      prv_step               => '26',
      prv_step_description   => 'MIRN/Pressure mismatch',
      prv_reconcile_issue_id => '16');
    lv_record_count := NULL;
    INSERT INTO ccbdatarec_issues (
           reconcile_issue_id,
           maximo_location,
           maximo_location_status,
           maximo_location_status_date,
           maximo_market_status,
           maximo_mirn,
           maximo_gdn,
           maximo_install_const,
           maximo_install_kpa,
           ccb_prem_id,
           ccb_sp_id,
           ccb_mirn,
           ccb_mirn_status,
           ccb_mirn_status_eff_dttm,
           ccb_mtr_id,
           ccb_serial_nbr,
           ccb_badge_nbr,
           ccb_install_const,
           ccb_install_kpa,
           ccb_cis_division
    )
    SELECT /*+ USE_HASH(loc, lsppre, lspmirn, mpx, gdn, mcd, mcd2, aloc aloc2) */
           '16',
           loc.location,
           loc.status,
           loc.statusdate,
           COALESCE(aloc.apa_gspstatus,loc.apa_gspstatus),
           lspmirn.alnvalue AS maximo_mirn,
           gdn.gdn_division,
           maxmpx.install_const AS maximo_install_const,
           COALESCE(lsppre.alnvalue,TO_CHAR(lsppre.numvalue)) AS maximo_install_kpa,
           mcd.prem_id,
           mcd.sp_id,
           mcd.mirn,
           mcd.mirn_status,
           mcd.mirn_status_eff_dttm,
           mcd.mtr_id,
           mcd.serial_nbr,
           mcd.badge_nbr,
           mcd.install_const,
           ccbmpx.char_val AS kpa,
           mcd.cis_division
    FROM   maximo.locations loc
    LEFT OUTER JOIN maximo.a_locations aloc
       ON  aloc.siteid = loc.siteid
       AND aloc.location = loc.location
    JOIN   maximo.locationspec lspmirn
       ON  loc.siteid = lspmirn.siteid
       AND loc.location = lspmirn.location
       AND lspmirn.assetattrid = 'MIRN'
    JOIN   maximo.locationspec lsppre
       ON  loc.siteid = lsppre.siteid
       AND loc.location = lsppre.location
       AND lsppre.assetattrid = 'PRESS'
    JOIN   ccb_meter_config_details mcd
       ON  mcd.mirn = lspmirn.alnvalue
    LEFT OUTER JOIN meter_pressure_xref maxmpx
       ON  maxmpx.kpa = COALESCE(lsppre.alnvalue,TO_CHAR(lsppre.numvalue))
       AND maxmpx.gdn_division = mcd.cis_division
       AND COALESCE(maxmpx.pcf_zone,'{') = COALESCE(mcd.pcf_zone,'{')
    LEFT OUTER JOIN cisadm.ci_mtr_char@mbxp ccbmpx
       ON mcd.mtr_id = ccbmpx.mtr_id
       AND ccbmpx.char_type_cd = 'MPR'
    LEFT OUTER JOIN maximo_location_gdns gdn
       ON  loc.location = gdn.location
    WHERE NOT EXISTS
          (
             SELECT 'x'
             FROM   ccb_meter_config_details mcd2
             WHERE  mcd2.mirn = mcd.mirn
             AND    mcd2.eff_dttm > mcd.eff_dttm
          )
    AND   TO_NUMBER(mcd.install_const) != TO_NUMBER(COALESCE(maxmpx.install_const,'-1'))
    AND NOT EXISTS
           (
             SELECT 'x'
             FROM   maximo.a_locations aloc2
             WHERE  aloc2.siteid = aloc.siteid
             AND    aloc2.location = aloc.location
             AND    aloc2.apa_marketactfinish = aloc.apa_marketactfinish
             AND    aloc2.eaudittimestamp > aloc.eaudittimestamp
           )
    AND NOT EXISTS
           (
             SELECT 'x'
             FROM   maximo.a_locations aloc2
             WHERE  aloc2.siteid = aloc.siteid
             AND    aloc2.location = aloc.location
             AND    COALESCE(aloc2.apa_marketactfinish,aloc2.eaudittimestamp) > COALESCE(aloc.apa_marketactfinish,aloc.eaudittimestamp)
           )
    AND NOT EXISTS 
        (
          SELECT 'x'
          FROM cisadm.ci_mtr_char@mbxp mtrc
          WHERE mtrc.mtr_id = ccbmpx.mtr_id
          AND mtrc.char_type_cd = 'MPR'
          AND mtrc.effdt > ccbmpx.effdt
        );

    lv_record_count := SQL%ROWCOUNT;
    pr_update_recon_hist_step(
      prv_step => '26',
      prv_record_count => lv_record_count);
    ---------------------------------------------------------

    ---------------------------------------------------------
    -- MIRN/Customer/Consumer Class mismatch
    ---------------------------------------------------------
    pr_create_recon_hist_step(
      prv_step               => '27',
      prv_step_description   => 'MIRN/Customer/Consumer Class mismatch',
      prv_reconcile_issue_id => '17');
    lv_record_count := NULL;
    INSERT INTO ccbdatarec_issues (
           reconcile_issue_id,
           maximo_location,
           maximo_location_status,
           maximo_location_status_date,
           maximo_market_status,
           maximo_mirn,
           maximo_gdn,
           maximo_consumer_class,
           ccb_prem_id,
           ccb_sp_id,
           ccb_mirn,
           ccb_mirn_status,
           ccb_mirn_status_eff_dttm,
           ccb_prem_cust_class,
           ccb_cis_division
    )
    SELECT /*+ USE_HASH(loc, lspcc, lspmirn, gdn, cmd, aloc aloc2) */
           '17',
           loc.location,
           loc.status,
           loc.statusdate,
           COALESCE(aloc.apa_gspstatus,loc.apa_gspstatus),
           lspmirn.alnvalue AS maximo_mirn,
           gdn.gdn_division,
           lspcc.alnvalue AS maximo_consumer_class,
           cmd.prem_id,
           cmd.sp_id,
           cmd.mirn,
           cmd.mirn_status,
           cmd.mirn_status_eff_dttm,
           cmd.prem_cust_class,
           cmd.cis_division
    FROM   ccb_mirn_details cmd
    JOIN   maximo.locationspec lspmirn
       ON  lspmirn.assetattrid = 'MIRN'
       AND cmd.mirn = lspmirn.alnvalue
    JOIN   maximo.locations loc
       ON  loc.siteid = lspmirn.siteid
       AND loc.location = lspmirn.location
    LEFT OUTER JOIN maximo.a_locations aloc
       ON  aloc.siteid = loc.siteid
       AND aloc.location = loc.location
    LEFT OUTER JOIN maximo.locationspec lspcc
       ON  lspmirn.siteid = lspcc.siteid
       AND lspmirn.location = lspcc.location
       AND lspcc.assetattrid = 'CONSMR_TYP'
    LEFT OUTER JOIN maximo_location_gdns gdn
       ON  lspmirn.location = gdn.location
    WHERE  COALESCE(SUBSTR(cmd.prem_cust_class,1,1),'{') != COALESCE(lspcc.alnvalue,'{')
    AND NOT EXISTS
           (
             SELECT 'x'
             FROM   maximo.a_locations aloc2
             WHERE  aloc2.siteid = aloc.siteid
             AND    aloc2.location = aloc.location
             AND    aloc2.apa_marketactfinish = aloc.apa_marketactfinish
             AND    aloc2.eaudittimestamp > aloc.eaudittimestamp
           )
    AND NOT EXISTS
           (
             SELECT 'x'
             FROM   maximo.a_locations aloc2
             WHERE  aloc2.siteid = aloc.siteid
             AND    aloc2.location = aloc.location
             AND    COALESCE(aloc2.apa_marketactfinish,aloc2.eaudittimestamp) > COALESCE(aloc.apa_marketactfinish,aloc.eaudittimestamp)
           );
        
    lv_record_count := SQL%ROWCOUNT;
    pr_update_recon_hist_step(
      prv_step => '27',
      prv_record_count => lv_record_count);
    ---------------------------------------------------------

    ---------------------------------------------------------
    -- CCB MIRN Exists in Maximo With Send to CCB = N
    ---------------------------------------------------------
    pr_create_recon_hist_step(
      prv_step               => '28',
      prv_step_description   => 'CCB MIRN Exists in Maximo With Send to CCB = N',
      prv_reconcile_issue_id => '18');
    lv_record_count := NULL;
    INSERT INTO ccbdatarec_issues (
           reconcile_issue_id,
           maximo_location,
           maximo_location_status,
           maximo_location_status_date,
           maximo_market_status,
           maximo_mirn,
           maximo_gdn,
           ccb_prem_id,
           ccb_sp_id,
           ccb_mirn,
           ccb_mirn_status,
           ccb_mirn_status_eff_dttm,
           ccb_cis_division
    )
    SELECT /*+ USE_HASH(loc, lspmirn, lspccb, gdn, cmd, aloc aloc2) */
           '18',
           loc.location,
           loc.status,
           loc.statusdate,
           COALESCE(aloc.apa_gspstatus,loc.apa_gspstatus),
           cmd.mirn,
           gdn.gdn_division,
           cmd.prem_id,
           cmd.sp_id,
           cmd.mirn,
           cmd.mirn_status,
           cmd.mirn_status_eff_dttm,
           cmd.cis_division
    FROM   ccb_mirn_details cmd
    JOIN   maximo.locationspec lspmirn
       ON  lspmirn.assetattrid = 'MIRN'
       AND cmd.mirn = lspmirn.alnvalue
    JOIN   maximo.locations loc
       ON  loc.siteid = lspmirn.siteid
       AND loc.location = lspmirn.location
    JOIN   maximo.locationspec lspccb
       ON  lspccb.assetattrid = 'SEND_TO_CCB'
       AND lspccb.siteid = loc.siteid
       AND lspccb.location = loc.location
    LEFT OUTER JOIN maximo.a_locations aloc
       ON  aloc.siteid = loc.siteid
       AND aloc.location = loc.location
    LEFT OUTER JOIN maximo_location_gdns gdn
       ON  loc.location = gdn.location
    WHERE  COALESCE(lspccb.alnvalue,'N') = 'N'
    AND NOT EXISTS
           (
             SELECT 'x'
             FROM   maximo.a_locations aloc2
             WHERE  aloc2.siteid = aloc.siteid
             AND    aloc2.location = aloc.location
             AND    aloc2.apa_marketactfinish = aloc.apa_marketactfinish
             AND    aloc2.eaudittimestamp > aloc.eaudittimestamp
           )
    AND NOT EXISTS
           (
             SELECT 'x'
             FROM   maximo.a_locations aloc2
             WHERE  aloc2.siteid = aloc.siteid
             AND    aloc2.location = aloc.location
             AND    COALESCE(aloc2.apa_marketactfinish,aloc2.eaudittimestamp) > COALESCE(aloc.apa_marketactfinish,aloc.eaudittimestamp)
           );
    lv_record_count := SQL%ROWCOUNT;
    pr_update_recon_hist_step(
      prv_step => '28',
      prv_record_count => lv_record_count);
    ---------------------------------------------------------

  END pr_reconcile_mirns;

  ------------------------------------------------------------------------------------------------------------------
  -- Reconcile meters
  ------------------------------------------------------------------------------------------------------------------
  PROCEDURE pr_reconcile_meters IS
    lv_record_count INTEGER;
  BEGIN
    ---------------------------------------------------------
    -- No location defined on Maximo meter asset
    ---------------------------------------------------------
    pr_create_recon_hist_step(
      prv_step               => '30',
      prv_step_description   => 'Location missing on Maximo meter',
      prv_reconcile_issue_id => '51');
    INSERT INTO ccbdatarec_issues (
           reconcile_issue_id,
           maximo_assetnum,
           maximo_serialnum,
           maximo_assettag
    )
    SELECT /*+ USE_HASH(ass, str) */
           '51',
           ass.assetnum,
           ass.serialnum,
           ass.assettag
    FROM   maximo.asset ass
    JOIN   maximo.classstructure str
       ON  str.classstructureid = ass.classstructureid
       AND str.classificationid = 'MT'
    WHERE  ass.location IS NULL
-- Need to confirm list of meter statuses for which this validation check is appropriate
    AND    ass.status IN ('ACTIVE','COMMISSIONED','TURNED OFF','REGISTERED','PLUGGED');
    lv_record_count := SQL%ROWCOUNT;
    pr_update_recon_hist_step(
      prv_step => '30',
      prv_record_count => lv_record_count);
    ---------------------------------------------------------

    ---------------------------------------------------------
    -- No Serial Number and/or Asset Tag defined on Maximo meter asset
    ---------------------------------------------------------
    pr_create_recon_hist_step(
      prv_step               => '31',
      prv_step_description   => 'Serial Number and/or Asset Tag missing on Maximo meter',
      prv_reconcile_issue_id => '52');
    INSERT INTO ccbdatarec_issues (
           reconcile_issue_id,
           maximo_location,
           maximo_location_status,
           maximo_location_status_date,
           maximo_market_status,
           maximo_mirn,
           maximo_gdn,
           maximo_assetnum,
           maximo_serialnum,
           maximo_assettag
    )
    SELECT /*+ USE_HASH(ass, str, loc, lspmirn, gdn, mcd, aloc aloc2) */
           '52',
           loc.location,
           loc.status,
           loc.statusdate,
           COALESCE(aloc.apa_gspstatus,loc.apa_gspstatus),
           lspmirn.alnvalue AS maximo_mirn,
           gdn.gdn_division,
           ass.assetnum,
           ass.serialnum,
           ass.assettag
    FROM   maximo.asset ass
    JOIN   maximo.classstructure str
       ON  str.classstructureid = ass.classstructureid
       AND str.classificationid = 'MT'
    LEFT OUTER JOIN maximo.locations loc
       ON  loc.siteid = ass.siteid
       AND loc.location = ass.location
    LEFT OUTER JOIN maximo.a_locations aloc
       ON  aloc.siteid = loc.siteid
       AND aloc.location = loc.location
    LEFT OUTER JOIN maximo.locationspec lspmirn
       ON  loc.siteid = lspmirn.siteid
       AND loc.location = lspmirn.location
       AND lspmirn.assetattrid = 'MIRN'
    LEFT OUTER JOIN maximo_location_gdns gdn
       ON  lspmirn.location = gdn.location
-- Need to confirm list of meter statuses for which this validation check is appropriate
    WHERE  ass.status IN ('ACTIVE','COMMISSIONED','TURNED OFF','REGISTERED','PLUGGED')
    AND   (ass.serialnum IS NULL
        OR ass.assettag IS NULL)
    AND NOT EXISTS
           (
             SELECT 'x'
             FROM   maximo.a_locations aloc2
             WHERE  aloc2.siteid = aloc.siteid
             AND    aloc2.location = aloc.location
             AND    aloc2.apa_marketactfinish = aloc.apa_marketactfinish
             AND    aloc2.eaudittimestamp > aloc.eaudittimestamp
           )
    AND NOT EXISTS
           (
             SELECT 'x'
             FROM   maximo.a_locations aloc2
             WHERE  aloc2.siteid = aloc.siteid
             AND    aloc2.location = aloc.location
             AND    COALESCE(aloc2.apa_marketactfinish,aloc2.eaudittimestamp) > COALESCE(aloc.apa_marketactfinish,aloc.eaudittimestamp)
           );

    lv_record_count := SQL%ROWCOUNT;
    pr_update_recon_hist_step(
      prv_step => '31',
      prv_record_count => lv_record_count);
    ---------------------------------------------------------

    ---------------------------------------------------------
    -- Maximo Meter not found in CCB
    ---------------------------------------------------------
    pr_create_recon_hist_step(
      prv_step               => '32',
      prv_step_description   => 'Maximo Meter missing in CCB',
      prv_reconcile_issue_id => '53');
    lv_record_count := NULL;
    INSERT INTO ccbdatarec_issues (
           reconcile_issue_id,
           maximo_location,
           maximo_location_status,
           maximo_location_status_date,
           maximo_market_status,
           maximo_mirn,
           maximo_gdn,
           maximo_assetnum,
           maximo_serialnum,
           maximo_assettag
    )
    SELECT /*+ USE_HASH(ass, str, loc, lspccb, lspmirn, gdn, mcd, aloc aloc2) */
           '53',
           loc.location,
           loc.status,
           loc.statusdate,
           COALESCE(aloc.apa_gspstatus,loc.apa_gspstatus),
           lspmirn.alnvalue AS mirn,
           gdn.gdn_division,
           ass.assetnum,
           ass.serialnum,
           ass.assettag
    FROM   maximo.asset ass
    JOIN   maximo.classstructure str
       ON  str.classstructureid = ass.classstructureid
       AND str.classificationid = 'MT'
    JOIN   maximo.locations loc
       ON  loc.siteid = ass.siteid
       AND loc.location = ass.location
    LEFT OUTER JOIN maximo.a_locations aloc
       ON  aloc.siteid = loc.siteid
       AND aloc.location = loc.location
    JOIN   maximo.locationspec lspccb
       ON  loc.siteid = lspccb.siteid
       AND loc.location = lspccb.location
       AND lspccb.assetattrid = 'SEND_TO_CCB'
       AND lspccb.alnvalue = 'Y'
    JOIN   maximo.locationspec lspmirn
       ON  loc.siteid = lspmirn.siteid
       AND loc.location = lspmirn.location
       AND lspmirn.assetattrid = 'MIRN'
       AND lspmirn.alnvalue IS NOT NULL
    LEFT OUTER JOIN maximo_location_gdns gdn
       ON  loc.location = gdn.location
    LEFT OUTER JOIN ccb_meter_config_details mcd
       ON  mcd.badge_nbr = ass.assettag
    WHERE  mcd.badge_nbr IS NULL
    AND NOT EXISTS
           (
             SELECT 'x'
             FROM   maximo.a_locations aloc2
             WHERE  aloc2.siteid = aloc.siteid
             AND    aloc2.location = aloc.location
             AND    aloc2.apa_marketactfinish = aloc.apa_marketactfinish
             AND    aloc2.eaudittimestamp > aloc.eaudittimestamp
           )
    AND NOT EXISTS
           (
             SELECT 'x'
             FROM   maximo.a_locations aloc2
             WHERE  aloc2.siteid = aloc.siteid
             AND    aloc2.location = aloc.location
             AND    COALESCE(aloc2.apa_marketactfinish,aloc2.eaudittimestamp) > COALESCE(aloc.apa_marketactfinish,aloc.eaudittimestamp)
           );

    lv_record_count := SQL%ROWCOUNT;
    pr_update_recon_hist_step(
      prv_step => '32',
      prv_record_count => lv_record_count);
    ---------------------------------------------------------

    ---------------------------------------------------------
    -- CCB Meter not found in Maximo
    ---------------------------------------------------------
    pr_create_recon_hist_step(
      prv_step               => '33',
      prv_step_description   => 'CCB Meter missing in Maximo',
      prv_reconcile_issue_id => '54');
    lv_record_count := NULL;
    INSERT INTO ccbdatarec_issues (
           reconcile_issue_id,
           ccb_prem_id,
           ccb_sp_id,
           ccb_mirn,
           ccb_mirn_status,
           ccb_mirn_status_eff_dttm,
           ccb_mtr_id,
           ccb_serial_nbr,
           ccb_badge_nbr,
           ccb_cis_division
    )
    SELECT /*+ USE_HASH(ass, str, mcd) */
           '54',
           prem_id,
           sp_id,
           mirn,
           mirn_status,
           mirn_status_eff_dttm,
           mtr_id,
           serial_nbr,
           badge_nbr,
           cis_division
    FROM   ccb_meter_config_details mcd
    WHERE NOT EXISTS
          (
             SELECT 'x'
             FROM   maximo.asset ass
             JOIN maximo.classstructure str
                ON  str.classstructureid = ass.classstructureid
                AND str.classificationid = 'MT'
             WHERE  mcd.badge_nbr = ass.assettag
          );
    lv_record_count := SQL%ROWCOUNT;
    pr_update_recon_hist_step(
      prv_step => '33',
      prv_record_count => lv_record_count);
    ---------------------------------------------------------

    ---------------------------------------------------------
    -- MIRN/Meter mismatch
    ---------------------------------------------------------
    pr_create_recon_hist_step(
      prv_step               => '34',
      prv_step_description   => 'MIRN/Meter mismatch',
      prv_reconcile_issue_id => '55');
    lv_record_count := NULL;
    INSERT INTO ccbdatarec_issues (
           reconcile_issue_id,
           maximo_location,
           maximo_location_status,
           maximo_location_status_date,
           maximo_market_status,
           maximo_mirn,
           maximo_gdn,
           maximo_assetnum,
           maximo_serialnum,
           maximo_assettag,
           ccb_prem_id,
           ccb_sp_id,
           ccb_mirn,
           ccb_mirn_status,
           ccb_mirn_status_eff_dttm,
           ccb_mtr_id,
           ccb_serial_nbr,
           ccb_badge_nbr,
           ccb_cis_division
    )
    SELECT /*+ USE_HASH(ass, str, loc, lspmirn, gdn, mcd, mcd2, aloc aloc2) */
           '55',
           loc.location,
           loc.status,
           loc.statusdate,
           COALESCE(aloc.apa_gspstatus,loc.apa_gspstatus),
           lspmirn.alnvalue,
           gdn.gdn_division,
           ass.assetnum,
           ass.serialnum,
           ass.assettag,
           mcd.prem_id,
           mcd.sp_id,
           mcd.mirn,
           mcd.mirn_status,
           mcd.mirn_status_eff_dttm,
           mcd.mtr_id,
           mcd.serial_nbr,
           mcd.badge_nbr,
           mcd.cis_division
    FROM   maximo.asset ass
    JOIN   maximo.classstructure str
       ON  str.classstructureid = ass.classstructureid
       AND str.classificationid = 'MT'
    JOIN   maximo.locations loc
       ON  loc.siteid = ass.siteid
       AND loc.location = ass.location
    LEFT OUTER JOIN maximo.a_locations aloc
       ON  aloc.siteid = loc.siteid
       AND aloc.location = loc.location
    JOIN   maximo.locationspec lspmirn
       ON  loc.siteid = lspmirn.siteid
       AND loc.location = lspmirn.location
       AND lspmirn.assetattrid = 'MIRN'
    JOIN   ccb_meter_config_details mcd
       ON  mcd.mirn = lspmirn.alnvalue
    LEFT OUTER JOIN maximo_location_gdns gdn
       ON  loc.location = gdn.location
    WHERE NOT EXISTS
          (
             SELECT 'x'
             FROM   ccb_meter_config_details mcd2
             WHERE  mcd2.mirn = mcd.mirn
             AND    mcd2.eff_dttm > mcd.eff_dttm
          )
    AND   mcd.badge_nbr != ass.assettag
    AND NOT EXISTS
           (
             SELECT 'x'
             FROM   maximo.a_locations aloc2
             WHERE  aloc2.siteid = aloc.siteid
             AND    aloc2.location = aloc.location
             AND    aloc2.apa_marketactfinish = aloc.apa_marketactfinish
             AND    aloc2.eaudittimestamp > aloc.eaudittimestamp
           )
    AND NOT EXISTS
           (
             SELECT 'x'
             FROM   maximo.a_locations aloc2
             WHERE  aloc2.siteid = aloc.siteid
             AND    aloc2.location = aloc.location
             AND    COALESCE(aloc2.apa_marketactfinish,aloc2.eaudittimestamp) > COALESCE(aloc.apa_marketactfinish,aloc.eaudittimestamp)
           );
        
    lv_record_count := SQL%ROWCOUNT;
    EXECUTE IMMEDIATE 'ANALYZE TABLE ccbdatarec_issues ESTIMATE STATISTICS';
    pr_update_recon_hist_step(
      prv_step => '34',
      prv_record_count => lv_record_count);
    ---------------------------------------------------------

  END pr_reconcile_meters;

  ------------------------------------------------------------------------------------------------------------------
  -- Create a summary of records processed and issues found
  ------------------------------------------------------------------------------------------------------------------
  PROCEDURE pr_reconciliation_summary IS
    lv_record_count INTEGER;
  BEGIN
    ---------------------------------------------------------
    -- Count of Maximo contestable MIRNS
    ---------------------------------------------------------
    pr_create_recon_hist_step(
      prv_step               => '40',
      prv_step_description   => 'Reconciliation Summary: Count of Maximo MIRNS by GDN');
    lv_record_count := NULL;
    INSERT INTO ccbdatarec_summary (
      recon_id,
      summary_issue_id,
      summary_issue_description,
      gdn_division, 
      record_count
    )
    SELECT /*+ USE_HASH(loc, lspccb, lspmirn, gdn) */
           pkv_recon_id,
           '1',
           'Count of Maximo MIRNS',
           gdn.gdn_division,
           COUNT(*)
    FROM   maximo.locations loc
    JOIN   maximo.locationspec lspccb
       ON  loc.siteid = lspccb.siteid
       AND loc.location = lspccb.location
       AND lspccb.assetattrid = 'SEND_TO_CCB'
       AND lspccb.alnvalue = 'Y'
    JOIN maximo.locationspec lspmirn
       ON  loc.siteid = lspmirn.siteid
       AND loc.location = lspmirn.location
       AND lspmirn.assetattrid = 'MIRN'
       AND lspmirn.alnvalue IS NOT NULL
    LEFT OUTER JOIN maximo_location_gdns gdn
       ON  loc.location = gdn.location
    GROUP BY gdn.gdn_division;   
    lv_record_count := SQL%ROWCOUNT;
          
    pr_update_recon_hist_step(
      prv_step => '40',
      prv_record_count => lv_record_count);
    ---------------------------------------------------------

    ---------------------------------------------------------
    -- Count of CCB MIRNS
    ---------------------------------------------------------
    pr_create_recon_hist_step(
      prv_step               => '41',
      prv_step_description   => 'Reconciliation Summary: Count of CCB MIRNs by Division');
    lv_record_count := NULL;
    INSERT INTO ccbdatarec_summary (
      recon_id,
      summary_issue_id,
      summary_issue_description,
      gdn_division, 
      record_count
    )
    SELECT pkv_recon_id,
           '2',
           'Count of CCB MIRNS',
           cmd.cis_division,
           COUNT(*)
    FROM   ccb_mirn_details cmd
    GROUP BY cmd.cis_division;   
    lv_record_count := SQL%ROWCOUNT;
          
    pr_update_recon_hist_step(
      prv_step => '41',
      prv_record_count => lv_record_count);
    ---------------------------------------------------------

    ---------------------------------------------------------
    -- Count of Maximo meters
    ---------------------------------------------------------
    pr_create_recon_hist_step(
      prv_step               => '42',
      prv_step_description   => 'Reconciliation Summary: Count of Maximo meters by GDN');
    lv_record_count := NULL;
    INSERT INTO ccbdatarec_summary (
      recon_id,
      summary_issue_id,
      summary_issue_description,
      gdn_division, 
      record_count
    )
    SELECT /*+ USE_HASH(ass, str, loc, lspccb, lspmirn, gdn) */
           pkv_recon_id,
           '3',
           'Count of Maximo Meters',
           gdn.gdn_division,
           COUNT(*)
    FROM   maximo.asset ass
    JOIN   maximo.classstructure str
       ON  str.classstructureid = ass.classstructureid
       AND str.classificationid = 'MT'
    JOIN   maximo.locations loc
       ON  loc.siteid = ass.siteid
       AND loc.location = ass.location
    JOIN   maximo.locationspec lspccb
       ON  loc.siteid = lspccb.siteid
       AND loc.location = lspccb.location
       AND lspccb.assetattrid = 'SEND_TO_CCB'
       AND lspccb.alnvalue = 'Y'
    JOIN   maximo.locationspec lspmirn
       ON  loc.siteid = lspmirn.siteid
       AND loc.location = lspmirn.location
       AND lspmirn.assetattrid = 'MIRN'
       AND lspmirn.alnvalue IS NOT NULL
    LEFT OUTER JOIN maximo_location_gdns gdn
       ON  loc.location = gdn.location
    GROUP BY gdn.gdn_division;   
    lv_record_count := SQL%ROWCOUNT;
          
    pr_update_recon_hist_step(
      prv_step => '42',
      prv_record_count => lv_record_count);
    ---------------------------------------------------------

     ---------------------------------------------------------
    -- Count of CCB meters
    ---------------------------------------------------------
    pr_create_recon_hist_step(
      prv_step               => '43',
      prv_step_description   => 'Reconciliation Summary: Count of CCB meters by Division');
    lv_record_count := NULL;
    INSERT INTO ccbdatarec_summary (
      recon_id,
      summary_issue_id,
      summary_issue_description,
      gdn_division, 
      record_count
    )
    SELECT pkv_recon_id,
           '4',
           'Count of CCB Meters',
           mcd.cis_division,
           COUNT(DISTINCT mtr_id)
    FROM   ccb_meter_config_details mcd
    GROUP BY mcd.cis_division;   
    lv_record_count := SQL%ROWCOUNT;
          
    pr_update_recon_hist_step(
      prv_step => '43',
      prv_record_count => lv_record_count);
    ---------------------------------------------------------

     ---------------------------------------------------------
    -- Issue count by GDN/Division
    ---------------------------------------------------------
    pr_create_recon_hist_step(
      prv_step               => '44',
      prv_step_description   => 'Reconciliation Summary: Count of reconciliation issues by GDN/Division');
    lv_record_count := NULL;
    INSERT INTO ccbdatarec_summary (
      recon_id,
      summary_issue_id,
      summary_issue_description,
      gdn_division, 
      record_count
    )
    SELECT pkv_recon_id,
           it.reconcile_issue_id,
           it.reconcile_issue,
           COALESCE(iss.maximo_gdn, iss.ccb_cis_division),
           COUNT(*)
    FROM   ccbdatarec_issue_types it
    LEFT OUTER JOIN ccbdatarec_issues iss
       ON  it.reconcile_issue_id = iss.reconcile_issue_id
    GROUP BY it.reconcile_issue_id,
             it.reconcile_issue,
             COALESCE(iss.maximo_gdn, iss.ccb_cis_division);   
    lv_record_count := SQL%ROWCOUNT;
          
    pr_update_recon_hist_step(
      prv_step => '44',
      prv_record_count => lv_record_count);
    ---------------------------------------------------------

 END pr_reconciliation_summary;


  ------------------------------------------------------------------------------------------------------------------
  -- Main controlling procedure for ccbdatarec process
  ------------------------------------------------------------------------------------------------------------------
  PROCEDURE main IS
  BEGIN
    pkv_recon_id := fn_create_reconciliation_hist;
    pr_drop_previous_run_data;
    pr_populate_location_gdns;
    pr_copy_ccb_mirn_data;
    pr_copy_ccb_meter_config_data;
    pr_reconcile_mirns;
    pr_reconcile_meters;
    pr_reconciliation_summary;
    pr_update_reconciliation_hist(
      prv_status => 'COMPLETE',
      prv_end_date => SYSDATE);
  END main;

END maximo_ccb_datarec;
/

show errors
/