DROP DATABASE LINK ccbdatarec
/

DROP SEQUENCE ccbdatarec_seq
/

DROP TABLE ccbdatarec_history
/

DROP TABLE ccbdatarec_history_steps
/

DROP TABLE gdn_division_xref
/

DROP TABLE maximo_location_gdns
/

DROP TABLE ccb_meter_config_details
/

DROP TABLE ccb_mirn_details
/

DROP TABLE ccbdatarec_issue_types
/

DROP TABLE ccbdatarec_issues
/

DROP TABLE ccbdatarec_summary
/

DROP PACKAGE maximo_ccb_datarec
/