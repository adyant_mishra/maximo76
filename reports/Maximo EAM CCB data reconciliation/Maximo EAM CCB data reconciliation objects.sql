CONNECT datarec/&MXODATARECPASSWORD

CREATE DATABASE LINK ccbdatarec
CONNECT TO datarec IDENTIFIED BY &SARDATARECPASSWORD
USING '&MBSRP'
/

CREATE SEQUENCE ccbdatarec_seq CACHE 2 ORDER 
/

CREATE TABLE ccbdatarec_history (
  recon_id     INTEGER NOT NULL,
  start_date   DATE NOT NULL,
  end_date     DATE,
  recon_status VARCHAR2(100),
  CONSTRAINT rec_hist_pk PRIMARY KEY (recon_id)
)
/


CREATE TABLE ccbdatarec_history_steps (
  recon_id           INTEGER NOT NULL,
  recon_step_id      INTEGER NOT NULL,
  step_description   VARCHAR2(100) NOT NULL,
  start_date         DATE NOT NULL,
  end_date           DATE,
  reconcile_issue_id INTEGER,
  record_count       INTEGER,
  CONSTRAINT rec_hist_step_pk PRIMARY KEY (recon_id, recon_step_id)
)
/

CREATE TABLE gdn_division_xref (
  gdn_location     VARCHAR(48) NOT NULL,
  gdn_division     VARCHAR2(10)
)
/

DELETE FROM gdn_division_xref
/

INSERT INTO gdn_division_xref (
  gdn_location,
  gdn_division
)
WITH gdn AS
    (
      SELECT loc.location AS gdn_location,
             CASE 
               WHEN loc.description LIKE '%Envestra Queensland' THEN 'QLDE'
               WHEN loc.description LIKE '%Australian%Gas%Networks%QLD%' THEN 'QLDE'
               WHEN loc.description LIKE '%Envestra Victoria' THEN 'VIC'
               WHEN loc.description LIKE '%Australian%Gas%Networks%VIC%' THEN 'VIC'
               WHEN loc.description LIKE '%Envestra South Australia' THEN 'SA'
               WHEN loc.description LIKE '%Australian%Gas%Networks%SA%' THEN 'SA'
               WHEN loc.description LIKE '%GDI Queensland' THEN 'QLDA'
             END AS gdn_division
      FROM   maximo.locations loc
      WHERE  (2,loc.location) IN 
             (
               SELECT level, 
                      loch.location 
               FROM   maximo.lochierarchy loch 
               WHERE  loch.systemid = 'NETWORKS' 
               CONNECT BY PRIOR loch.location = loch.parent 
                   AND    PRIOR loch.systemid = loch.systemid 
                   AND    LEVEL < 3 
                   START WITH loch.parent IS NULL
             )
    )
SELECT gdn_location,
       gdn_division
FROM   gdn
/

CREATE TABLE meter_pressure_xref (
  gdn_division  VARCHAR2(10) NOT NULL,
  kpa           VARCHAR2(10) NOT NULL,
  install_const VARCHAR2(10) NOT NULL,
  pcf_zone      VARCHAR2(10)
)
/

DELETE FROM meter_pressure_xref
/

INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','0','1.0000','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','1.2','1.0123','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','2','1.0197','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','2.7','1.0271','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','3','1.0296','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','4','1.0395','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','5','1.0493','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','6','1.0593','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','6.5','1.0642','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','7','1.0691','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','10','1.0987','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','13','1.1283','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','14','1.1385','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','15','1.1480','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','18','1.1776','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','30','1.2961','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','35','1.3464','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','40','1.3948','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','50','1.4950','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','70','1.6933','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','80','1.7895','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','100','1.9910','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','101','1.9968','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','111','2.0955','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','121','2.1942','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','131','2.2929','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','140','2.3885','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','141','2.3916','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','152','2.5001','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','162','2.5988','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','172','2.6975','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','182','2.7962','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','192','2.8949','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','200','2.9861','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','203','3.0035','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','243','3.3982','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','253','3.4969','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','283','3.7930','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','293','3.8917','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','304','4.0002','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','355','4.5036','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','400','4.9477','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','555','6.4774','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','0','1.0000','PCF-OAKY');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','1.2','0.9659','PCF-OAKY');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','2.7','0.9808','PCF-OAKY');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','3','0.9832','PCF-OAKY');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','3.5','0.9882','PCF-OAKY');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','6','1.0129','PCF-OAKY');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','7','1.0228','PCF-OAKY');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','10','1.0524','PCF-OAKY');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','14','1.0921','PCF-OAKY');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','30','1.2498','PCF-OAKY');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','35','1.2999','PCF-OAKY');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','50','1.4485','PCF-OAKY');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','70','1.6468','PCF-OAKY');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','100','1.9445','PCF-OAKY');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','120','2.1380','PCF-OAKY');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','130','2.2367','PCF-OAKY');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','140','2.3420','PCF-OAKY');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','200','2.9396','PCF-OAKY');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','210','3.0262','PCF-OAKY');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','215','3.0756','PCF-OAKY');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','0','1.0000','PCF-TOOW');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','1.2','0.9476','PCF-TOOW');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','2.7','0.9625','PCF-TOOW');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','3','0.9649','PCF-TOOW');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','3.5','0.9699','PCF-TOOW');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','6','0.9946','PCF-TOOW');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','7','1.0045','PCF-TOOW');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','9','1.0242','PCF-TOOW');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','10','1.0341','PCF-TOOW');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','14','1.0737','PCF-TOOW');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','15','1.0834','PCF-TOOW');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','17','1.1032','PCF-TOOW');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','27','1.2019','PCF-TOOW');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','35','1.2816','PCF-TOOW');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','37','1.3006','PCF-TOOW');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','47','1.3992','PCF-TOOW');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','50','1.4302','PCF-TOOW');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','57','1.4979','PCF-TOOW');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','67','1.5966','PCF-TOOW');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','70','1.6284','PCF-TOOW');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','77','1.6953','PCF-TOOW');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','100','1.9261','PCF-TOOW');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','108','2.0013','PCF-TOOW');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','118','2.1000','PCF-TOOW');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','120','2.1197','PCF-TOOW');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','138','2.2974','PCF-TOOW');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','140','2.3236','PCF-TOOW');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','150','2.4158','PCF-TOOW');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','160','2.5145','PCF-TOOW');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','200','2.9211','PCF-TOOW');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','210','3.0079','PCF-TOOW');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','1120','11.9889','PCF-TOOW');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','2.75','1.0271','PCF-BRIS');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','2.75','0.9808','PCF-OAKY');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDA','2.75','0.9625','PCF-TOOW');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDE','1.1','1.0114','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDE','1.2','1.0121','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDE','2.7','1.0274','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDE','3','1.0299','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDE','6','1.0595','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDE','6.2','1.0618','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDE','7','1.0694','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDE','15','1.1483','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDE','35','1.3457','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDE','50','1.4937','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDE','70','1.6911','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDE','90','1.8885','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDE','100','1.9872','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDE','140','2.3820','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDE','200','2.9741','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDE','350','4.4545','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDE','600','6.9218','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDE','1.125','1.0114','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDE','2.75','1.0274','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('QLDE','6.232','1.0618','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','1','1.0099','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','1.1','1.0109','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','1.4','1.0139','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','1.5','1.0148','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','2','1.0198','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','2.5','1.0247','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','2.7','1.0272','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','3','1.0297','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','3.5','1.0347','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','4','1.0396','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','5','1.0495','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','7','1.0693','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','8','1.0792','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','10','1.0990','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','15','1.1485','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','18','1.1782','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','20','1.1980','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','25','1.2475','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','30','1.2970','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','35','1.3466','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','37','1.3664','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','40','1.3961','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','45','1.4456','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','50','1.4952','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','60','1.5943','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','70','1.6936','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','80','1.7929','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','90','1.8922','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','100','1.9916','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','105','2.0413','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','110','2.0909','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','140','2.3895','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','150','2.4891','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','170','2.6884','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','200','2.9877','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','220','3.1874','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','240','3.3874','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','280','3.7879','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','350','4.4903','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('SA','2.75','1.0272','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('VIC','1.1','1.0109','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('VIC','1.5','1.0148','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('VIC','2','1.0198','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('VIC','2.5','1.0247','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('VIC','2.7','1.0272','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('VIC','3','1.0297','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('VIC','4','1.0396','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('VIC','5','1.0495','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('VIC','6','1.0593','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('VIC','7','1.0692','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('VIC','7.5','1.0742','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('VIC','10','1.0989','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('VIC','12','1.1188','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('VIC','15','1.1484','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('VIC','18','1.1781','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('VIC','20','1.1979','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('VIC','30','1.2970','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('VIC','35','1.3465','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('VIC','40','1.3960','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('VIC','50','1.4951','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('VIC','60','1.5942','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('VIC','70','1.6934','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('VIC','80','1.7927','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('VIC','100','1.9913','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('VIC','120','2.1901','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('VIC','140','2.3891','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('VIC','145','2.4389','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('VIC','150','2.4887','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('VIC','170','2.6879','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('VIC','200','2.9872','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('VIC','210','3.0870','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('VIC','250','3.4866','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('VIC','300','3.9873','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('VIC','350','4.4890','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('VIC','390','4.8915','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('VIC','400','4.9922','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('VIC','450','5.4961','');
INSERT INTO meter_pressure_xref(gdn_division, kpa, install_const, pcf_zone) VALUES ('VIC','2.75','1.0272','');

CREATE TABLE maximo_location_gdns (
  location         VARCHAR(48) NOT NULL,
  gdn_location     VARCHAR(48) NOT NULL,
  gdn_division     VARCHAR2(10) NOT NULL
)
/

CREATE TABLE ccb_meter_config_details (
  mtr_id                VARCHAR2(10),
  badge_nbr             VARCHAR2(30),
  serial_nbr            VARCHAR2(16),
  mtr_config_id         VARCHAR2(10) NOT NULL,
  eff_dttm              DATE NOT NULL,
  install_date          DATE NOT NULL,
  install_const         VARCHAR2(10) NOT NULL,
  install_kpa           VARCHAR2(10),
  pcf_zone              VARCHAR2(10),
  sp_mtr_hist_id        VARCHAR2(10) NOT NULL,
  sp_id                 VARCHAR2(10) NOT NULL,
  removal_dttm          DATE,
  mirn                  VARCHAR2(16) NOT NULL,
  mirn_status           VARCHAR2(16) NOT NULL,
  mirn_status_eff_dttm  DATE NOT NULL,
  prem_id               VARCHAR2(10) NOT NULL,
  prem_address          VARCHAR2(1000) NOT NULL,
  cis_division          VARCHAR2(5) NOT NULL
)
STORAGE
(
  INITIAL 1M
  NEXT    1M
)
/

ALTER TABLE ccb_meter_config_details MODIFY (
  install_kpa NULL
)
/

ALTER TABLE ccb_meter_config_details ADD (
  pcf_zone VARCHAR2(10)
)
/

CREATE TABLE ccb_mirn_details (
  sp_id                 VARCHAR2(10) NOT NULL,
  mirn                  VARCHAR2(16) NOT NULL,
  mirn_status           VARCHAR2(16) NOT NULL,
  mirn_status_eff_dttm  DATE NOT NULL,
  prem_id               VARCHAR2(10) NOT NULL,
  prem_address          VARCHAR2(1000) NOT NULL,
  prem_cust_class       VARCHAR2(16),
  cis_division          VARCHAR2(5) NOT NULL
)
STORAGE
(
  INITIAL 1M
  NEXT    1M
)
/

CREATE TABLE ccbdatarec_issue_types (
  reconcile_issue_id     INTEGER NOT NULL,
  reconcile_issue        VARCHAR2(100) NOT NULL,
  issue_group_id         INTEGER NOT NULL,
  issue_group            VARCHAR2(10) NOT NULL,
  reportable             VARCHAR2(1) NOT NULL,
  detail_report          VARCHAR2(100),
  maximo_application     VARCHAR2(100)  
)
/
INSERT INTO ccbdatarec_issue_types(reconcile_issue_id, reconcile_issue, issue_group_id, issue_group, reportable, detail_report, maximo_application)
VALUES ('10','MIRN id missing on Maximo location','1','MIRN','N',NULL,NULL)
/
INSERT INTO ccbdatarec_issue_types(reconcile_issue_id, reconcile_issue, issue_group_id, issue_group, reportable, detail_report, maximo_application)
VALUES ('11','No GDN associated with Maximo location','1','MIRN','N',NULL,NULL)
/
INSERT INTO ccbdatarec_issue_types(reconcile_issue_id, reconcile_issue, issue_group_id, issue_group, reportable, detail_report, maximo_application)
VALUES ('12','Maximo MIRN missing in CCB','1','MIRN','Y','APA_REC007_MaximoMIRNNotInCCB.rptdesign','PLUSGLOC')
/
INSERT INTO ccbdatarec_issue_types(reconcile_issue_id, reconcile_issue, issue_group_id, issue_group, reportable, detail_report, maximo_application)
VALUES ('13','CCB MIRN missing in Maximo','1','MIRN','Y','APA_REC008_CCBMIRNNotInMaximo.rptdesign','PLUSGLOC')
/
INSERT INTO ccbdatarec_issue_types(reconcile_issue_id, reconcile_issue, issue_group_id, issue_group, reportable, detail_report, maximo_application)
VALUES ('14','MIRN/Address mismatch','1','MIRN','Y','APA_REC010_MIRNAddressMismatch.rptdesign','PLUSGLOC')
/
INSERT INTO ccbdatarec_issue_types(reconcile_issue_id, reconcile_issue, issue_group_id, issue_group, reportable, detail_report, maximo_application)
VALUES ('15','MIRN/Status mismatch','1','MIRN','Y','APA_REC003_MIRNStatusMismatch.rptdesign','PLUSGLOC')
/
INSERT INTO ccbdatarec_issue_types(reconcile_issue_id, reconcile_issue, issue_group_id, issue_group, reportable, detail_report, maximo_application)
VALUES ('16','MIRN/Pressure mismatch','1','MIRN','Y','APA_REC004_MIRNPressureMismatch.rptdesign','PLUSGLOC')
/
INSERT INTO ccbdatarec_issue_types(reconcile_issue_id, reconcile_issue, issue_group_id, issue_group, reportable, detail_report, maximo_application)
VALUES ('17','MIRN/Customer/Consumer Class mismatch','1','MIRN','Y','APA_REC005_MIRNCustConsumerClassMismatch.rptdesign','PLUSGLOC')
/
INSERT INTO ccbdatarec_issue_types(reconcile_issue_id, reconcile_issue, issue_group_id, issue_group, reportable, detail_report, maximo_application)
VALUES ('18','CCB MIRN Exists in Maximo with Send to CCB = N','1','MIRN','Y','APA_REC011_CCBMIRNSendToCCBIsN.rptdesign','PLUSGLOC')
/
INSERT INTO ccbdatarec_issue_types(reconcile_issue_id, reconcile_issue, issue_group_id, issue_group, reportable, detail_report, maximo_application)
VALUES ('51','Location missing on Maximo meter asset','2','METER','N',NULL,NULL)
/
INSERT INTO ccbdatarec_issue_types(reconcile_issue_id, reconcile_issue, issue_group_id, issue_group, reportable, detail_report, maximo_application)
VALUES ('52','Serial Number and/or Asset Tag missing on Maximo meter asset','2','METER','N',NULL,NULL)
/
INSERT INTO ccbdatarec_issue_types(reconcile_issue_id, reconcile_issue, issue_group_id, issue_group, reportable, detail_report, maximo_application)
VALUES ('53','Maximo Meter missing in CCB','1','METER','Y','APA_REC001_MaximoMeterNotInCCB.rptdesign','PLUSGASS')
/
INSERT INTO ccbdatarec_issue_types(reconcile_issue_id, reconcile_issue, issue_group_id, issue_group, reportable, detail_report, maximo_application)
VALUES ('54','CCB Meter missing in Maximo','1','METER','Y','APA_REC006_CCBMeterNotInMaximo.rptdesign','PLUSGASS')
/
INSERT INTO ccbdatarec_issue_types(reconcile_issue_id, reconcile_issue, issue_group_id, issue_group, reportable, detail_report, maximo_application)
VALUES ('55','MIRN/Meter Mismatch','1','METER','Y','APA_REC002_MIRNMeterMismatch.rptdesign','PLUSGASS')
/

CREATE TABLE ccbdatarec_issues (
  reconcile_issue_id          INTEGER NOT NULL,
  maximo_location             VARCHAR2(12),
  maximo_location_status      VARCHAR2(20),  
  maximo_location_status_date DATE,  
  maximo_market_status        VARCHAR2(20),  
  maximo_location_address     VARCHAR2(1000),
  maximo_mirn                 VARCHAR2(10),
  maximo_consumer_class       VARCHAR2(10),
  maximo_assetnum             VARCHAR2(12),
  maximo_serialnum            VARCHAR2(64),
  maximo_assettag             VARCHAR2(64),
  maximo_install_const        VARCHAR2(10),
  maximo_install_kpa          VARCHAR2(10),
  maximo_gdn                  VARCHAR2(64),
  ccb_prem_id                 VARCHAR2(10),
  ccb_prem_cust_class         VARCHAR2(16),
  ccb_prem_address            VARCHAR2(1000),
  ccb_sp_id                   VARCHAR2(10),
  ccb_mirn                    VARCHAR2(10),
  ccb_mirn_status             VARCHAR2(16),
  ccb_mirn_status_eff_dttm    DATE,
  ccb_mtr_id                  VARCHAR2(10),
  ccb_serial_nbr              VARCHAR2(16),
  ccb_badge_nbr               VARCHAR2(30),
  ccb_install_const           VARCHAR2(10),
  ccb_install_kpa             VARCHAR2(10),
  ccb_cis_division            VARCHAR2(5)
)
STORAGE
(
  INITIAL 1M
  NEXT    1M
)
/

CREATE TABLE ccbdatarec_summary (
  recon_id                  INTEGER NOT NULL,
  summary_issue_id          INTEGER NOT NULL,
  summary_issue_description VARCHAR2(100) NOT NULL,
  gdn_division              VARCHAR2(10), 
  record_count              INTEGER
)
/

GRANT SELECT ON ccbdatarec_history TO maximo, readonly
/
GRANT SELECT ON ccbdatarec_history_steps TO maximo, readonly
/
GRANT SELECT ON gdn_division_xref TO maximo, readonly
/
GRANT SELECT ON meter_pressure_xref TO maximo, readonly
/
GRANT SELECT ON maximo_location_gdns TO maximo, readonly
/
GRANT SELECT ON ccb_mirn_details TO maximo, readonly
/
GRANT SELECT ON ccb_meter_config_details TO maximo, readonly
/
GRANT SELECT ON ccbdatarec_issue_types TO maximo, readonly
/
GRANT SELECT ON ccbdatarec_issues TO maximo, readonly
/
GRANT SELECT ON ccbdatarec_summary TO maximo, readonly
/
